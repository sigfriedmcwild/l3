CC=clang++
CCOMPILEFLAGS=-std=c++1z -Weverything -Werror -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-missing-prototypes -Wno-format-pedantic -Wno-switch-enum -Wno-float-equal -Wno-padded -Wno-missing-noreturn
CDEBUGINFOFLAGS=-g
COPTIMIZEFLAGS=-O0
#COPTIMIZEFLAGS=-O3 -flto
CINCLUDES=-Iexternal/istring/inc -Isrc/common/inc -Isrc/compiler/inc -Isrc/lexer/inc/ -Isrc/parser/inc -Isrc/vm/inc
CFLAGS=$(CCOMPILEFLAGS) $(CINCLUDES) $(CDEBUGINFOFLAGS) $(COPTIMIZEFLAGS)
LDFLAGS=


INTDIR=int
OUTDIR=out
SRCDIR=src
TEST_INTDIR=int/test
TEST_OUTDIR=out/test
TEST_SRCDIR=test

COMPILER_EXE=l3c
COMPILER_TEST_EXE=compiler_tests

COMPILER_LIB_SOURCES=\
	src/compiler/src/compiler.cpp

COMPILER_EXE_SOURCES=\
	src/compiler/src/l3c.cpp

COMPILER_TEST_SOURCES=\
	test/compiler/test.cpp

LEXER_EXE=l3lex
LEXER_TEST_EXE=lexer_tests

LEXER_LIB_SOURCES=\
	src/lexer/src/lexer.cpp

LEXER_EXE_SOURCES=\
	src/lexer/src/l3lex.cpp

LEXER_TEST_SOURCES=\
	test/lexer/test.cpp

PARSER_EXE=l3parse
PARSER_TEST_EXE=parser_tests

PARSER_LIB_SOURCES=\
	src/parser/src/parser.cpp

PARSER_EXE_SOURCES=\
	src/parser/src/l3parse.cpp

PARSER_TEST_SOURCES=\
	test/parser/test.cpp

VM_EXE=l3vm
VM_TEST_EXE=vm_tests

VM_LIB_SOURCES=\
	src/vm/src/object.cpp \
	src/vm/src/vm.cpp \

VM_EXE_SOURCES=\
	src/vm/src/l3vm.cpp

VM_TEST_SOURCES=\
	test/vm/test.cpp

LIBS=\

# magically generated vars
COMPILER_OUTEXE=$(OUTDIR)/$(COMPILER_EXE)
COMPILER_TEST_OUTEXE=$(TEST_OUTDIR)/$(COMPILER_TEST_EXE)

COMPILER_LIB_OBJECTS=$(COMPILER_LIB_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
COMPILER_EXE_OBJECTS=$(COMPILER_EXE_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
COMPILER_TEST_OBJECTS=$(COMPILER_TEST_SOURCES:$(TEST_SRCDIR)/%.cpp=$(TEST_INTDIR)/%.o)

LEXER_OUTEXE=$(OUTDIR)/$(LEXER_EXE)
LEXER_TEST_OUTEXE=$(TEST_OUTDIR)/$(LEXER_TEST_EXE)

LEXER_LIB_OBJECTS=$(LEXER_LIB_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
LEXER_EXE_OBJECTS=$(LEXER_EXE_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
LEXER_TEST_OBJECTS=$(LEXER_TEST_SOURCES:$(TEST_SRCDIR)/%.cpp=$(TEST_INTDIR)/%.o)

PARSER_OUTEXE=$(OUTDIR)/$(PARSER_EXE)
PARSER_TEST_OUTEXE=$(TEST_OUTDIR)/$(PARSER_TEST_EXE)

PARSER_LIB_OBJECTS=$(PARSER_LIB_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
PARSER_EXE_OBJECTS=$(PARSER_EXE_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
PARSER_TEST_OBJECTS=$(PARSER_TEST_SOURCES:$(TEST_SRCDIR)/%.cpp=$(TEST_INTDIR)/%.o)

VM_OUTEXE=$(OUTDIR)/$(VM_EXE)
VM_TEST_OUTEXE=$(TEST_OUTDIR)/$(VM_TEST_EXE)

VM_LIB_OBJECTS=$(VM_LIB_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
VM_EXE_OBJECTS=$(VM_EXE_SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.o)
VM_TEST_OBJECTS=$(VM_TEST_SOURCES:$(TEST_SRCDIR)/%.cpp=$(TEST_INTDIR)/%.o)

SOURCES=\
	$(COMPILER_LIB_SOURCES)\
	$(COMPILER_EXE_SOURCES)\
	$(LEXER_LIB_SOURCES)\
	$(LEXER_EXE_SOURCES)\
	$(PARSER_LIB_SOURCES)\
	$(PARSER_EXE_SOURCES)\
	$(VM_LIB_SOURCES)\
	$(VM_EXE_SOURCES)\

TEST_SOURCES=\
	$(COMPILER_TEST_SOURCES) \
	$(LEXER_TEST_SOURCES)\
	$(PARSER_TEST_SOURCES)\
	$(VM_TEST_SOURCES)\

DEPMAKEFILES=$(SOURCES:$(SRCDIR)/%.cpp=$(INTDIR)/%.d)
TEST_DEPMAKEFILES=$(TEST_SOURCES:$(TEST_SRCDIR)/%.cpp=$(TEST_INTDIR)/%.d)

all: build

build: build_info $(COMPILER_OUTEXE) $(LEXER_OUTEXE) $(PARSER_OUTEXE) $(VM_OUTEXE)
	@echo
	@echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	@echo done
	@echo

build_info:
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo
	@echo CC="$(CC)"
	$(CC) --version
	@echo
	@echo CFLAGS="$(CFLAGS)"
	@echo LDFLAGS="$(LDFLAGS)"
	@echo
	@echo INTDIR="$(INTDIR)"
	@echo OUTDIR="$(OUTDIR)"
	@echo SRCDIR="$(SRCDIR)"
	@echo
	@echo COMPILER_EXE="$(COMPILER_EXE)"
	@echo COMPILER_LIB_SOURCES="$(COMPILER_LIB_SOURCES)"
	@echo COMPILER_EXE_SOURCES="$(COMPILER_EXE_SOURCES)"
	@echo
	@echo COMPILER_OUTEXE="$(COMPILER_OUTEXE)"
	@echo COMPILER_LIB_OBJECTS="$(COMPILER_LIB_OBJECTS)"
	@echo COMPILER_EXE_OBJECTS="$(COMPILER_EXE_OBJECTS)"
	@echo
	@echo LEXER_EXE="$(LEXER_EXE)"
	@echo LEXER_LIB_SOURCES="$(LEXER_LIB_SOURCES)"
	@echo LEXER_EXE_SOURCES="$(LEXER_EXE_SOURCES)"
	@echo
	@echo LEXER_OUTEXE="$(LEXER_OUTEXE)"
	@echo LEXER_LIB_OBJECTS="$(LEXER_LIB_OBJECTS)"
	@echo LEXER_EXE_OBJECTS="$(LEXER_EXE_OBJECTS)"
	@echo
	@echo PARSER_EXE="$(PARSER_EXE)"
	@echo PARSER_LIB_SOURCES="$(PARSER_LIB_SOURCES)"
	@echo PARSER_EXE_SOURCES="$(PARSER_EXE_SOURCES)"
	@echo
	@echo PARSER_OUTEXE="$(PARSER_OUTEXE)"
	@echo PARSER_LIB_OBJECTS="$(PARSER_LIB_OBJECTS)"
	@echo PARSER_EXE_OBJECTS="$(PARSER_EXE_OBJECTS)"
	@echo
	@echo VM_EXE="$(VM_EXE)"
	@echo VM_LIB_SOURCES="$(VM_LIB_SOURCES)"
	@echo VM_EXE_SOURCES="$(VM_EXE_SOURCES)"
	@echo
	@echo VM_OUTEXE="$(VM_OUTEXE)"
	@echo VM_LIB_OBJECTS="$(VM_LIB_OBJECTS)"
	@echo VM_EXE_OBJECTS="$(VM_EXE_OBJECTS)"
	@echo
	@echo LIBS="$(LIBS)"
	@echo
	@echo SOURCES="$(SOURCES)"
	@echo DEPMAKEFILES="$(DEPMAKEFILES)"
	@echo

$(COMPILER_OUTEXE): $(COMPILER_EXE_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(COMPILER_EXE_OBJECTS) and $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) and $(LIBS) as $(COMPILER_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(COMPILER_EXE_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(COMPILER_OUTEXE) $(LIBS)

$(LEXER_OUTEXE): $(LEXER_EXE_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(LEXER_EXE_OBJECTS) and $(LEXER_LIB_OBJECTS) and $(LIBS) as $(LEXER_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(LEXER_EXE_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(LEXER_OUTEXE) $(LIBS)

$(PARSER_OUTEXE): $(PARSER_EXE_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(PARSER_EXE_OBJECTS) and $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) and $(LIBS) as $(PARSER_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(PARSER_EXE_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(PARSER_OUTEXE) $(LIBS)

$(VM_OUTEXE): $(VM_EXE_OBJECTS) $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(VM_EXE_OBJECTS) and $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) and $(LIBS) as $(VM_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(VM_EXE_OBJECTS) $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(VM_OUTEXE) $(LIBS)

$(INTDIR)/%.o: $(SRCDIR)/%.cpp
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo building $< as $@
	@echo
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $< -o $@

$(INTDIR)/%.d: $(SRCDIR)/%.cpp
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo creating dependency make file for $< as $@ with target $(INTDIR)/$*.o
	@echo
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -MM $< -MF $@ -MT $(INTDIR)/$*.o

clean:
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo deleting everything in $(INTDIR) and $(OUTDIR)
	@echo
	rm -rf $(INTDIR) $(OUTDIR)

test: test_info $(COMPILER_TEST_OUTEXE) $(LEXER_TEST_OUTEXE) $(PARSER_TEST_OUTEXE) $(VM_TEST_OUTEXE)
	@echo
	@echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	@echo tests done
	@echo

test_info: build_info
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo
	@echo TEST_INTDIR="$(TEST_INTDIR)"
	@echo TEST_OUTDIR="$(TEST_OUTDIR)"
	@echo TEST_SRCDIR="$(TEST_SRCDIR)"
	@echo
	@echo COMPILER_TEST_EXE="$(COMPILER_TEST_EXE)"
	@echo COMPILER_TEST_SOURCES="$(COMPILER_TEST_SOURCES)"
	@echo
	@echo COMPILER_TEST_OUTEXE="$(COMPILER_TEST_OUTEXE)"
	@echo COMPILER_TEST_OBJECTS="$(COMPILER_TEST_OBJECTS)"
	@echo
	@echo LEXER_TEST_EXE="$(LEXER_TEST_EXE)"
	@echo LEXER_TEST_SOURCES="$(LEXER_TEST_SOURCES)"
	@echo
	@echo LEXER_TEST_OUTEXE="$(LEXER_TEST_OUTEXE)"
	@echo LEXER_TEST_OBJECTS="$(LEXER_TEST_OBJECTS)"
	@echo
	@echo PARSER_TEST_EXE="$(PARSER_TEST_EXE)"
	@echo PARSER_TEST_SOURCES="$(PARSER_TEST_SOURCES)"
	@echo
	@echo PARSER_TEST_OUTEXE="$(PARSER_TEST_OUTEXE)"
	@echo PARSER_TEST_OBJECTS="$(PARSER_TEST_OBJECTS)"
	@echo
	@echo VM_TEST_EXE="$(VM_TEST_EXE)"
	@echo VM_TEST_SOURCES="$(VM_TEST_SOURCES)"
	@echo
	@echo VM_TEST_OUTEXE="$(VM_TEST_OUTEXE)"
	@echo VM_TEST_OBJECTS="$(VM_TEST_OBJECTS)"
	@echo
	@echo TEST_SOURCES="$(TEST_SOURCES)"
	@echo TEST_DEPMAKEFILES="$(TEST_DEPMAKEFILES)"
	@echo TEST_LIBS="$(TEST_LIBS)"
	@echo

$(COMPILER_TEST_OUTEXE): $(COMPILER_TEST_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(COMPILER_TEST_OBJECTS) and $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) and $(TEST_LIBS) as $(COMPILER_TEST_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(COMPILER_TEST_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(COMPILER_TEST_OUTEXE) $(TEST_LIBS)

$(LEXER_TEST_OUTEXE): $(LEXER_TEST_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(LEXER_TEST_OBJECTS) and $(LEXER_LIB_OBJECTS) and $(TEST_LIBS) as $(LEXER_TEST_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(LEXER_TEST_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(LEXER_TEST_OUTEXE) $(TEST_LIBS)

$(PARSER_TEST_OUTEXE): $(PARSER_TEST_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(PARSER_TEST_OBJECTS) and $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) and $(TEST_LIBS) as $(PARSER_TEST_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(PARSER_TEST_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(PARSER_TEST_OUTEXE) $(TEST_LIBS)

$(VM_TEST_OUTEXE): $(VM_TEST_OBJECTS) $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS)
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo linking $(VM_TEST_OBJECTS) and $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) and $(TEST_LIBS) as $(VM_TEST_OUTEXE)
	@echo
	mkdir -p $(@D)
	$(CC) $(LDFLAGS) $(VM_TEST_OBJECTS) $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(VM_TEST_OUTEXE) $(TEST_LIBS)

$(TEST_INTDIR)/%.o: $(TEST_SRCDIR)/%.cpp
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo building $< as $@
	@echo
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $< -o $@

$(TEST_INTDIR)/%.d: $(TEST_SRCDIR)/%.cpp
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo creating dependency make file for $< as $@ with target $(TEST_INTDIR)/$*.o
	@echo
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -MM $< -MF $@ -MT $(TEST_INTDIR)/$*.o

perf: test
	@echo
	@echo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@echo building vm perf tests
	@echo
	$(CC) $(CFLAGS) $(VM_TEST_SOURCES) $(VM_LIB_OBJECTS) $(COMPILER_LIB_OBJECTS) $(PARSER_LIB_OBJECTS) $(LEXER_LIB_OBJECTS) -o $(TEST_OUTDIR)/vm_perf_test $(TEST_LIBS) -DPERF_TEST=1

include $(DEPMAKEFILES)
include $(TEST_DEPMAKEFILES)
