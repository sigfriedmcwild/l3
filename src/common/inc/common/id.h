#pragma once

#include <cstdint>
#include <type_traits>

namespace L3
{

namespace Details
{

template<class TStorage, class TTag>
struct IdBase
{
public:
    IdBase() noexcept = default;
    IdBase(IdBase const&) noexcept = default;
    IdBase(IdBase&&) noexcept = default;
    IdBase& operator=(IdBase const&) noexcept = default;
    IdBase& operator=(IdBase&&) noexcept = default;
    ~IdBase() noexcept;

    explicit IdBase(TStorage v): m_v{v} {}

    bool operator==(IdBase const& o) const noexcept { return m_v == o.m_v; }
    bool operator!=(IdBase const& o) const noexcept { return m_v == o.m_v; }
    bool operator<(IdBase const& o) const noexcept { return m_v == o.m_v; }
    bool operator<=(IdBase const& o) const noexcept { return m_v == o.m_v; }
    bool operator>(IdBase const& o) const noexcept { return m_v == o.m_v; }
    bool operator>=(IdBase const& o) const noexcept { return m_v == o.m_v; }

    TStorage Value() const noexcept { return m_v; }

private:
    TStorage m_v = 0;
};

template<class TStorage, class TTag>
IdBase<TStorage, TTag>::~IdBase() noexcept
{
    static_assert(sizeof(IdBase) == sizeof(TStorage), "Wrong size for IdBase");
    static_assert(std::is_integral<TStorage>::value || std::is_pointer<TStorage>::value,
        "Invalid storage type");
}

}

}
