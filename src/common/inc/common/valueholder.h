#pragma once

#include <cassert>

#include <common/error.h>
#include <istring/istring.h>

namespace L3
{

using IString::istring;

enum class ValueHolderType
{
    None,
    Double,
    Error,
    String,
};

class ValueHolder
{
public:
    ValueHolder() noexcept;
    ValueHolder(ValueHolder const& o) noexcept;
    ValueHolder(ValueHolder&& o) noexcept;
    ValueHolder& operator=(ValueHolder const& o) noexcept;
    ValueHolder& operator=(ValueHolder&& o) noexcept;
    ~ValueHolder() noexcept;

    ValueHolder(double v) noexcept;
    ValueHolder(ErrorType v) noexcept;
    ValueHolder(istring&& v) noexcept;

    ValueHolderType Type() const noexcept;
    double DoubleValue() const noexcept;
    ErrorType ErrorValue() const noexcept;
    istring StringValue() const noexcept;

    explicit operator bool() const noexcept;

private:
    void Clear() noexcept;
    void Set(double v) noexcept;
    void Set(ErrorType v) noexcept;
    void Set(istring&& v) noexcept;

private:
    ValueHolderType m_type;
    union
    {
        double v_double;
        ErrorType v_error;
        istring v_string;
    };
};

inline
ValueHolder::ValueHolder() noexcept
    : m_type{ValueHolderType::None}
{}

inline
ValueHolder::ValueHolder(ValueHolder const& o) noexcept
    : ValueHolder{}
{
    switch (o.m_type)
    {
        case ValueHolderType::None: break;
        case ValueHolderType::Double: Set(o.v_double); break;
        case ValueHolderType::Error: Set(o.v_error); break;
        case ValueHolderType::String: Set(istring{o.v_string}); break;
    }
}

inline
ValueHolder::ValueHolder(ValueHolder&& o) noexcept
    : ValueHolder{}
{
    switch (o.m_type)
    {
        case ValueHolderType::None: break;
        case ValueHolderType::Double: Set(std::move(o.v_double)); break;
        case ValueHolderType::Error: Set(std::move(o.v_error)); break;
        case ValueHolderType::String: Set(std::move(o.v_string)); break;
    }

    o.Clear();
}

inline
ValueHolder& ValueHolder::operator=(ValueHolder const& o) noexcept
{
    if (this == &o)
    {
        return *this;
    }

    if (m_type != o.m_type)
    {
        Clear();
    }

    switch (o.m_type)
    {
        case ValueHolderType::None: break;
        case ValueHolderType::Double: Set(o.v_double); break;
        case ValueHolderType::Error: Set(o.v_error); break;
        case ValueHolderType::String: Set(istring{o.v_string}); break;
    }

    return *this;
}

inline
ValueHolder& ValueHolder::operator=(ValueHolder&& o) noexcept
{
    if (this == &o)
    {
        return *this;
    }

    if (m_type != o.m_type)
    {
        Clear();
    }

    switch (o.m_type)
    {
        case ValueHolderType::None: break;
        case ValueHolderType::Double: Set(std::move(o.v_double)); break;
        case ValueHolderType::Error: Set(std::move(o.v_error)); break;
        case ValueHolderType::String: Set(std::move(o.v_string)); break;
    }

    o.Clear();

    return *this;
}

inline
ValueHolder::~ValueHolder() noexcept
{
    Clear();
}

inline
ValueHolder::ValueHolder(double v) noexcept
    : ValueHolder{}
{
    Set(v);
}

inline
ValueHolder::ValueHolder(ErrorType v) noexcept
    : ValueHolder{}
{
    Set(v);
}

inline
ValueHolder::ValueHolder(istring&& v) noexcept
    : ValueHolder{}
{
    Set(std::move(v));
}

inline
ValueHolderType ValueHolder::Type() const noexcept
{
    return m_type;
}

inline
double ValueHolder::DoubleValue() const noexcept
{
    assert(m_type == ValueHolderType::Double);
    return v_double;
}

inline
ErrorType ValueHolder::ErrorValue() const noexcept
{
    assert(m_type == ValueHolderType::Error);
    return v_error;
}

inline
istring ValueHolder::StringValue() const noexcept
{
    assert(m_type == ValueHolderType::String);
    return v_string;
}

inline
void ValueHolder::Clear() noexcept
{
    if (m_type == ValueHolderType::String)
    {
        v_string.~istring();
    }

    m_type = ValueHolderType::None;
}

inline
void ValueHolder::Set(double v) noexcept
{
    assert(m_type == ValueHolderType::None || m_type == ValueHolderType::Double);
    v_double = v;
    m_type = ValueHolderType::Double;
}

inline
void ValueHolder::Set(ErrorType v) noexcept
{
    assert(m_type == ValueHolderType::None || m_type == ValueHolderType::Error);
    v_error = v;
    m_type = ValueHolderType::Error;
}

inline
void ValueHolder::Set(istring&& v) noexcept
{
    assert(m_type == ValueHolderType::None || m_type == ValueHolderType::String);
    if (m_type == ValueHolderType::None)
    {
        m_type = ValueHolderType::String;
        new (&v_string) istring{std::move(v)};
    }
    else
    {
        v_string = std::move(v);
    }
}

inline
ValueHolder::operator bool() const noexcept
{
    return m_type != ValueHolderType::None;
}

inline
bool operator==(ValueHolder const& l, ValueHolder const& r)
{
    if (l.Type() != r.Type())
    {
        return false;
    }

    switch (l.Type())
    {
        case ValueHolderType::None: return true;
        case ValueHolderType::Double: return l.DoubleValue() == r.DoubleValue();
        case ValueHolderType::Error: return l.ErrorValue() == r.ErrorValue();
        case ValueHolderType::String: return l.StringValue() == r.StringValue();
    }
}

inline
bool operator!=(ValueHolder const& l, ValueHolder const& r)
{
    return !(l == r);
}

}
