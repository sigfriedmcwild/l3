#pragma once

#include <istring/istring.h>

namespace L3
{

using IString::istring;

struct Cursor
{
    size_t Index = 0;
    uint32_t Line = 1;
    uint32_t Column = 1;
};

struct SourceReference
{
    istring Text;
    Cursor Begin;
    Cursor End;
};

inline
bool operator==(Cursor l, Cursor r) noexcept
{
    assert (l.Index != r.Index || (l.Line == r.Line && l.Column == r.Column));
    assert (l.Index == r.Index || !(l.Line == r.Line && l.Column == r.Column));
    return l.Index == r.Index; // assume line/column numbering is coherent
}

inline
bool operator!=(Cursor l, Cursor r) noexcept
{
    return !(l == r);
}

inline
bool operator<(Cursor l, Cursor r) noexcept
{
    return l.Index < r.Index;
}

inline
bool operator==(SourceReference const& l, SourceReference const& r) noexcept
{
    assert((l.Text == r.Text && l.Begin == r.Begin) || l.End != r.End);
    assert(!(l.Text == r.Text && l.Begin == r.Begin) || l.End == r.End);
    return l.Text == r.Text && l.Begin == r.Begin;
}

inline
bool operator!=(SourceReference const& l, SourceReference const& r) noexcept
{
    return !(l == r);
}

}
