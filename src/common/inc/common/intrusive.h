#pragma once

#include <type_traits>

namespace L3
{

template<class T>
class Intrusive
{
public:
    Intrusive() = delete;
    Intrusive(Intrusive const&) = delete;
    Intrusive(Intrusive&&) = delete;
    Intrusive& operator=(Intrusive const&) = delete;
    Intrusive& operator=(Intrusive&&) = delete;

    ~Intrusive();

    template<class... TArgs>
    Intrusive(TArgs&&... args);

    T Value;

    size_t Hold() const noexcept;
    size_t Release() const noexcept;

private:
    mutable size_t m_refCount = 0; // intentionally not threadsafe
};

template<class T>
class IntrusivePtr
{
public:
    IntrusivePtr() noexcept;
    IntrusivePtr(IntrusivePtr const& o) noexcept;
    IntrusivePtr(IntrusivePtr&& o) noexcept;
    IntrusivePtr& operator=(IntrusivePtr const& o) noexcept;
    IntrusivePtr& operator=(IntrusivePtr&& o) noexcept;
    ~IntrusivePtr() noexcept;

    explicit IntrusivePtr(Intrusive<T>* ptr) noexcept;

    void Reset() noexcept;
    Intrusive<T>* Get() const noexcept;

    T& operator*() const noexcept;
    T* operator->() const noexcept;
    explicit operator bool() const noexcept;

private:
    Intrusive<T>* m_ptr;
};

template<class T>
size_t Intrusive<T>::Hold() const noexcept
{
    ++m_refCount;
    return m_refCount;
}

template<class T>
size_t Intrusive<T>::Release() const noexcept
{
    --m_refCount;
    auto tmp = m_refCount;

    if (m_refCount == 0) { delete this; }

    return tmp;
}

template<class T>
Intrusive<T>::~Intrusive()
{
    static_assert(std::is_object<T>::value, "Intrusive<T>, T must be an object");
}

template<class T> template<class... TArgs>
Intrusive<T>::Intrusive(TArgs&&... args)
    : Value{std::forward<TArgs>(args)...}
{}

template<class T>
IntrusivePtr<T>::IntrusivePtr() noexcept
    : m_ptr{nullptr}
{}

template<class T>
IntrusivePtr<T>::IntrusivePtr(IntrusivePtr const& o) noexcept
    : m_ptr{o.m_ptr}
{
    if (m_ptr) { m_ptr->Hold(); }
}

template<class T>
IntrusivePtr<T>::IntrusivePtr(IntrusivePtr&& o) noexcept
    : m_ptr{o.m_ptr}
{
    o.m_ptr = nullptr;
}

template<class T>
IntrusivePtr<T>& IntrusivePtr<T>::operator=(IntrusivePtr const& o) noexcept
{
    if (this == *o) { return *this; }

    Reset();
    m_ptr = o.m_ptr;
    m_ptr->Hold();

    return *this;
}

template<class T>
IntrusivePtr<T>& IntrusivePtr<T>::operator=(IntrusivePtr&& o) noexcept
{
    if (this == &o) { return *this; }

    Reset();
    m_ptr = o.m_ptr;
    o.m_ptr = nullptr;

    return *this;
}

template<class T>
IntrusivePtr<T>::~IntrusivePtr() noexcept
{
    static_assert(sizeof(IntrusivePtr) == sizeof(Intrusive<T>*), "Wrong size for IntrusivePtr");
    Reset();
}

template<class T>
IntrusivePtr<T>::IntrusivePtr(Intrusive<T>* ptr) noexcept
    : m_ptr{ptr}
{
    if (m_ptr) { m_ptr->Hold(); }
}

template<class T>
void IntrusivePtr<T>::Reset() noexcept
{
    if (m_ptr) { m_ptr->Release(); m_ptr = nullptr; }
}

template<class T>
Intrusive<T>* IntrusivePtr<T>::Get() const noexcept
{
    return m_ptr;
}

template<class T>
T& IntrusivePtr<T>::operator*() const noexcept
{
    return m_ptr->Value;
}

template<class T>
T* IntrusivePtr<T>::operator->() const noexcept
{
    return &m_ptr->Value;
}

template<class T>
IntrusivePtr<T>::operator bool() const noexcept
{
    return m_ptr != nullptr;
}

template<class T>
bool operator==(IntrusivePtr<T> const& l, IntrusivePtr<T> const& r) noexcept
{
    return l.m_ptr == r.m_ptr;
}

template<class T>
bool operator!=(IntrusivePtr<T> const& l, IntrusivePtr<T> const& r) noexcept
{
    return l.m_ptr != r.m_ptr;
}

template<class T>
bool operator<(IntrusivePtr<T> const& l, IntrusivePtr<T> const& r) noexcept
{
    return l.m_ptr < r.m_ptr;
}

template<class T>
bool operator<=(IntrusivePtr<T> const& l, IntrusivePtr<T> const& r) noexcept
{
    return l.m_ptr <= r.m_ptr;
}

template<class T>
bool operator>(IntrusivePtr<T> const& l, IntrusivePtr<T> const& r) noexcept
{
    return l.m_ptr > r.m_ptr;
}

template<class T>
bool operator>=(IntrusivePtr<T> const& l, IntrusivePtr<T> const& r) noexcept
{
    return l.m_ptr >= r.m_ptr;
}

template<class T, class... TArgs>
IntrusivePtr<T> MakeIntrusive(TArgs&&... args)
{
    return IntrusivePtr<T>{new Intrusive<T>{std::forward<TArgs>(args)...}};
}

}
