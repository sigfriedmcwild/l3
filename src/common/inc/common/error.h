#pragma once

namespace L3
{

enum class ErrorType
{
    Generic,
    InvalidToken,
    InvalidAtom,
    InvalidId,
    InvalidNumber,
    InvalidEscape,
    InvalidCharacter,
    UnterminatedString,
    UnexpectedToken,
};

}
