#pragma once

#include <cassert>
#include <cstdio>
#include <string>

#include <common/error.h>
#include <common/sourcereference.h>
#include <common/valueholder.h>
#include <istring/istring.h>
#include <istring/string_view.h>
#include <istring/utility.h>

namespace L3
{

using IString::istring;
using IString::istring_view;

inline
istring_view ErrorTypeName(ErrorType e) noexcept
{
    switch (e)
    {
        case ErrorType::Generic:            return "GENERIC ERROR";
        case ErrorType::InvalidToken:       return "INVALID TOKEN";
        case ErrorType::InvalidAtom:        return "INVALID ATOM";
        case ErrorType::InvalidId:          return "INVALID IDENTIFIER";
        case ErrorType::InvalidNumber:      return "INVALID NUMBER";
        case ErrorType::InvalidEscape:      return "INVALID ESCAPE";
        case ErrorType::InvalidCharacter:   return "INVALID CHARACTER";
        case ErrorType::UnterminatedString: return "UNTERMINATED STRING";
        case ErrorType::UnexpectedToken:    return "UNEXPECTED TOKEN";
    }
}

inline
istring EscapeForPrint(istring const& source) noexcept
{
    // TODO need istring buffer api?
    auto buffer = to_string(source);

    auto Replace = [](std::string& text, char c, auto const& r)
    {
        auto next = text.find(c);
        while (next != std::string::npos)
        {
            text.replace(next, 1, r);
            next = text.find(c, next + std::strlen(r));
        }
    };

    Replace(buffer, '\\', "\\\\");
    Replace(buffer, '"', "\\\"");
    Replace(buffer, '\'', "\\'");
    Replace(buffer, '\0', "\\0");
    Replace(buffer, '\t', "\\t");
    Replace(buffer, '\r', "\\r");
    Replace(buffer, '\n', "\\n");

    return istring{buffer};
}

inline
void PrintSourceReference(SourceReference const& sr) noexcept
{
    static size_t const tokenMaxText = 60;

    auto tokenText = EscapeForPrint(sr.Text).substr(0, tokenMaxText);

    assert(sr.End.Column > 0); // it should be impossible for a token end cursor
                               // to underflow the line count
    std::printf("%4u:%4u, %4u:%4u> '%.*s'",
        sr.Begin.Line,
        sr.Begin.Column,
        sr.End.Line,
        sr.End.Column - 1,
        PRINTF_STR(tokenText)
    );
}

inline
void PrintSourceReferenceLine(SourceReference const& sr) noexcept
{
    PrintSourceReference(sr);
    std::printf("\n");
}

inline
istring to_istring(ValueHolder const& v)
{
    switch (v.Type())
    {
        case ValueHolderType::None:
            return istring{};
        case ValueHolderType::Double:
            return istring{std::to_string(v.DoubleValue())};
        case ValueHolderType::Error:
            return istring{ErrorTypeName(v.ErrorValue())};
        case ValueHolderType::String:
            return EscapeForPrint(v.StringValue());
    }
}

}
