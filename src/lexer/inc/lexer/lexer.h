#pragma once

#include <common/sourcereference.h>
#include <common/valueholder.h>
#include <istring/istring.h>

namespace L3
{

using IString::istring;

enum class TokenType
{
    Begin,
    Error,
    Eof,
    Comment,
    IdAtom,
    IdVar,
    Number,
    String,
    KAtom,
    KBreak,
    KContinue,
    KDo,
    KElse,
    KFalse,
    KFor,
    KFn,
    KIf,
    KLst,
    KNil,
    KRecurse,
    KReturn,
    KTbl,
    KTrue,
    KVar,
    KWhile,
    OAdd,
    OSubtract,
    OMultiply,
    ODivide,
    OModulus,
    OSize,
    OIncrement,
    ODecrement,
    OEqual,
    ONotEqual,
    OLess,
    OLessEqual,
    OGreater,
    OGreaterEqual,
    OAnd,
    OOr,
    ONot,
    OAppend,
    OMemberAccess,
    OMemberCall,
    OAssign,
    OAddAssign,
    OSubtractAssign,
    OMultiplyAssign,
    ODivideAssign,
    OModulusAssign,
    OAndAssign,
    OOrAssign,
    OAppendAssign,
    POpen,
    PClose,
    POpenSquare,
    PCloseSquare,
    POpenCurly,
    PCloseCurly,
    PSemicolon,
    PComma,
};

class SourceState
{
public:
    SourceState() noexcept = default;
    SourceState(SourceState const&) = default;
    SourceState(SourceState&&) noexcept = default;
    SourceState& operator=(SourceState const&) = default;
    SourceState& operator=(SourceState&&) noexcept = default;

    explicit SourceState(istring const& source) noexcept;

    bool Eof() const noexcept;
    L3::Cursor Cursor() const noexcept;
    char Current() const noexcept;
    size_t Size() const noexcept;

    istring TokenString(L3::Cursor begin, L3::Cursor end) const noexcept;

    SourceState Next() const noexcept;

private:
    SourceState(istring const& s, L3::Cursor c);

public:
    istring m_source;
    L3::Cursor m_cursor = {};
};

class Token
{
public:
    Token() noexcept = default;
    Token(Token const&) noexcept = default;
    Token(Token&&) noexcept = default;
    Token& operator=(Token const&) noexcept = default;
    Token& operator=(Token&&) noexcept = default;

    static Token Error(ErrorType v, SourceReference&& s) noexcept;
    static Token IdAtom(istring&& v, SourceReference&& s) noexcept;
    static Token IdVar(istring&& v, SourceReference&& s) noexcept;
    static Token Number(double v, SourceReference&& s) noexcept;
    static Token Simple(TokenType t, SourceReference&& s) noexcept;
    static Token String(istring&& v, SourceReference&& s) noexcept;

    TokenType Type() const noexcept;
    SourceReference Source() const noexcept;
    ValueHolder Value() const noexcept;

private:
    Token(
        TokenType t,
        SourceReference&& s,
        ValueHolder&& v
    ) noexcept;

private:
    TokenType m_type = TokenType::Begin;
    SourceReference m_source;
    ValueHolder m_value;
};

class LexerState
{
public:
    LexerState() noexcept = default;
    LexerState(LexerState const&) = delete;
    LexerState(LexerState&&) noexcept = default;
    LexerState& operator=(LexerState const&) = delete;
    LexerState& operator=(LexerState&&) noexcept = default;

    static LexerState Error(ErrorType v, SourceState&& s, Cursor b) noexcept;
    static LexerState IdAtom(istring&& v, SourceState&& s, Cursor b) noexcept;
    static LexerState IdVar(istring&& v, SourceState&& s, Cursor b) noexcept;
    static LexerState Number(double v, SourceState&& s, Cursor b) noexcept;
    static LexerState Simple(TokenType t, SourceState&& s, Cursor b) noexcept;
    static LexerState String(istring&& v, SourceState&& s, Cursor b) noexcept;

    bool Terminal() const noexcept;
    SourceState Source() const noexcept;
    L3::Token Token() const noexcept;

private:
    LexerState(SourceState&& s, L3::Token&& t) noexcept;

    L3::SourceState m_source;
    L3::Token m_token;
};

class Lexer
{
public:
    static LexerState Init(istring const& text) noexcept;
    static LexerState Advance(LexerState&& source) noexcept;

private:
    static LexerState AdvanceOverIdentifier(SourceState&& s) noexcept;
    static LexerState AdvanceOverAtom(SourceState&& s) noexcept;
    static LexerState AdvanceOverNumber(SourceState&& s, bool leadingDot) noexcept;
    static LexerState AdvanceOverSingleCharacterToken(SourceState&& s) noexcept;
    static LexerState AdvanceOverMultiCharacterToken(SourceState&& s) noexcept;
    static LexerState AdvanceOverString(SourceState&& s) noexcept;

    static bool CheckIdentifier(SourceState const& s) noexcept;

    static SourceState ConsumeComment(SourceState&& s) noexcept;
    static SourceState ConsumeDigits(SourceState&& s) noexcept;
    static SourceState ConsumeUnderscore(SourceState&& s) noexcept;
    static SourceState ConsumeWhiteSpace(SourceState&& s) noexcept;

    static LexerState TryRecognizeKeyword(LexerState&& s) noexcept;
};

}
