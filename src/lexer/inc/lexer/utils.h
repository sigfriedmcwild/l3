#pragma once

#include <cstdio>
#include <string>

#include <common/utils.h>
#include <istring/string_view.h>
#include <istring/utility.h>
#include <lexer/lexer.h>

namespace L3
{

using IString::istring_view;

inline
istring_view TokenTypeName(TokenType t) noexcept
{
    switch (t)
    {
        case TokenType::Begin:              return "Begin";
        case TokenType::Error:              return "Error";
        case TokenType::Eof:                return "Eof";
        case TokenType::Comment:            return "Comment";
        case TokenType::IdAtom:             return "IdAtom";
        case TokenType::IdVar:              return "IdVar";
        case TokenType::Number:             return "Number";
        case TokenType::String:             return "String";
        case TokenType::KAtom:              return "KAtom";
        case TokenType::KBreak:             return "KBreak";
        case TokenType::KContinue:          return "KContinue";
        case TokenType::KDo:                return "KDo";
        case TokenType::KElse:              return "KElse";
        case TokenType::KFalse:             return "KFalse";
        case TokenType::KFor:               return "KFor";
        case TokenType::KFn:                return "KFn";
        case TokenType::KIf:                return "KIf";
        case TokenType::KLst:               return "KLst";
        case TokenType::KNil:               return "KNil";
        case TokenType::KRecurse:           return "KRecurse";
        case TokenType::KReturn:            return "KReturn";
        case TokenType::KTbl:               return "KTbl";
        case TokenType::KTrue:              return "KTrue";
        case TokenType::KVar:               return "KVar";
        case TokenType::KWhile:             return "KWhile";
        case TokenType::OAdd:               return "OAdd";
        case TokenType::OSubtract:          return "OSubtract";
        case TokenType::OMultiply:          return "OMultiply";
        case TokenType::ODivide:            return "ODivide";
        case TokenType::OModulus:           return "OModulus";
        case TokenType::OSize:              return "OSize";
        case TokenType::OIncrement:         return "OIncrement";
        case TokenType::ODecrement:         return "ODecrement";
        case TokenType::OEqual:             return "OEqual";
        case TokenType::ONotEqual:          return "ONotEqual";
        case TokenType::OLess:              return "OLess";
        case TokenType::OLessEqual:         return "OLessEqual";
        case TokenType::OGreater:           return "OGreater";
        case TokenType::OGreaterEqual:      return "OGreaterEqual";
        case TokenType::OAnd:               return "OAnd";
        case TokenType::OOr:                return "OOr";
        case TokenType::ONot:               return "ONot";
        case TokenType::OAppend:            return "OAppend";
        case TokenType::OMemberAccess:      return "OMemberAccess";
        case TokenType::OMemberCall:        return "OMemberCall";
        case TokenType::OAssign:            return "OAssign";
        case TokenType::OAddAssign:         return "OAddAssign";
        case TokenType::OSubtractAssign:    return "OSubtractAssign";
        case TokenType::OMultiplyAssign:    return "OMultiplyAssign";
        case TokenType::ODivideAssign:      return "ODivideAssign";
        case TokenType::OModulusAssign:     return "OModulusAssign";
        case TokenType::OAndAssign:         return "OAndAssign";
        case TokenType::OOrAssign:          return "OOrAssign";
        case TokenType::OAppendAssign:      return "OAppendAssign";
        case TokenType::POpen:              return "POpen";
        case TokenType::PClose:             return "PClose";
        case TokenType::POpenSquare:        return "POpenSquare";
        case TokenType::PCloseSquare:       return "PCloseSquare";
        case TokenType::POpenCurly:         return "POpenCurly";
        case TokenType::PCloseCurly:        return "PCloseCurly";
        case TokenType::PSemicolon:         return "PSemicolon";
        case TokenType::PComma:             return "PComma";
        //default:                            return istring_view{"<INVALID TOKEN TYPE>"};
    }
}

inline
void PrintToken(Token const& t) noexcept
{
    static size_t const tokenTypeNameWidth = 20;
    static size_t const tokenValueMaxWidth = 30;

    auto tn = L3::TokenTypeName(t.Type());
    auto tv = to_istring(t.Value()).substr(0, tokenValueMaxWidth);

    auto pad = tokenTypeNameWidth + tokenValueMaxWidth - tn.size() - tv.size();
    std::printf("%.*s%*c%.*s - ",
        PRINTF_STR(tn),
        static_cast<int>(pad), ' ',
        PRINTF_STR(tv)
    );
    PrintSourceReference(t.Source());
}

inline
void PrintToken(LexerState const& l) noexcept
{
    PrintToken(l.Token());
}

inline
void PrintTokenLine(Token const& t) noexcept
{
    PrintToken(t);
    std::printf("\n");
}

inline
void PrintTokenLine(LexerState const& l) noexcept
{
    PrintTokenLine(l.Token());
}

inline
bool operator==(Token const& l, Token const& r) noexcept
{
    return l.Type() == r.Type() && l.Source() == r.Source() && l.Value() == r.Value();
}

inline
bool operator!=(Token const& l, Token const& r) noexcept
{
    return !(l == r);
}

}
