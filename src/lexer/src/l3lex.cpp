#include <cerrno>
#include <cstdio>
#include <string>

#include <istring/istring.h>
#include <lexer/lexer.h>
#include <lexer/utils.h>

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::printf("Usage: l3lex <file>\n");
        return 0;
    }

    auto f = fopen(argv[1], "r");
    if (!f)
    {
        std::perror("Failed to open input file");
        return 1;
    }

    std::string buffer;
    while (true)
    {
        static size_t const CHUNK_SIZE = 1024 * 10;

        auto current = buffer.size();
        buffer.resize(buffer.size() + CHUNK_SIZE);

        auto read = std::fread(&buffer[current], 1, CHUNK_SIZE, f);
        if (read != CHUNK_SIZE)
        {
            if (std::feof(f))
            {
                buffer.resize(current + read);
                break;
            }
            else if (std::ferror(f))
            {
                std::printf("Error reading file\n");
                return 1;
            }
        }
    }

    IString::istring text{buffer};

    auto ls = L3::Lexer::Init(text);

    ls = L3::Lexer::Advance(std::move(ls));
    while (!ls.Terminal())
    {
        L3::PrintTokenLine(ls.Token());
        ls = L3::Lexer::Advance(std::move(ls));
    }

    L3::PrintTokenLine(ls.Token());

    return 0;
}
