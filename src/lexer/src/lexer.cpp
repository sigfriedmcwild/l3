#include <lexer/lexer.h>

#include <cassert>
#include <cstdlib>

namespace L3
{

SourceState::SourceState(istring const& source) noexcept
    : m_source{source}, m_cursor{0, 1, 1} // Line and Column are 1 based indexes
{}

bool SourceState::Eof() const noexcept
{
    return m_cursor.Index == m_source.size();
}

Cursor SourceState::Cursor() const noexcept
{
    return m_cursor;
}

char SourceState::Current() const noexcept
{
    assert(!Eof());
    return m_source[m_cursor.Index];
}

size_t SourceState::Size() const noexcept
{
    return m_source.size();
}

istring SourceState::TokenString(L3::Cursor begin, L3::Cursor end) const noexcept
{
    return m_source.substr(begin.Index, end.Index - begin.Index);
}

SourceState SourceState::Next() const noexcept
{
    assert(!Eof());
    if (Current() == '\n')
    {
        return SourceState{m_source, L3::Cursor{m_cursor.Index + 1, m_cursor.Line + 1, 1}};
    }
    else
    {
        return SourceState{m_source, L3::Cursor{m_cursor.Index + 1, m_cursor.Line, m_cursor.Column + 1}};
    }
}

SourceState::SourceState(istring const& s, L3::Cursor c)
    : m_source{s}, m_cursor{c}
{}

Token Token::Error(ErrorType v, SourceReference&& s) noexcept
{
    return Token{TokenType::Error, std::move(s), ValueHolder{v}};
}

Token Token::IdAtom(istring&& v, SourceReference&& s) noexcept
{
    return Token{TokenType::IdAtom, std::move(s), ValueHolder{std::move(v)}};
}

Token Token::IdVar(istring&& v, SourceReference&& s) noexcept
{
    return Token{TokenType::IdVar, std::move(s), ValueHolder{std::move(v)}};
}

Token Token::Number(double v, SourceReference&& s) noexcept
{
    return Token{TokenType::Number, std::move(s), ValueHolder{v}};
}

Token Token::Simple(TokenType t, SourceReference&& s) noexcept
{
    return Token{t, std::move(s), ValueHolder{}};
}

Token Token::String(istring&& v, SourceReference&& s) noexcept
{
    return Token{TokenType::String, std::move(s), ValueHolder{std::move(v)}};
}

TokenType Token::Type() const noexcept
{
    return m_type;
}

SourceReference Token::Source() const noexcept
{
    return m_source;
}

ValueHolder Token::Value() const noexcept
{
    return m_value;
}

Token::Token(TokenType t, SourceReference&& s, ValueHolder&& v) noexcept
:
    m_type{t},
    m_source{std::move(s)},
    m_value{std::move(v)}
{}

LexerState LexerState::Error(ErrorType v, SourceState&& s, Cursor b) noexcept
{
    auto e = s.Cursor();
    auto sr = SourceReference{s.TokenString(b, e), b, e};
    return LexerState{std::move(s), Token::Error(v, std::move(sr))};
}

LexerState LexerState::IdAtom(istring&& v, SourceState&& s, Cursor b) noexcept
{
    auto e = s.Cursor();
    auto sr = SourceReference{s.TokenString(b, e), b, e};
    return LexerState{std::move(s), Token::IdAtom(std::move(v), std::move(sr))};
}

LexerState LexerState::IdVar(istring&& v, SourceState&& s, Cursor b) noexcept
{
    auto e = s.Cursor();
    auto sr = SourceReference{s.TokenString(b, e), b, e};
    return LexerState{std::move(s), Token::IdVar(std::move(v), std::move(sr))};
}

LexerState LexerState::Number(double v, SourceState&& s, Cursor b) noexcept
{
    auto e = s.Cursor();
    auto sr = SourceReference{s.TokenString(b, e), b, e};
    return LexerState{std::move(s), Token::Number(v, std::move(sr))};
}

LexerState LexerState::Simple(TokenType t, SourceState&& s, Cursor b) noexcept
{
    auto e = s.Cursor();
    auto sr = SourceReference{s.TokenString(b, e), b, e};
    return LexerState{std::move(s), Token::Simple(t, std::move(sr))};

}

LexerState LexerState::String(istring&& v, SourceState&& s, Cursor b) noexcept
{
    auto e = s.Cursor();
    auto sr = SourceReference{s.TokenString(b, e), b, e};
    return LexerState{std::move(s), Token::String(std::move(v), std::move(sr))};
}

bool LexerState::Terminal() const noexcept
{
    return m_token.Type() == TokenType::Error || m_token.Type() == TokenType::Eof;
}

SourceState LexerState::Source() const noexcept
{
    return m_source;
}

Token LexerState::Token() const noexcept
{
    return m_token;
}

LexerState::LexerState(SourceState&& s, L3::Token&& t) noexcept
    : m_source{std::move(s)}, m_token{std::move(t)}
{}

LexerState Lexer::Init(istring const& text) noexcept
{
    auto source = SourceState{text};
    auto c = source.Cursor();
    return LexerState::Simple(TokenType::Begin, std::move(source), c);
}

LexerState Lexer::Advance(LexerState&& source) noexcept
{
    if (source.Terminal())
    {
        return std::move(source);
    }

    auto nextTokenStart = ConsumeWhiteSpace(source.Source());
    auto begin = nextTokenStart.Cursor();

    if (nextTokenStart.Eof())
    {
        return LexerState::Simple(TokenType::Eof, std::move(nextTokenStart), begin);
    }

    auto c = nextTokenStart.Current();
    switch (c)
    {
        case 'a': case 'A': case 'b': case 'B':
        case 'c': case 'C': case 'd': case 'D':
        case 'e': case 'E': case 'f': case 'F':
        case 'g': case 'G': case 'h': case 'H':
        case 'i': case 'I': case 'j': case 'J':
        case 'k': case 'K': case 'l': case 'L':
        case 'm': case 'M': case 'n': case 'N':
        case 'o': case 'O': case 'p': case 'P':
        case 'q': case 'Q': case 'r': case 'R':
        case 's': case 'S': case 't': case 'T':
        case 'u': case 'U': case 'v': case 'V':
        case 'w': case 'W': case 'x': case 'X':
        case 'y': case 'Y': case 'z': case 'Z':
        case '_':
            return AdvanceOverIdentifier(std::move(nextTokenStart));
        case '@':
            return AdvanceOverAtom(std::move(nextTokenStart));
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            return AdvanceOverNumber(std::move(nextTokenStart), false);
        case '(': case ')': case '[': case ']': case '{': case '}':
        case '#': case ':': case ',': case ';':
            return AdvanceOverSingleCharacterToken(std::move(nextTokenStart));
        case '+': case '-': case '*': case '/': case '%':
        case '=': case '!': case '<': case '>':
        case '&': case '|': case '.':
            return AdvanceOverMultiCharacterToken(std::move(nextTokenStart));
        case '"': case '\'':
            return AdvanceOverString(std::move(nextTokenStart));
    }

    return LexerState::Error(ErrorType::InvalidCharacter, nextTokenStart.Next(), begin);
}

LexerState Lexer::AdvanceOverIdentifier(SourceState&& s) noexcept
{
    auto begin = s.Cursor();

    if (s.Current() == '_')
    {
        s = ConsumeUnderscore(std::move(s));
        if (s.Eof())
        {
            return LexerState::Error(ErrorType::InvalidId, std::move(s), begin);
        }
        else if (!CheckIdentifier(s))
        {
            return LexerState::Error(ErrorType::InvalidId, std::move(s), begin);
        }
    }
    s = s.Next();

    while (true)
    {
        if (s.Eof())
        {
            break;
        }
        else if (CheckIdentifier(s))
        {
            s = s.Next();
        }
        else
        {
            break;
        }
    }

    auto v = s.TokenString(begin, s.Cursor());
    auto l = LexerState::IdVar(std::move(v), std::move(s), begin);
    return TryRecognizeKeyword(std::move(l));
}

LexerState Lexer::AdvanceOverAtom(SourceState&& s) noexcept
{
    auto begin = s.Cursor();

    s = s.Next();
    if (s.Eof())
    {
        return LexerState::Error(ErrorType::InvalidAtom, std::move(s), begin);
    }
    else if (!CheckIdentifier(s))
    {
        return LexerState::Error(ErrorType::InvalidAtom, std::move(s), begin);
    }

    auto l = AdvanceOverIdentifier(std::move(s));

    if (l.Token().Type() == TokenType::IdVar)
    {
        auto v = l.Token().Value().StringValue();
        return LexerState::IdAtom(std::move(v), l.Source(), begin);
    }
    else
    {
        return LexerState::Error(ErrorType::InvalidAtom, l.Source(), begin);
    }
}

LexerState Lexer::AdvanceOverNumber(SourceState&& s, bool leadingDot) noexcept
{
    static auto const ToLexerState = [](SourceState&& state, Cursor b) noexcept
    {
        auto vs = state.TokenString(b, state.Cursor());
        if (vs.size() > 63)
        {
            return LexerState::Error(ErrorType::InvalidNumber, std::move(state), b);
        }
        if (vs == "0e0" || vs == "0E0")
        {
            // workaround strtod being dumb
            return LexerState::Number(1, std::move(state), b);
        }

        char temp[64] = {};
        std::memcpy(temp, vs.data(), vs.size());

        char* consumed = nullptr;
        auto v = std::strtod(temp, &consumed);

        if (consumed == temp)
        {
            return LexerState::Error(ErrorType::InvalidNumber, std::move(state), b);
        }

        return LexerState::Number(v, std::move(state), b);
    };
    static auto const LeadingZeroError = [](SourceState&& state, Cursor b) noexcept
    {
        state = ConsumeDigits(std::move(state));
        return LexerState::Error(ErrorType::InvalidNumber, std::move(state), b);
    };
    static auto const ConsumeInteger = [](SourceState&& state, bool& valid) noexcept
    {
        valid = true;

        if (state.Current() == '0')
        {
            state = state.Next();
            if (state.Eof()) {return std::move(state);}

            switch (state.Current())
            {
                case '0': case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8': case '9':
                {
                    valid = false;
                    return std::move(state);
                }
            }
        }
        else
        {
            state = ConsumeDigits(std::move(state));
            if (state.Eof()) {std::move(state);}
        }

        return std::move(state);
    };

    auto begin = s.Cursor();
    if (leadingDot)
    {
        begin = Cursor{begin.Index - 1, begin.Line, begin.Column - 1};
    }

    if (!leadingDot)
    {
        // integral part
        // integral part is either a single 0 or not 0 followed by digits
        // if there isn't a leading dot we know there is at least 1 digit
        auto valid = true;
        s = ConsumeInteger(std::move(s), valid);
        if (!valid) {return LeadingZeroError(std::move(s), begin);}
        else if (s.Eof()) {return ToLexerState(std::move(s), begin);}
    }

    // there may be a dot now
    if (s.Current() == '.' || leadingDot)
    {
        // decimal part
        // in the leading dot case we know there's at least 1 digit following it
        s = ConsumeDigits(s.Next());
        if (s.Eof()) {return ToLexerState(std::move(s), begin);}
    }

    // we must have consumed some input by this point (at least 2 charaters in
    // the leading dot case)
    assert(s.Cursor().Index > begin.Index);
    assert(!leadingDot || s.Cursor().Index > begin.Index + 1);

    auto c = s.Current();
    if (c == 'e' || c == 'E')
    {
        // exponent
        s = s.Next();
        if (s.Eof()) {return LexerState::Error(ErrorType::InvalidNumber, std::move(s), begin);}

        // check for +/-
        c = s.Current();
        if (c == '+' || c == '-')
        {
            s = s.Next();
            if (s.Eof()) {return LexerState::Error(ErrorType::InvalidNumber, std::move(s), begin);}
        }

        auto beginExp = s.Cursor();

        // exponent must be at least 1 digit and not begin with 0
        auto valid = true;
        s = ConsumeInteger(std::move(s), valid);
        if (!valid) {return LeadingZeroError(std::move(s), begin);}
        else if (s.Eof()) {return ToLexerState(std::move(s), begin);}

        if (s.Cursor().Index == beginExp.Index)
        {
            return LexerState::Error(ErrorType::InvalidNumber, std::move(s), begin);
        }
    }

    return ToLexerState(std::move(s), begin);
}

LexerState Lexer::AdvanceOverSingleCharacterToken(SourceState&& s) noexcept
{
    auto t = TokenType::Error;
    switch (s.Current())
    {
        case '(': {t = TokenType::POpen;} break;
        case ')': {t = TokenType::PClose;} break;
        case '[': {t = TokenType::POpenSquare;} break;
        case ']': {t = TokenType::PCloseSquare;} break;
        case '{': {t = TokenType::POpenCurly;} break;
        case '}': {t = TokenType::PCloseCurly;} break;
        case '#': {t = TokenType::OSize;} break;
        case ':': {t = TokenType::OMemberCall;} break;
        case ',': {t = TokenType::PComma;} break;
        case ';': {t = TokenType::PSemicolon;} break;
        default: {assert(false);} break;
    }

    return LexerState::Simple(t, s.Next(), s.Cursor());
}

LexerState Lexer::AdvanceOverMultiCharacterToken(SourceState&& s) noexcept
{
    static auto const EndSingleCharaterToken = [](TokenType tt, SourceState&& ss, Cursor b) noexcept
    {
        switch (tt)
        {
            case TokenType::OAnd: case TokenType::OOr:
                return LexerState::Error(ErrorType::InvalidToken, std::move(ss), b);
            default: return LexerState::Simple(tt, std::move(ss), b);
        }
    };

    auto begin = s.Cursor();

    auto t = TokenType::Error;

    switch (s.Current())
    {
        case '+': {t = TokenType::OAdd;} break;
        case '-': {t = TokenType::OSubtract;} break;
        case '*': {t = TokenType::OMultiply;} break;
        case '/': {t = TokenType::ODivide;} break;
        case '%': {t = TokenType::OModulus;} break;
        case '=': {t = TokenType::OAssign;} break;
        case '!': {t = TokenType::ONot;} break;
        case '<': {t = TokenType::OLess;} break;
        case '>': {t = TokenType::OGreater;} break;
        case '&': {t = TokenType::OAnd;} break; // provisional
        case '|': {t = TokenType::OOr;} break; // provisional
        case '.': {t = TokenType::OMemberAccess;} break;
    }

    assert(t != TokenType::Error);

    s = s.Next();
    if (s.Eof()) {return EndSingleCharaterToken(t, std::move(s), begin);}

    switch (s.Current())
    {
        case '+':
        {
            if (t == TokenType::OAdd)           {t = TokenType::OIncrement;}
            else {return EndSingleCharaterToken(t, std::move(s), begin);}
        }
        break;
        case '-':
        {
            if (t == TokenType::OSubtract)      {t = TokenType::ODecrement;}
            else {return EndSingleCharaterToken(t, std::move(s), begin);}
        }
        break;
        case '/':
        {
            if (t == TokenType::ODivide)
            {
                return LexerState::Simple(TokenType::Comment, ConsumeComment(std::move(s)), begin);
            }
            else
            {
                return EndSingleCharaterToken(t, std::move(s), begin);
            }
        }
        case '=':
        {
            switch (t)
            {
                case TokenType::OAdd:           {t = TokenType::OAddAssign;} break;
                case TokenType::OSubtract:      {t = TokenType::OSubtractAssign;} break;
                case TokenType::OMultiply:      {t = TokenType::OMultiplyAssign;} break;
                case TokenType::ODivide:        {t = TokenType::ODivideAssign;} break;
                case TokenType::OModulus:       {t = TokenType::OModulusAssign;} break;
                case TokenType::OAssign:        {t = TokenType::OEqual;} break;
                case TokenType::ONot:           {t = TokenType::ONotEqual;} break;
                case TokenType::OLess:          {t = TokenType::OLessEqual;} break;
                case TokenType::OGreater:       {t = TokenType::OGreaterEqual;} break;
                default: return EndSingleCharaterToken(t, std::move(s), begin);
            }
        }
        break;
        case '&':
        {
            if (t == TokenType::OAnd)           {t = TokenType::OAnd;}
            else {return EndSingleCharaterToken(t, std::move(s), begin);}
        }
        break;
        case '|':
        {
            if (t == TokenType::OOr)            {t = TokenType::OOr;}
            else {return EndSingleCharaterToken(t, std::move(s), begin);}
        }
        break;
        case '.':
        {
            if (t == TokenType::OMemberAccess)  {t = TokenType::OAppend;}
            else {return EndSingleCharaterToken(t, std::move(s), begin);}
        }
        break;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
        {
            if (t == TokenType::OMemberAccess)  {return AdvanceOverNumber(std::move(s), true);}
            else {return EndSingleCharaterToken(t, std::move(s), begin);}
        }
        default: return EndSingleCharaterToken(t, std::move(s), begin);
    }

    s = s.Next();
    if (s.Eof()) {return LexerState::Simple(t, std::move(s), begin);}

    switch (s.Current())
    {
        case '=':
        {
            switch (t)
            {
                case TokenType::OAnd:           {t = TokenType::OAndAssign;} break;
                case TokenType::OOr:            {t = TokenType::OOrAssign;} break;
                case TokenType::OAppend:        {t = TokenType::OAppendAssign;} break;
                default: return LexerState::Simple(t, std::move(s), begin);
            }
        }
        break;
        default: return LexerState::Simple(t, std::move(s), begin);
    }

    return LexerState::Simple(t, s.Next(), begin);
}

LexerState Lexer::AdvanceOverString(SourceState&& s) noexcept
{
    auto escapeCount = 0u;
    auto begin = s.Cursor();
    auto delimiter = s.Current();

    s = s.Next();

    auto contentBegin = s.Cursor();
    auto contentEnd = s.Cursor();

    while (true)
    {
        if (s.Eof())
        {
            return LexerState::Error(ErrorType::UnterminatedString, std::move(s), begin);
        }

        auto c = s.Current();
        if (c == delimiter)
        {
            s = s.Next();
            break;
        }
        else if (c == '\\')
        {
            ++escapeCount;

            s = s.Next();
            if (s.Eof())
            {
                return LexerState::Error(ErrorType::InvalidEscape, std::move(s), begin);
            }

            switch (s.Current())
            {
                case 'n': case 'r': case 't': case '0':
                case '"': case '\'': case '\\':
                {
                    s = s.Next();
                }
                break;
                default:
                    return LexerState::Error(ErrorType::InvalidEscape, s.Next(), begin);
            }

            contentEnd = s.Cursor();
        }
        else
        {
            s = s.Next();
            contentEnd = s.Cursor();
        }
    }

    auto v = s.TokenString(contentBegin, contentEnd);

    if (escapeCount != 0)
    {
        // TODO this will need the istring buffer api
        std::string buffer{};
        buffer.resize(v.size() - escapeCount); // we lose one character for each escape

        auto copyTo = buffer.begin();
        auto copyFrom = v.begin();
        for (auto i = v.begin(), endI = v.end(); i != endI; ++i)
        {
            if (*i == '\\')
            {
                auto copyAmount = static_cast<size_t>(i - copyFrom);
                assert(static_cast<size_t>(buffer.end() - copyTo) >= copyAmount + 1);
                // the plus one is the space for the escape character

                std::memcpy(&*copyTo, &*copyFrom, copyAmount);

                ++i;
                assert(i != endI);

                auto replace = '!';
                switch (*i)
                {
                    case 'n': {replace = '\n';} break;
                    case 'r': {replace = '\r';} break;
                    case 't': {replace = '\t';} break;
                    case '0': {replace = '\0';} break;
                    case '"': {replace = '"';} break;
                    case '\'': {replace = '\'';} break;
                    case '\\': {replace = '\\';} break;
                    default: {assert(false);} break;
                }

                copyTo += static_cast<std::string::iterator::difference_type>(copyAmount);
                *copyTo = replace;
                ++copyTo;

                copyFrom = std::next(i);
            }
        }

        // copy the final chunk of data
        auto copyAmount = static_cast<size_t>(v.end() - copyFrom);
        assert(static_cast<size_t>(buffer.end() - copyTo) >= copyAmount);

        std::memcpy(&*copyTo, &*copyFrom, copyAmount);

        v = istring{buffer};
    }

    return LexerState::String(std::move(v), std::move(s), begin);
}

bool Lexer::CheckIdentifier(SourceState const& s) noexcept
{
    switch (s.Current())
    {
        case 'a': case 'A': case 'b': case 'B':
        case 'c': case 'C': case 'd': case 'D':
        case 'e': case 'E': case 'f': case 'F':
        case 'g': case 'G': case 'h': case 'H':
        case 'i': case 'I': case 'j': case 'J':
        case 'k': case 'K': case 'l': case 'L':
        case 'm': case 'M': case 'n': case 'N':
        case 'o': case 'O': case 'p': case 'P':
        case 'q': case 'Q': case 'r': case 'R':
        case 's': case 'S': case 't': case 'T':
        case 'u': case 'U': case 'v': case 'V':
        case 'w': case 'W': case 'x': case 'X':
        case 'y': case 'Y': case 'z': case 'Z':
        case '_':
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            return true;
        default: return false;
    }
}

SourceState Lexer::ConsumeComment(SourceState&& s) noexcept
{
    while (true)
    {
        if (s.Eof()) {return std::move(s);}

        switch (s.Current())
        {
            case '\r':
            case '\n':
            {
                return std::move(s);
            }
            default:
            {
                s = s.Next();
            }
        }
    }
}

SourceState Lexer::ConsumeDigits(SourceState&& s) noexcept
{
    while (true)
    {
        if (s.Eof()) {return std::move(s);}

        switch (s.Current())
        {
            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
            {
                s = s.Next();
            }
            break;
            default:
            {
                return std::move(s);
            }
        }
    }
}

SourceState Lexer::ConsumeUnderscore(SourceState&& s) noexcept
{
    while (true)
    {
        if (s.Eof()) {return std::move(s);}

        switch (s.Current())
        {
            case '_':
            {
                s = s.Next();
            }
            break;
            default:
            {
                return std::move(s);
            }
        }
    }
}

SourceState Lexer::ConsumeWhiteSpace(SourceState&& s) noexcept
{
    while (true)
    {
        if (s.Eof()) {return std::move(s);}

        switch (s.Current())
        {
            case ' ':
            case '\t':
            case '\n':
            {
                s = s.Next();
            }
            break;
            case '\r':
            {
                auto ns = s.Next();
                if (ns.Current() == '\n')
                {
                    s = ns.Next();
                }
                else
                {
                    return std::move(s);
                }
            }
            break;
            default:
            {
                return std::move(s);
            }
        }
    }
}

LexerState Lexer::TryRecognizeKeyword(LexerState&& s) noexcept
{
    assert(s.Token().Type() == TokenType::IdVar);

    auto v = s.Token().Value().StringValue();
    auto kw = TokenType::Error;

    if      (v == "atom")           {kw = TokenType::KAtom;}
    else if (v == "break")          {kw = TokenType::KBreak;}
    else if (v == "continue")       {kw = TokenType::KContinue;}
    else if (v == "do")             {kw = TokenType::KDo;}
    else if (v == "else")           {kw = TokenType::KElse;}
    else if (v == "false")          {kw = TokenType::KFalse;}
    else if (v == "for")            {kw = TokenType::KFor;}
    else if (v == "fn")             {kw = TokenType::KFn;}
    else if (v == "if")             {kw = TokenType::KIf;}
    else if (v == "lst")            {kw = TokenType::KLst;}
    else if (v == "nil")            {kw = TokenType::KNil;}
    else if (v == "recurse")        {kw = TokenType::KRecurse;}
    else if (v == "return")         {kw = TokenType::KReturn;}
    else if (v == "tbl")            {kw = TokenType::KTbl;}
    else if (v == "true")           {kw = TokenType::KTrue;}
    else if (v == "var")            {kw = TokenType::KVar;}
    else if (v == "while")          {kw = TokenType::KWhile;}
    else {return std::move(s);}

    assert(kw != TokenType::Error);
    return LexerState::Simple(kw, s.Source(), s.Token().Source().Begin);
}

}
