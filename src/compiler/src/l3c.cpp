#include <cerrno>
#include <cstdio>
#include <string>

#include <istring/istring.h>
#include <istring/string_view.h>
#include <lexer/lexer.h>
#include <lexer/utils.h>
#include <parser/parser.h>
#include <parser/utils.h>

int main(int argc, char* argv[])
{
    if (argc != 2 && argc != 3)
    {
        std::printf("Usage: l3lex [-norewrite] <file>\n");
        return 0;
    }

    size_t currentArg = 1;
    bool doRewrite = true;

    if (argc == 3)
    {
        auto arg = argv[currentArg];
        auto argSize = strlen(arg);
        if (IString::istring_view{"-norewrite"} == IString::istring_view{arg, argSize})
        {
            doRewrite = false;
        }
        else
        {
            std::printf("Invalid arg '%s'\n", argv[currentArg]);
            return 2;
        }
        ++currentArg;
    }

    auto f = fopen(argv[currentArg], "r");
    if (!f)
    {
        std::perror("Failed to open input file");
        return 1;
    }

    std::string buffer;
    while (true)
    {
        static size_t const CHUNK_SIZE = 1024 * 10;

        auto current = buffer.size();
        buffer.resize(buffer.size() + CHUNK_SIZE);

        auto read = std::fread(&buffer[current], 1, CHUNK_SIZE, f);
        if (read != CHUNK_SIZE)
        {
            if (std::feof(f))
            {
                buffer.resize(current + read);
                break;
            }
            else if (std::ferror(f))
            {
                std::printf("Error reading file\n");
                return 1;
            }
        }
    }

    auto text = IString::istring{buffer};
    auto l = L3::Lexer::Init(text);
    auto p = L3::Parser::Init(text);

    while (!p.Terminal())
    {
        l = L3::Lexer::Advance(std::move(l));
        p = L3::Parser::Consume(std::move(p), l.Token());
    }

    if (!doRewrite)
    {
        PrintNode(p.Ast());
        if (!p.Stack().empty())
        {
            std::printf("*************\n");
            PrintParseStack(p.Stack());
        }
        return 0;
    }

    auto r = L3::AstRewrite::Rewrite(p.Ast());
    PrintNode(r);

    return 0;
}
