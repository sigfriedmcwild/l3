#include <compiler/compiler.h>

#include <cassert>
#include <string>

namespace L3
{

namespace
{

struct CompilerInternal
{
    static void FindVariables(Node const& ast, Environment& e) noexcept;

    static void CompileAssignment(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileAtom(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileBinaryOperator(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileBlock(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileCall(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileConstructor(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileIf(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileIncrement(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileLoop(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileMemberAccess(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileNumber(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileOperatorAssignment(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileRecurse(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileReturn(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileString(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileSelfCall(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileTree(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileUnaryOperator(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileVariableDefinition(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileVariableRead(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileVariableWrite(Node const& ast, CompilerContext& ctx) noexcept;

    static void Emit(Instruction i, CompilerContext& ctx) noexcept;
    static void EmitCount(Instruction i, int64_t count, CompilerContext& ctx) noexcept;
    static void EmitGlobalIndex(Instruction i, int64_t index, CompilerContext& ctx) noexcept;
    static size_t EmitJump(size_t targetAddress, bool relative, CompilerContext& ctx) noexcept;
    static void EmitStackIndex(Instruction i, int64_t index, CompilerContext& ctx) noexcept;
    template<InstructionArgumentType T>
    static void EmitValue(
        Instruction i,
        InstructionArgumentConverter<T> const& c,
        CompilerContext& ctx
    ) noexcept;

    static void EmitInstruction(Instruction i, CompilerContext& ctx) noexcept;

    static void UpdateJumpAddress(
        size_t jumpAddress,
        size_t targetAddress,
        bool relative,
        CompilerContext& ctx
    ) noexcept;
};

}

InstructionInfo InstructionMetadata(Instruction instruction) noexcept
{
#define I(i, t, push, pop) InstructionInfo{InstructionArgumentType::t, push, pop},
    static InstructionInfo const metadata[] =
    {
        INSTRUCTIONS
    };
#undef I

    static_assert(std::size(metadata) < 64, "Too many instructions defined");

    return metadata[static_cast<size_t>(instruction)];
}

int64_t AtomTableBuilder::Lookup(Node const& n) const noexcept
{
    auto i = m_indexes.find(n);
    assert(i != m_indexes.end());
    assert(i->second < m_atoms.size());
    return static_cast<int64_t>(i->second);
}

void AtomTableBuilder::Add(Node const& n, std::string&& s) noexcept
{
    auto i = m_atoms.size();
    auto res0 = m_atoms.emplace(std::move(s), i);
    if (!res0.second) { i = res0.first->second; }

    auto res1 = m_indexes.emplace(n, i);
    assert(res1.second);
}

AtomTable AtomTableBuilder::Make(AtomTableBuilder&& atb) noexcept
{
    AtomTable at{};
    at.resize(atb.m_atoms.size());

    for (auto& kv : atb.m_atoms)
    {
        at[kv.second] = std::move(kv.first);
    }

    return at;
}

int64_t StringTableBuilder::Lookup(Node const& n) const noexcept
{
    auto i = m_indexes.find(n);
    assert(i != m_indexes.end());
    assert(i->second < m_strings.size());
    return static_cast<int64_t>(i->second);
}

void StringTableBuilder::Add(Node const& n, std::string&& s) noexcept
{
    auto i = m_strings.size();
    auto res0 = m_strings.emplace(std::move(s), i);
    if (!res0.second) { i = res0.first->second; }

    auto res1 = m_indexes.emplace(n, i);
    assert(res1.second);
}

StringTable StringTableBuilder::Make(StringTableBuilder&& stb) noexcept
{
    StringTable st{};
    st.resize(stb.m_strings.size());

    for (auto& kv : stb.m_strings)
    {
        st[kv.second] = std::move(kv.first);
    }

    return st;
}

Environment::operator bool() const noexcept
{
    return static_cast<bool>(m_impl);
}

VariableInfo Environment::Lookup(Node const& n) const noexcept
{
    assert(m_impl);
    assert(n.Type() == NodeType::DefinitionVariable || n.Type() == NodeType::IdVar);

    istring v;
    if (n.Type() == NodeType::DefinitionVariable)
    {
        assert(n.Children().size() == 1);
        assert(!n.Value());

        v = n.Children()[0].Value().StringValue();
    }
    else
    {
        v = n.Value().StringValue();
    }

    return Lookup(v);
}

VariableInfo Environment::Lookup(istring const& id) const noexcept
{
    assert(m_impl);
    assert(!id.empty());

    auto i = m_impl->LocalScope.find(id);

    if (i == m_impl->LocalScope.end())
    {
        if (!m_impl->ParentScope)
        {
            assert(false); // TODO report compilation error
        }

        return m_impl->ParentScope.Lookup(id);
    }

    return i->second;
}

std::pair<istring const, VariableInfo> const& Environment::ReverseLookup(int64_t i) const noexcept
{
    assert(m_impl);
    assert(-FormalParameterCount() <= i && i <= VariableCount());
    assert(i != 0);

    for (auto& kv : m_impl->LocalScope)
    {
        if (kv.second.Index == i)
        {
            return kv;
        }
    }

    if (m_impl->ParentScope)
    {
        return m_impl->ParentScope.ReverseLookup(i);
    }

    assert(false); // this should never happen, variable indeces are dense
}

void Environment::Prepare(Node const& n) noexcept
{
    assert(m_impl);
    assert(n.Type() == NodeType::DefinitionVariable);
    assert(n.Children().size() == 1);
    assert(!n.Value());

    auto v = n.Children()[0].Value().StringValue();
    assert(!v.empty());

    auto i = NextPrepareVarIndex();

    auto res = m_impl->LocalScope.emplace(v, VariableInfo{n, i, m_impl->IsGlobal});
    if (!res.second)
    {
        assert(false); // TODO report compilation error
    }
}

void Environment::Declare(Node const& n) noexcept
{
    assert(m_impl);
    assert(n.Type() == NodeType::DefinitionVariable);
    assert(n.Children().size() == 1);
    assert(!n.Value());

    auto v = n.Children()[0].Value().StringValue();
    assert(!v.empty());

    auto i = NextDeclareVarIndex();

    auto res = m_impl->LocalScope.emplace(v, VariableInfo{n, i, m_impl->IsGlobal});
    if (!res.second)
    {
        assert(false);
    }
}

void Environment::DeclareFormalParameter(Node const& n) noexcept
{
    assert(m_impl);
    assert(m_impl->IsScopeRoot);
    assert(!m_impl->IsGlobal);
    assert(n.Type() == NodeType::IdVar);

    auto v = n.Value().StringValue();
    assert(!v.empty());

    auto i = -NextDeclareArgIndex();

    auto res = m_impl->LocalScope.emplace(v, VariableInfo{n, i, false});
    if (!res.second)
    {
        assert(false);
    }
}

void Environment::DeclareGlobal(istring const& id) noexcept
{
    assert(m_impl);
    assert(!id.empty());

    NextPrepareVarIndex();
    auto i = NextDeclareVarIndex();

    auto res = m_impl->LocalScope.emplace(id, VariableInfo{Node{}, i, m_impl->IsGlobal});
    if (!res.second)
    {
        assert(false);
    }
}

int64_t Environment::VariableCount() const noexcept
{
    assert(m_impl);
    if (!m_impl->IsScopeRoot) {return m_impl->ParentScope.VariableCount();}
    return m_impl->PreparedVarCount;
}

int64_t Environment::FormalParameterCount() const noexcept
{
    assert(m_impl);
    if (!m_impl->IsScopeRoot) {return m_impl->ParentScope.FormalParameterCount();}
    return m_impl->DeclaredArgCount;
}

Environment Environment::New(Environment const& e) noexcept
{
    auto n = Child(e);
    n.m_impl->IsScopeRoot = true;
    return n;
}

Environment Environment::Child(Environment const& e) noexcept
{
    Environment c{};
    c.m_impl = std::make_shared<Impl>();
    c.m_impl->ParentScope = e;
    return c;
}

Environment Environment::Clone(Environment const& e) noexcept
{
    assert(e);
    Environment c{};
    c.m_impl = std::make_shared<Impl>(*e.m_impl);
    return c;
}

Environment Environment::Global() noexcept
{
    auto e = New(Environment{});
    e.m_impl->IsGlobal = true;
    return e;
}

int64_t Environment::NextPrepareVarIndex() noexcept
{
    assert(m_impl);
    if (!m_impl->IsScopeRoot) {return m_impl->ParentScope.NextPrepareVarIndex();}
    ++m_impl->PreparedVarCount;
    return m_impl->PreparedVarCount;
}

int64_t Environment::NextDeclareVarIndex() noexcept
{
    assert(m_impl);
    if (!m_impl->IsScopeRoot) {return m_impl->ParentScope.NextDeclareVarIndex();}
    assert(m_impl->DeclaredVarCount < m_impl->PreparedVarCount);
    ++m_impl->DeclaredVarCount;
    return m_impl->DeclaredVarCount;
}

int64_t Environment::NextDeclareArgIndex() noexcept
{
    assert(m_impl);
    assert(m_impl->IsScopeRoot);
    ++m_impl->DeclaredArgCount;
    return m_impl->DeclaredArgCount;
}

CompilerContext::CompilerContext(
    Environment const& e,
    FunctionList const& fl,
    AtomTableBuilder&& atb,
    StringTableBuilder&& stb
) noexcept
    : m_sharedData{std::make_shared<SharedData>()}, m_environment{e}
{
    for (auto i = 0u; i < fl.size(); ++i)
    {
        auto& f = fl[i];
        auto res = m_sharedData->Functions.emplace(f, FunctionInfo{f, i, 0, 0, false});
        assert(res.second);
    }

    m_sharedData->Atoms = std::move(atb);
    m_sharedData->Strings = std::move(stb);
}

Bytecode& CompilerContext::Code() noexcept
{
    assert(m_sharedData);
    return m_sharedData->Code;
}

Bytecode const& CompilerContext::Code() const noexcept
{
    assert(m_sharedData);
    return m_sharedData->Code;
}

FunctionInfoMap& CompilerContext::Functions() noexcept
{
    assert(m_sharedData);
    return m_sharedData->Functions;
}

FunctionInfoMap const& CompilerContext::Functions() const noexcept
{
    assert(m_sharedData);
    return m_sharedData->Functions;
}

AtomTableBuilder const& CompilerContext::Atoms() const noexcept
{
    assert(m_sharedData);
    return m_sharedData->Atoms;
}

StringTableBuilder const& CompilerContext::Strings() const noexcept
{
    assert(m_sharedData);
    return m_sharedData->Strings;
}

Environment& CompilerContext::Variables() noexcept
{
    assert(m_environment);
    return m_environment;
}

Environment const& CompilerContext::Variables() const noexcept
{
    assert(m_environment);
    return m_environment;
}

CompilerContext CompilerContext::SubScope(bool scopeRoot) const noexcept
{
    if (scopeRoot)
    {
        return CompilerContext{m_sharedData, Environment::New(m_environment)};
    }
    else
    {
        return CompilerContext{m_sharedData, Environment::Child(m_environment)};
    }
}

Bytecode CompilerContext::EjectCode() noexcept
{
    assert(m_sharedData);
    return std::move(m_sharedData->Code);
}

AtomTable CompilerContext::EjectAtomTable() noexcept
{
    assert(m_sharedData);
    return AtomTableBuilder::Make(std::move(m_sharedData->Atoms));
}

StringTable CompilerContext::EjectStringTable() noexcept
{
    assert(m_sharedData);
    return StringTableBuilder::Make(std::move(m_sharedData->Strings));
}

CompilerContext::CompilerContext(
    std::shared_ptr<SharedData> const& s,
    Environment&& e
)
    : m_sharedData{s}, m_environment{std::move(e)}
{}

FunctionList Compiler::FindFunctions(Node const& ast) noexcept
{
    assert(ast.Type() == NodeType::Block);

    // turns out that auto does not play well with recursive lambdas and I'm not
    // paying the cost to make this a std::function (even it is likely to not
    // allocate)
    auto walk = [](Node const& tree, FunctionList& list, auto const& self) noexcept -> void
    {
        if (tree.Type() == NodeType::ConstructorFunction)
        {
            list.push_back(tree);
        }

        for (auto& child : tree.Children())
        {
            self(child, list, self);
        }
    };

    auto l = FunctionList{ast};
    walk(ast, l, walk);
    return l;
}

FunctionTable Compiler::BuildFunctionTable(FunctionList const& fl, FunctionInfoMap const& fim) noexcept
{
    FunctionTable t;
    for (auto& n : fl)
    {
        auto i = fim.find(n);
        assert(i != fim.end());

        t.push_back(i->second);
    }
    return t;
}

AtomTableBuilder Compiler::BuildAtomTable(Node const& ast) noexcept
{
    assert(ast.Type() == NodeType::Block);

    // turns out that auto does not play well with recursive lambdas and I'm not
    // paying the cost to make this a std::function (even it is likely to not
    // allocate)
    auto walk = [](Node const& tree, AtomTableBuilder& table, auto const& self) noexcept -> void
    {
        if (tree.Type() == NodeType::IdAtom)
        {
            auto v = tree.Value().StringValue();
            assert(!v.empty());
            table.Add(tree, std::string{v.data(), v.size()});
        }
        else
        {
            for (auto& child : tree.Children())
            {
                self(child, table, self);
            }
        }
    };

    auto t = AtomTableBuilder{};
    walk(ast, t, walk);
    return t;
}

StringTableBuilder Compiler::BuildStringTable(Node const& ast) noexcept
{
    assert(ast.Type() == NodeType::Block);

    // turns out that auto does not play well with recursive lambdas and I'm not
    // paying the cost to make this a std::function (even it is likely to not
    // allocate)
    auto walk = [](Node const& tree, StringTableBuilder& table, auto const& self) noexcept -> void
    {
        if (tree.Type() == NodeType::String)
        {
            auto s = std::string{};
            for (auto& child : tree.Children())
            {
                assert(child.Type() == NodeType::StringToken);
                auto v = child.Value().StringValue();
                s.append(v.data(), v.size());
            }

            table.Add(tree, std::move(s));
        }
        else
        {
            for (auto& child : tree.Children())
            {
                self(child, table, self);
            }
        }
    };

    auto t = StringTableBuilder{};
    walk(ast, t, walk);
    return t;
}

void Compiler::CompileFunction(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::ConstructorFunction);

    auto& children = ast.Children();
    assert(children.size() == 2);

    auto& args = children[0];
    assert(args.Type() == NodeType::IdVarList);

    auto& block = children[1];
    assert(block.Type() == NodeType::Block);

    auto localCtx = ctx.SubScope(true); // TODO pull env from function info

    // declare all the arguments (in reverse order to obtain the correct indeces)
    for (auto i = args.Children().rbegin(), endI = args.Children().rend(); i != endI; ++i)
    {
        auto& n = *i;
        assert(n.Type() == NodeType::IdVar);
        localCtx.Variables().DeclareFormalParameter(n);
    }

    // update the function info
    {
        auto& functions = localCtx.Functions();
        auto i = functions.find(ast);
        assert(i != functions.end());

        auto address = localCtx.Code().size();
        auto argCount = args.Children().size();

        i->second.Address = address;
        i->second.ArgCount = argCount;
        i->second.IsVarArg = false;
    }

    // scan for locals so that we can preallocate the stack
    CompilerInternal::FindVariables(block, localCtx.Variables());

    auto localCount = localCtx.Variables().VariableCount();
    if (localCount > 0)
        {CompilerInternal::EmitStackIndex(Instruction::Alloc, localCount, localCtx);}

    CompilerInternal::CompileBlock(block, localCtx);

    CompilerInternal::EmitCount(Instruction::Return, 0, localCtx);
}

void Compiler::CompileRoot(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::Block);

    auto localCtx = ctx.SubScope(true);

    CompilerInternal::FindVariables(ast, localCtx.Variables());

    auto localCount = localCtx.Variables().VariableCount();
    if (localCount > 0)
        {CompilerInternal::EmitStackIndex(Instruction::Alloc, localCount, localCtx);}

    CompilerInternal::CompileBlock(ast, localCtx);

    CompilerInternal::EmitCount(Instruction::Return, 0, localCtx);
}

void CompilerInternal::FindVariables(Node const& ast, Environment& e) noexcept
{
    assert(ast.Type() == NodeType::Block);

    // turns out that auto does not play well with recursive lambdas and I'm not
    // paying the cost to make this a std::function (even it is likely to not
    // allocate)
    auto walk = [](Node const& tree, Environment env, auto const& self) noexcept -> void
    {
        if (tree.Type() == NodeType::Block)
        {
            env = Environment::Child(env);
        }
        else if (tree.Type() == NodeType::DefinitionVariable)
        {
            env.Prepare(tree);
        }
        else if (tree.Type() == NodeType::ConstructorFunction)
        {
            // do not recurse in child functions
            return;
        }

        for (auto& child : tree.Children())
        {
            self(child, env, self);
        }
    };

    walk(ast, e, walk);
}

void CompilerInternal::CompileAssignment(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::OAssign);

    auto& children = ast.Children();
    assert(children.size() == 2);

    auto& lvalList = children[0];
    assert(lvalList.Type() == NodeType::LvalList);
    auto& lvals = lvalList.Children();

    auto& values = children[1];
    assert(values.Type() == NodeType::ExpressionList);

    // TODO figure out current stack level (relative to frame)
    //auto desiredLevel = /*current level +*/ lvals.size();

    // stack _
    auto& valueNodes = values.Children();
    assert(!valueNodes.empty());

    for (auto& node : valueNodes)
    {
        CompileTree(node, ctx);
    }
    // stack _:values

    for (auto i = lvals.rbegin(), endI = lvals.rend(); i != endI; ++i)
    {
        auto& lval = *i;

        if (lval.Type() == NodeType::DefinitionVariable)
        {
            CompileVariableDefinition(lval, ctx);
            CompileVariableWrite(lval, ctx);
        }
        else if (lval.Type() == NodeType::IdVar)
        {
            CompileVariableWrite(lval, ctx);
        }
        else if (lval.Type() == NodeType::ObjectSubscript)
        {
            // push table/list and key onto the stack
            auto grandChildren = lval.Children();
            assert(grandChildren.size() == 2);

            CompileTree(grandChildren[0], ctx);
            CompileTree(grandChildren[1], ctx);

            // stack _:v:t:k
            Emit(Instruction::SubscriptWrite, ctx);
        }
        else if (lval.Type() == NodeType::ObjectMemberAccess)
        {
            // push table onto the stack and the atom in the bytecode
            auto grandChildren = lval.Children();
            assert(grandChildren.size() == 2);

            CompileTree(grandChildren[0], ctx);
            auto index = ctx.Atoms().Lookup(grandChildren[1]);

            // stack _:v:t
            InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
            convert.Set(index);
            EmitValue(Instruction::SubscriptMemberWrite, convert, ctx);
        }
        else
        {
            assert(false); // TODO report compilation error
        }
    }
}

void CompilerInternal::CompileAtom(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::IdAtom);

    auto index = ctx.Atoms().Lookup(ast);

    InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
    convert.Set(index);
    EmitValue(Instruction::PushAtom, convert, ctx);
}

void CompilerInternal::CompileBinaryOperator(Node const& ast, CompilerContext& ctx) noexcept
{
    // push the 2 operands on the stack
    auto& children = ast.Children();
    assert(children.size() == 2);

    CompileTree(children[0], ctx);
    CompileTree(children[1], ctx);

    auto op = Instruction::NoOp;
    switch (ast.Type())
    {
        case NodeType::ObjectSubscript:     op = Instruction::SubscriptRead; break;
        case NodeType::OAdd:                op = Instruction::Add; break;
        case NodeType::OSubtract:           op = Instruction::Subtract; break;
        case NodeType::OMultiply:           op = Instruction::Multiply; break;
        case NodeType::ODivide:             op = Instruction::Divide; break;
        case NodeType::OModulus:            op = Instruction::Modulus; break;
        case NodeType::OEqual:              op = Instruction::Equal; break;
        case NodeType::ONotEqual:           op = Instruction::NotEqual; break;
        case NodeType::OLess:               op = Instruction::Less; break;
        case NodeType::OLessEqual:          op = Instruction::LessEqual; break;
        case NodeType::OGreater:            op = Instruction::Greater; break;
        case NodeType::OGreaterEqual:       op = Instruction::GreaterEqual; break;
        case NodeType::OAnd:                op = Instruction::And; break;
        case NodeType::OOr:                 op = Instruction::Or; break;
        case NodeType::ONot:                op = Instruction::Not; break;
        case NodeType::OAppend:             op = Instruction::Append; break;
        default:
            assert(false);
    }

    Emit(op, ctx);
}

void CompilerInternal::CompileBlock(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::Block);

    auto subCtx = ctx.SubScope(false);

    auto& children = ast.Children();

    for (auto& node : children)
    {
        CompileTree(node, subCtx);
    }
}

void CompilerInternal::CompileCall(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::ObjectFunction);

    auto& children = ast.Children();
    assert(children.size() >= 1);

    CompileTree(children[0], ctx);
    for (size_t i = 1, endI = children.size(); i < endI; ++i)
    {
        CompileTree(children[i], ctx);
    }

    InstructionArgumentConverter<InstructionArgumentType::ArgReturn> convert;
    convert.Set(static_cast<int64_t>(children.size() - 1), 1);
    EmitValue(Instruction::Call, convert, ctx);
}

void CompilerInternal::CompileConstructor(Node const& ast, CompilerContext& ctx) noexcept
{
    switch (ast.Type())
    {
        case NodeType::ConstructorList:     Emit(Instruction::PushList, ctx); break;
        case NodeType::ConstructorTable:    Emit(Instruction::PushTable, ctx); break;
        case NodeType::ConstructorFunction:
        {
            auto& functions = ctx.Functions();
            auto i = functions.find(ast);
            assert(i != functions.end());

            InstructionArgumentConverter<InstructionArgumentType::Function> convert;
            convert.Set(i->second.Index);

            EmitValue(Instruction::PushClosure, convert, ctx);

            // TODO save off environment in fuction info to use during compilation
            // TODO how do I know what I need to capture if I haven't compiled the function yet?
            // TODO maybe do a scan for captures (use of vars declared outside the scope root)
        }
        break;
        default:
            assert(false);
    }
}

void CompilerInternal::CompileIf(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::If);

    auto& children = ast.Children();
    assert(!children.empty());

    auto jumpsToEnd = std::vector<size_t>{};

    auto CompileIfCondition = [&](Node const& lAst, CompilerContext& lCtx, bool jumpToEnd)
    {
        assert(lAst.Type() == NodeType::IfCondition);

        auto& lChildren = lAst.Children();
        assert(lChildren.size() == 2);

        auto& lCondition = lChildren[0];
        auto& lBlock = lChildren[1];
        assert(lBlock.Type() == NodeType::Block);

        // compile the condition
        CompileTree(lCondition, lCtx);
        Emit(Instruction::Test, lCtx);
        auto lJumpOverAddress = EmitJump(1337, true, lCtx);

        CompileBlock(lBlock, ctx);
        if (jumpToEnd)
        {
            auto lJumpToEndAddress = EmitJump(1337, true, lCtx);
            jumpsToEnd.push_back(lJumpToEndAddress);
        }

        UpdateJumpAddress(lJumpOverAddress, lCtx.Code().size(), true, lCtx);
    };

    for (size_t i = 0, endI = children.size() - 1; i < endI; ++i)
    {
        CompileIfCondition(children[i], ctx, true);
    }

    auto& last = children.back();
    if (last.Type() == NodeType::IfCondition)
    {
        CompileIfCondition(last, ctx, false);
    }
    else if (last.Type() == NodeType::Block)
    {
        CompileBlock(last, ctx);
    }
    else
    {
        assert(false);
    }

    // update jumps to end
    auto endAddress = ctx.Code().size();
    for (auto i : jumpsToEnd)
    {
        UpdateJumpAddress(i, endAddress, true, ctx);
    }
}

void CompilerInternal::CompileIncrement(Node const& ast, CompilerContext& ctx) noexcept
{
    // push the operand on the stack
    auto& children = ast.Children();
    assert(children.size() == 1);
    auto& child = children[0];

    if (child.Type() == NodeType::IdVar)
    {
        // stack _

        auto op = Instruction::NoOp;
        switch (ast.Type())
        {
            case NodeType::OPreIncrement:       op = Instruction::PreIncrement; break;
            case NodeType::OPreDecrement:       op = Instruction::PreDecrement; break;
            case NodeType::OPostIncrement:      op = Instruction::PostIncrement; break;
            case NodeType::OPostDecrement:      op = Instruction::PostDecrement; break;
            default:
                assert(false);
        }

        auto info = ctx.Variables().Lookup(child);
        if (info.IsGlobal)
        {
            assert(false); // TODO report compilation error
        }
        else
        {
            EmitStackIndex(op, info.Index, ctx);
        }
    }
    else if (child.Type() == NodeType::ObjectSubscript)
    {
        // push table/list and key onto the stack
        auto grandChildren = child.Children();
        assert(grandChildren.size() == 2);

        CompileTree(grandChildren[0], ctx);
        CompileTree(grandChildren[1], ctx);

        // stack _:t:k

        auto op = Instruction::NoOp;
        switch (ast.Type())
        {
            case NodeType::OPreIncrement:       op = Instruction::SubscriptPreIncrement; break;
            case NodeType::OPreDecrement:       op = Instruction::SubscriptPreDecrement; break;
            case NodeType::OPostIncrement:      op = Instruction::SubscriptPostIncrement; break;
            case NodeType::OPostDecrement:      op = Instruction::SubscriptPostDecrement; break;
            default:
                assert(false);
        }

        Emit(op, ctx);
    }
    else
    {
        assert(false); // TODO report compilation error
    }
}

void CompilerInternal::CompileLoop(Node const& ast, CompilerContext& ctx) noexcept
{
    switch (ast.Type())
    {
        case NodeType::LoopWhile:
        {
            auto& children = ast.Children();
            assert(children.size() == 2);

            auto& condition = children[0];
            auto& block = children[1];
            assert(block.Type() == NodeType::Block);

            auto loopStart = ctx.Code().size();

            CompileTree(condition, ctx);
            Emit(Instruction::Test, ctx);
            auto jumpToEnd = EmitJump(1337, true, ctx);

            CompileBlock(block, ctx);

            EmitJump(loopStart, true, ctx);

            auto endAddress = ctx.Code().size();
            UpdateJumpAddress(jumpToEnd, endAddress, true, ctx);
        }
        break;
        case NodeType::LoopDo:
        {
            auto& children = ast.Children();
            assert(children.size() == 2);

            auto& condition = children[1];
            auto& block = children[0];
            assert(block.Type() == NodeType::Block);

            auto loopStart = ctx.Code().size();

            CompileBlock(block, ctx);

            CompileTree(condition, ctx);
            Emit(Instruction::TestNot, ctx);
            EmitJump(loopStart, true, ctx);
        }
        break;
        case NodeType::LoopFor:
        {
            assert(false);
        }
        break;
        default:
            assert(false);
    }
}

void CompilerInternal::CompileMemberAccess(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::ObjectMemberAccess);

    auto& children = ast.Children();
    assert(children.size() == 2);

    CompileTree(children[0], ctx);
    auto index = ctx.Atoms().Lookup(children[1]);

    InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
    convert.Set(static_cast<int64_t>(index));
    EmitValue(Instruction::SubscriptMemberRead, convert, ctx);
}

void CompilerInternal::CompileNumber(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::Number);
    assert(ast.Value());
    assert(ast.Children().empty());

    InstructionArgumentConverter<InstructionArgumentType::Number> convert;
    convert.Value() = ast.Value().DoubleValue();

    EmitValue(Instruction::PushNumber, convert, ctx);
}

void CompilerInternal::CompileOperatorAssignment(Node const& /*ast*/, CompilerContext& /*ctx*/) noexcept
{
    // TODO
    assert(false);
}

void CompilerInternal::CompileRecurse(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::Recurse);

    auto argCount = ctx.Variables().FormalParameterCount();
    auto closureIndex = -argCount - 1;

    // TODO check we are compiling a function and not root

    EmitStackIndex(Instruction::Read, closureIndex, ctx);
}

void CompilerInternal::CompileReturn(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::Return);

    auto& children = ast.Children();

    for (auto& node : children)
    {
        CompileTree(node, ctx);
    }

    // TODO handle the case of a multi ret function
    EmitCount(Instruction::Return, static_cast<int64_t>(children.size()), ctx);
}

void CompilerInternal::CompileSelfCall(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::ObjectMemberCall);

    auto& children = ast.Children();
    assert(children.size() >= 2);

    CompileTree(children[0], ctx);

    auto index = ctx.Atoms().Lookup(children[1]);
    InstructionArgumentConverter<InstructionArgumentType::Atom> convertAtom;
    convertAtom.Set(index);
    EmitValue(Instruction::SubscriptSelfCall, convertAtom, ctx);

    for (size_t i = 2, endI = children.size(); i < endI; ++i)
    {
        CompileTree(children[i], ctx);
    }

    InstructionArgumentConverter<InstructionArgumentType::ArgReturn> convertArgRetun;
    convertArgRetun.Set(static_cast<int64_t>(children.size() - 2 + 1), 1);
    EmitValue(Instruction::Call, convertArgRetun, ctx);
}

void CompilerInternal::CompileString(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::String);

    auto index = ctx.Strings().Lookup(ast);

    InstructionArgumentConverter<InstructionArgumentType::String> convert;
    convert.Set(index);

    EmitValue(Instruction::PushString, convert, ctx);
}

void CompilerInternal::CompileTree(Node const& ast, CompilerContext& ctx) noexcept
{
    switch (ast.Type())
    {
        case NodeType::Block:               CompileBlock(ast, ctx); break;
        case NodeType::DefinitionAtom:      // TODO
        case NodeType::DefinitionVariable:  CompileVariableDefinition(ast, ctx); break;
        case NodeType::If:                  CompileIf(ast, ctx); break;
        case NodeType::LoopWhile:
        case NodeType::LoopDo:
        case NodeType::LoopFor:             CompileLoop(ast, ctx); break;
        case NodeType::ObjectFunction:      CompileCall(ast, ctx); break;
        case NodeType::ObjectSubscript:     CompileBinaryOperator(ast, ctx); break;
        case NodeType::ObjectMemberAccess:  CompileMemberAccess(ast, ctx); break;
        case NodeType::ObjectMemberCall:    CompileSelfCall(ast, ctx); break;
        case NodeType::IdAtom:              CompileAtom(ast, ctx); break;
        case NodeType::IdVar:               CompileVariableRead(ast, ctx); break;
        case NodeType::ConstructorList:
        case NodeType::ConstructorTable:
        case NodeType::ConstructorFunction: CompileConstructor(ast, ctx); break;
        case NodeType::String:              CompileString(ast, ctx); break;
        case NodeType::Number:              CompileNumber(ast, ctx); break;
        case NodeType::Break:               // TODO
        case NodeType::Continue:            // TODO
        case NodeType::Recurse:             CompileRecurse(ast, ctx); break;
        case NodeType::Return:              CompileReturn(ast, ctx); break;
        case NodeType::KNil:                Emit(Instruction::PushNil, ctx); break;
        case NodeType::KTrue:               Emit(Instruction::PushTrue, ctx); break;
        case NodeType::KFalse:              Emit(Instruction::PushFalse, ctx); break;
        case NodeType::OAdd:
        case NodeType::OSubtract:
        case NodeType::OMultiply:
        case NodeType::ODivide:
        case NodeType::OModulus:            CompileBinaryOperator(ast, ctx); break;
        case NodeType::ONegate:
        case NodeType::OSize:               CompileUnaryOperator(ast, ctx); break;
        case NodeType::OPreIncrement:
        case NodeType::OPreDecrement:
        case NodeType::OPostIncrement:
        case NodeType::OPostDecrement:      CompileIncrement(ast, ctx); break;
        case NodeType::OEqual:
        case NodeType::ONotEqual:
        case NodeType::OLess:
        case NodeType::OLessEqual:
        case NodeType::OGreater:
        case NodeType::OGreaterEqual:
        case NodeType::OAnd:
        case NodeType::OOr:                 CompileBinaryOperator(ast, ctx); break;
        case NodeType::ONot:                CompileUnaryOperator(ast, ctx); break;
        case NodeType::OAppend:             CompileBinaryOperator(ast, ctx); break;
        case NodeType::OAssign:             CompileAssignment(ast, ctx); break;
        case NodeType::OAddAssign:
        case NodeType::OSubtractAssign:
        case NodeType::OMultiplyAssign:
        case NodeType::ODivideAssign:
        case NodeType::OModulusAssign:
        case NodeType::OAndAssign:
        case NodeType::OOrAssign:
        case NodeType::OAppendAssign:       CompileOperatorAssignment(ast, ctx); break;
        default:
            assert(false);
    }
}

void CompilerInternal::CompileUnaryOperator(Node const& ast, CompilerContext& ctx) noexcept
{
    // push the operand on the stack
    auto& children = ast.Children();
    assert(children.size() == 1);

    CompileTree(children[0], ctx);

    auto op = Instruction::NoOp;
    switch (ast.Type())
    {
        case NodeType::ONot:                op = Instruction::Not; break;
        case NodeType::ONegate:             op = Instruction::Negate; break;
        case NodeType::OSize:               op = Instruction::Size; break;
        default:
            assert(false);
    }

    Emit(op, ctx);
}

void CompilerInternal::CompileVariableDefinition(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::DefinitionVariable);
    ctx.Variables().Declare(ast);
}

void CompilerInternal::CompileVariableRead(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::IdVar);
    auto info = ctx.Variables().Lookup(ast);
    if (info.IsGlobal)
    {
        EmitGlobalIndex(Instruction::GlobalRead, info.Index, ctx);
    }
    else
    {
        EmitStackIndex(Instruction::Read, info.Index, ctx);
    }
}

void CompilerInternal::CompileVariableWrite(Node const& ast, CompilerContext& ctx) noexcept
{
    assert(ast.Type() == NodeType::IdVar || ast.Type() == NodeType::DefinitionVariable);
    auto info = ctx.Variables().Lookup(ast);
    if (info.IsGlobal)
    {
        EmitGlobalIndex(Instruction::GlobalWrite, info.Index, ctx);
    }
    else
    {
        EmitStackIndex(Instruction::Write, info.Index, ctx);
    }

}

void CompilerInternal::Emit(Instruction i, CompilerContext& ctx) noexcept
{
    assert(InstructionMetadata(i).ArgType == InstructionArgumentType::Nil);
    EmitInstruction(i, ctx);
}

void CompilerInternal::EmitCount(Instruction i, int64_t count, CompilerContext& ctx) noexcept
{
    assert(InstructionMetadata(i).ArgType == InstructionArgumentType::Count);

    InstructionArgumentConverter<InstructionArgumentType::Count> convert;
    convert.Set(count);

    EmitValue(i, convert, ctx);
}

void CompilerInternal::EmitGlobalIndex(
    Instruction i,
    int64_t index,
    CompilerContext& ctx
) noexcept
{
    assert(InstructionMetadata(i).ArgType == InstructionArgumentType::GlobalIndex);

    InstructionArgumentConverter<InstructionArgumentType::GlobalIndex> convert;
    convert.Set(index);

    EmitValue(i, convert, ctx);
}

size_t CompilerInternal::EmitJump(size_t targetAddress, bool relative, CompilerContext& ctx) noexcept
{
    auto jumpAddress = ctx.Code().size();
    assert(targetAddress != jumpAddress);

    if (relative)
    {
        auto offset =
            static_cast<int64_t>(targetAddress) - static_cast<int64_t>(jumpAddress + 1);

        EmitInstruction(Instruction::JumpRelative, ctx);
        InstructionArgumentConverter<InstructionArgumentType::RelativeIndex> convert;
        convert.Set(offset);
        convert.Append(ctx.Code());
    }
    else
    {
        EmitInstruction(Instruction::Jump, ctx);
        InstructionArgumentConverter<InstructionArgumentType::AbsoluteIndex> convert;
        convert.Set(static_cast<int64_t>(targetAddress));
        convert.Append(ctx.Code());
    }

    return jumpAddress;
}

void CompilerInternal::EmitStackIndex(Instruction i, int64_t index, CompilerContext& ctx) noexcept
{
    assert(InstructionMetadata(i).ArgType == InstructionArgumentType::StackIndex);
    assert(index != 0 || i == Instruction::Alloc);
    assert(index >= 0 || i != Instruction::Alloc);

    InstructionArgumentConverter<InstructionArgumentType::StackIndex> convert;
    convert.Set(index);

    EmitValue(i, convert, ctx);
}

template<InstructionArgumentType T>
void CompilerInternal::EmitValue(
    Instruction i,
    InstructionArgumentConverter<T> const& c,
    CompilerContext& ctx
) noexcept
{
    EmitInstruction(i, ctx);
    c.Append(ctx.Code());
}

void CompilerInternal::EmitInstruction(Instruction i, CompilerContext& ctx) noexcept
{
    ctx.Code().push_back(static_cast<uint8_t>(i));
}

void CompilerInternal::UpdateJumpAddress(
    size_t jumpAddress,
    size_t targetAddress,
    bool relative,
    CompilerContext& ctx
) noexcept
{
    assert(targetAddress != jumpAddress);

    if (relative)
    {
        auto offset =
            static_cast<int64_t>(targetAddress) - static_cast<int64_t>(jumpAddress + 1);

        InstructionArgumentConverter<InstructionArgumentType::RelativeIndex> convert;
        convert.Set(offset);
        convert.Update(ctx.Code(), jumpAddress + 1);
    }
    else
    {
        InstructionArgumentConverter<InstructionArgumentType::AbsoluteIndex> convert;
        convert.Set(static_cast<int64_t>(targetAddress));
        convert.Update(ctx.Code(), jumpAddress + 1);
    }
}


}
