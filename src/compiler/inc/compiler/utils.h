#pragma once

#include <cstdio>
#include <map>

#include <common/utils.h>
#include <istring/string_view.h>
#include <istring/utility.h>
#include <compiler/compiler.h>

namespace L3
{

using IString::istring_view;

inline
istring_view InstructionName(Instruction t) noexcept
{
    switch (t)
    {
#define I(i, t, push, pop) case Instruction::i: return #i;
        INSTRUCTIONS
#undef I
    }
}

inline
void PrintDisassembly(
    Bytecode const& code,
    FunctionTable const& functions,
    Environment const& globals,
    AtomTable const& atoms,
    StringTable const& strings
) noexcept
{
    std::map<size_t, FunctionInfo> functionLookup;
    for (auto& fi : functions)
    {
        auto res = functionLookup.emplace(fi.Address, fi);
        assert(res.second);
    }

    std::printf("*******\n");

    for (size_t i = 0, endI = code.size(); i < endI; /* custom increment */)
    {
        auto fIt = functionLookup.find(i);
        if (fIt != functionLookup.end())
        {
            if (i != 0) { std::printf("\n"); }
            std::printf("// fn args: %llu\n", fIt->second.ArgCount);
        }

        auto instruction = static_cast<Instruction>(code[i]);
        auto metadata = InstructionMetadata(instruction);
        auto name = InstructionName(instruction);

        ++i;

        char const pad[] = "                          ";
        auto padSize = static_cast<int>(20 - name.size());
        std::printf("0x%04zx %.*s%.*s", i, PRINTF_STR(name), padSize, pad);

        auto WriteStackChange = [](InstructionInfo const& m, int64_t extraPop, int64_t extraPush)
        {
            auto pop = -static_cast<int>(m.Pop) + -extraPop;
            auto push = static_cast<int>(m.Push) + extraPush;

            if (pop != 0 || push != 0)
            {
                std::printf("%+3lld %+3lld", pop, push);
            }
            else
            {
                std::printf("       ");
            }
        };

        switch (metadata.ArgType)
        {
            case InstructionArgumentType::Nil:
            {
                WriteStackChange(metadata, 0, 0);
            }
            break;
            case InstructionArgumentType::AbsoluteIndex:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::AbsoluteIndex> convert;
                convert.Read(code, i);
                std::printf("\t0x%04llx", static_cast<uint64_t>(convert.Get()));
            }
            break;
            case InstructionArgumentType::RelativeIndex:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::RelativeIndex> convert;
                auto jumpBase = static_cast<int64_t>(i);
                convert.Read(code, i);
                auto jumpTarget = static_cast<size_t>(jumpBase + convert.Get());
                std::printf("\t%lld\t -> 0x%04zx", convert.Get(), jumpTarget);
            }
            break;
            case InstructionArgumentType::StackIndex:
            {
                InstructionArgumentConverter<InstructionArgumentType::StackIndex> convert;
                convert.Read(code, i);

                if (instruction == Instruction::Alloc)
                {
                    WriteStackChange(metadata, 0, convert.Get());
                }
                else
                {
                    WriteStackChange(metadata, 0, 0);
                }

                std::printf("\t%lld", convert.Get());
            }
            break;
            case InstructionArgumentType::Count:
            {
                InstructionArgumentConverter<InstructionArgumentType::Count> convert;
                convert.Read(code, i);

                WriteStackChange(metadata, convert.Get(), 0);

                std::printf("\t%lld", convert.Get());
            }
            break;
            case InstructionArgumentType::Atom:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
                convert.Read(code, i);
                auto index = static_cast<uint32_t>(convert.Get());
                std::printf("\t%u\t -> @%.*s", index, PRINTF_STR(atoms[index]));
            }
            break;
            case InstructionArgumentType::Number:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::Number> convert;
                convert.Read(code, i);
                std::printf("\t%f", convert.Value());
            }
            break;
            case InstructionArgumentType::String:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::String> convert;
                convert.Read(code, i);
                auto index = static_cast<uint32_t>(convert.Get());
                std::printf("\t%u\t -> '%.*s'", index, PRINTF_STR(strings[index]));
            }
            break;
            case InstructionArgumentType::Function:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::Function> convert;
                convert.Read(code, i);
                auto& info = functions[static_cast<size_t>(convert.Get())];
                std::printf("\t%lld\t -> 0x%04zx", convert.Get(), info.Address);
            }
            break;
            case InstructionArgumentType::GlobalIndex:
            {
                WriteStackChange(metadata, 0, 0);

                InstructionArgumentConverter<InstructionArgumentType::GlobalIndex> convert;
                convert.Read(code, i);
                auto& info = globals.ReverseLookup(convert.Get());
                std::printf("\t%lld\t -> %.*s", convert.Get(), PRINTF_STR(info.first));
            }
            break;
            case InstructionArgumentType::ArgReturn:
            {
                InstructionArgumentConverter<InstructionArgumentType::ArgReturn> convert;
                convert.Read(code, i);

                WriteStackChange(
                    metadata,
                    static_cast<int64_t>(convert.Args()),
                    static_cast<int64_t>(convert.Returns())
                );

                std::printf("\t%lld, %lld", convert.Args(), convert.Returns());
            }
            break;
        }

        std::printf("\n");
    }

    std::printf("*******\n");
}

inline
void PrintDisassembly(CompilerContext& ctx, FunctionList const& functions) noexcept
{
    auto ft = Compiler::BuildFunctionTable(functions, ctx.Functions());
    auto at = ctx.EjectAtomTable();
    auto st = ctx.EjectStringTable();
    PrintDisassembly(ctx.Code(), ft, ctx.Variables(), at, st);
}

}
