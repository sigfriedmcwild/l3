#pragma once

#include <map>
#include <memory>
#include <vector>

#include <compiler/bytecode.h>
#include <compiler/instruction.h>
#include <istring/istring.h>
#include <parser/parser.h>

namespace L3
{

using IString::istring;

#if 0 // Ideas about serialization
namespace Binary
{

// dependencies:
// - header (fixed): everything
// - string table (variable): nothing
// - block (variable): string table (could build in parallel), parent environment
// - debug info (variable): everything

struct Header
{
    char Marker[2];
    uint16_t Version;
    uint32_t EntryPoint;
    uint32_t DebugInfo;
};

static_assert(sizeof(Header) == 96, "Unexpected padding");

struct String
{
    uint32_t Size;
    char Text[1];
}

}

uint32_t const STRING_TABLE_BASE = sizeof(Binary::Header);
#endif

struct FunctionInfo // TODO split in compile time and runtime info (smaller types for runtime)
{
    Node Ast; // Compile only
    int64_t Index; // Compile only
    size_t Address;
    uint64_t ArgCount;
    bool IsVarArg;
};

using AtomTable = std::vector<std::string>;

using FunctionList = std::vector<Node>;
using FunctionInfoMap = std::map<Node, FunctionInfo>;
using FunctionTable = std::vector<FunctionInfo>;

using StringTable = std::vector<std::string>;

class AtomTableBuilder
{
public:
    AtomTableBuilder() noexcept = default;
    AtomTableBuilder(AtomTableBuilder const&) noexcept = delete;
    AtomTableBuilder(AtomTableBuilder&&) noexcept = default;
    AtomTableBuilder& operator=(AtomTableBuilder const&) noexcept = delete;
    AtomTableBuilder& operator=(AtomTableBuilder&&) noexcept = default;

    int64_t Lookup(Node const& n) const noexcept;

    void Add(Node const& n, std::string&& s) noexcept;

    static AtomTable Make(AtomTableBuilder&& atb) noexcept;

private:
    std::map<Node, uint64_t> m_indexes;
    std::map<std::string, uint64_t> m_atoms;
};

class StringTableBuilder
{
public:
    StringTableBuilder() noexcept = default;
    StringTableBuilder(StringTableBuilder const&) noexcept = delete;
    StringTableBuilder(StringTableBuilder&&) noexcept = default;
    StringTableBuilder& operator=(StringTableBuilder const&) noexcept = delete;
    StringTableBuilder& operator=(StringTableBuilder&&) noexcept = default;

    int64_t Lookup(Node const& n) const noexcept;

    void Add(Node const& n, std::string&& s) noexcept;

    static StringTable Make(StringTableBuilder&& stb) noexcept;

private:
    std::map<Node, uint64_t> m_indexes;
    std::map<std::string, uint64_t> m_strings{{"", 0}};
};

struct VariableInfo
{
    Node Ast;
    int64_t Index;
    bool IsGlobal;
};

class Environment
{
public:
    Environment() noexcept = default;
    Environment(Environment const&) noexcept = default;
    Environment(Environment&&) noexcept = default;
    Environment& operator=(Environment const&) noexcept = default;
    Environment& operator=(Environment&&) noexcept = default;

    explicit operator bool() const noexcept;

    VariableInfo Lookup(Node const& n) const noexcept;
    VariableInfo Lookup(istring const& id) const noexcept;

    std::pair<istring const, VariableInfo> const& ReverseLookup(int64_t i) const noexcept;

    void Prepare(Node const& n) noexcept;
    void Declare(Node const& n) noexcept;
    void DeclareFormalParameter(Node const& n) noexcept;
    void DeclareGlobal(istring const& id) noexcept;

    int64_t VariableCount() const noexcept;
    int64_t FormalParameterCount() const noexcept;

    static Environment New(Environment const& e) noexcept;
    static Environment Child(Environment const& e) noexcept;
    static Environment Clone(Environment const& e) noexcept;
    static Environment Global() noexcept;

private:
    int64_t NextPrepareVarIndex() noexcept;
    int64_t NextDeclareVarIndex() noexcept;
    int64_t NextDeclareArgIndex() noexcept;

private:
    struct Impl;

    std::shared_ptr<Impl> m_impl;
};

struct Environment::Impl
{
    Impl() = default;
    Impl(Impl const&) = default;
    Impl(Impl&&) = delete;
    Impl operator=(Impl const&) = delete;
    Impl operator=(Impl&&) = delete;

    std::map<istring, VariableInfo> LocalScope;
    Environment ParentScope;
    int64_t PreparedVarCount = 0;
    int64_t DeclaredVarCount = 0;
    int64_t DeclaredArgCount = 0;
    bool IsScopeRoot = false;
    bool IsGlobal = false;
};

class CompilerContext
{
public:
    CompilerContext() noexcept = delete;
    CompilerContext(CompilerContext const&) noexcept = delete;
    CompilerContext(CompilerContext&&) noexcept = default;
    CompilerContext& operator=(CompilerContext const&) noexcept = delete;
    CompilerContext& operator=(CompilerContext&&) noexcept = default;

    CompilerContext(
        Environment const& e,
        FunctionList const& fl,
        AtomTableBuilder&& atb,
        StringTableBuilder&& stb
    ) noexcept;

    Bytecode& Code() noexcept;
    Bytecode const& Code() const noexcept;
    FunctionInfoMap& Functions() noexcept;
    FunctionInfoMap const& Functions() const noexcept;
    AtomTableBuilder const& Atoms() const noexcept;
    StringTableBuilder const& Strings() const noexcept;
    Environment& Variables() noexcept;
    Environment const& Variables() const noexcept;

    CompilerContext SubScope(bool scopeRoot) const noexcept;

    Bytecode EjectCode() noexcept;
    AtomTable EjectAtomTable() noexcept;
    StringTable EjectStringTable() noexcept;

private:
    struct SharedData
    {
        Bytecode Code;
        FunctionInfoMap Functions;
        AtomTableBuilder Atoms;
        StringTableBuilder Strings;
    };

    CompilerContext(std::shared_ptr<SharedData> const& s, Environment&& e);

private:
    std::shared_ptr<SharedData> m_sharedData;
    Environment m_environment;
};

class Compiler
{
public:
    static FunctionList FindFunctions(Node const& ast) noexcept;
    static FunctionTable BuildFunctionTable(FunctionList const& fl, FunctionInfoMap const& fim) noexcept;
    static AtomTableBuilder BuildAtomTable(Node const& ast) noexcept;
    static StringTableBuilder BuildStringTable(Node const& ast) noexcept;

    static void CompileFunction(Node const& ast, CompilerContext& ctx) noexcept;
    static void CompileRoot(Node const& ast, CompilerContext& ctx) noexcept;
};

}
