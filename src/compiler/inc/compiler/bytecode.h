#pragma once

#include <array>
#include <cassert>
#include <limits>
#include <vector>

#include <compiler/instruction.h>

namespace L3
{

using Bytecode = std::vector<uint8_t>;

namespace Detail
{

template<class T>
class ByteConverter
{
public:
    T& Value() noexcept
    {
        return m_data.v;
    }

    void Set(int64_t v)
    {
        if (v > std::numeric_limits<T>::max())
        {
            assert(false); // TODO report compilation error
        }
        else if (v < std::numeric_limits<T>::min())
        {
            assert(false); // TODO report compilation error
        }

        m_data.v = static_cast<T>(v);
    }

    int64_t Get() const noexcept
    {
        return static_cast<int64_t>(m_data.v);
    }

    void Read(Bytecode const& code, size_t& i) noexcept
    {
        assert(i + std::size(m_data.bytes) <= code.size());
        m_data.v = *reinterpret_cast<T const*>(code.data() + i);
        i += sizeof(T);
    }

    void Append(Bytecode& code) const noexcept
    {
        code.insert(code.end(), std::begin(m_data.bytes), std::end(m_data.bytes));
    }

    void Update(Bytecode& code, size_t i) const noexcept
    {
        assert(i + std::size(m_data.bytes) <= code.size());
        for (size_t j = 0, endJ = std::size(m_data.bytes); j < endJ; ++j)
        {
            code[i + j] = m_data.bytes[j];
        }
    }

private:
    union
    {
        T v;
        uint8_t bytes[sizeof(T)];
    } m_data;
};

}

template<InstructionArgumentType>
struct InstructionArgumentConverter;

template<>
struct InstructionArgumentConverter<InstructionArgumentType::AbsoluteIndex>
    : public Detail::ByteConverter<uint32_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::RelativeIndex>
    : public Detail::ByteConverter<int16_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::StackIndex>
    : public Detail::ByteConverter<int8_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::Count>
    : public Detail::ByteConverter<uint8_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::Atom>
    : public Detail::ByteConverter<uint32_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::Number>
    : public Detail::ByteConverter<double>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::String>
    : public Detail::ByteConverter<uint32_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::Function>
    : public Detail::ByteConverter<uint32_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::GlobalIndex>
    : public Detail::ByteConverter<uint16_t>
{};

template<>
struct InstructionArgumentConverter<InstructionArgumentType::ArgReturn>
{
public:
    void Set(int64_t argCount, int64_t returns)
    {
        if (argCount < 0)
        {
            assert(false); // TODO report compilation error
        }
        else if (returns < 0)
        {
            assert(false); // TODO report compilation error
        }

        m_argConverter.Set(argCount);
        m_returnConverter.Set(returns);
    }

    uint64_t Args() noexcept
    {
        return static_cast<uint64_t>(m_argConverter.Get());
    }

    uint64_t Returns() noexcept
    {
        return static_cast<uint64_t>(m_returnConverter.Get());
    }

    void Read(Bytecode const& code, size_t& i) noexcept
    {
        m_argConverter.Read(code, i);
        m_returnConverter.Read(code, i);
    }

    void Append(Bytecode& code) const noexcept
    {
        m_argConverter.Append(code);
        m_returnConverter.Append(code);
    }

private:
    Detail::ByteConverter<int8_t> m_argConverter;
    Detail::ByteConverter<int8_t> m_returnConverter;
};

}
