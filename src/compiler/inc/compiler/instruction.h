#pragma once

namespace L3
{

enum class InstructionArgumentType
{
    Nil,            // no argument
    AbsoluteIndex,  // uint32_t (index in bytecode)
    RelativeIndex,  // int16_t  (relative index in bytecode)
    StackIndex,     // int8_t   (index into the stack relative to the frame)
    Count,          // uint8_t  (count for call and returns)
    Atom,           // uint32_t (index in atom table)
    Number,         // double   (numeric constant)
    String,         // uint32_t (index in string table)
    Function,       // uint32_t (index in function table)
    GlobalIndex,    // uint16_t (index in global table)
    ArgReturn,      // uint8_t, uint8_t
};

struct InstructionInfo
{
    InstructionArgumentType ArgType;
    uint8_t Pop;
    uint8_t Push;
};

#define INSTRUCTIONS \
    I(NoOp, Nil, 0, 0) \
    I(Call, ArgReturn, 1, 0) \
    I(Return, Count, 0, 0) \
    I(Read, StackIndex, 0, 1) \
    I(Write, StackIndex, 1, 0) \
    I(GlobalRead, GlobalIndex, 0, 1) \
    I(GlobalWrite, GlobalIndex, 1, 0) \
    I(Alloc, StackIndex, 0, 0) \
    I(Jump, AbsoluteIndex, 0, 0) \
    I(JumpRelative, RelativeIndex, 0, 0) \
    I(Test, Nil, 1, 0) \
    I(TestNot, Nil, 1, 0) \
    I(PushNil, Nil, 0, 1) \
    I(PushAtom, Atom, 0, 1) \
    I(PushNumber, Number, 0, 1) \
    I(PushString, String, 0, 1) \
    I(PushTrue, Nil, 0, 1) \
    I(PushFalse, Nil, 0, 1) \
    I(PushList, Nil, 0, 1) \
    I(PushTable, Nil, 0, 1) \
    I(PushClosure, Function, 0, 1) \
    I(Add, Nil, 2, 1) \
    I(Subtract, Nil, 2, 1) \
    I(Multiply, Nil, 2, 1) \
    I(Divide, Nil, 2, 1) \
    I(Modulus, Nil, 2, 1) \
    I(And, Nil, 2, 1) \
    I(Or, Nil, 2, 1) \
    I(Append, Nil, 2, 1) \
    I(Size, Nil, 1, 1) \
    I(Negate, Nil, 1, 1) \
    I(Not, Nil, 1, 1) \
    I(SubscriptRead, Nil, 2, 1) \
    I(SubscriptWrite, Nil, 3, 1) \
    I(Equal, Nil, 2, 1) \
    I(NotEqual, Nil, 2, 1) \
    I(Less, Nil, 2, 1) \
    I(LessEqual, Nil, 2, 1) \
    I(Greater, Nil, 2, 1) \
    I(GreaterEqual, Nil, 2, 1) \
    I(PreIncrement, StackIndex, 0, 1) \
    I(PreDecrement, StackIndex, 0, 1) \
    I(PostIncrement, StackIndex, 0, 1) \
    I(PostDecrement, StackIndex, 0, 1) \
    I(SubscriptPreIncrement, Nil, 2, 1) \
    I(SubscriptPreDecrement, Nil, 2, 1) \
    I(SubscriptPostIncrement, Nil, 2, 1) \
    I(SubscriptPostDecrement, Nil, 2, 1) \
    I(SubscriptMemberRead, Atom, 1, 1) \
    I(SubscriptMemberWrite, Atom, 1, 1) \
    I(SubscriptSelfCall, Atom, 1, 2) \

#define I(i, t, push, pop) i,
enum class Instruction
{
    INSTRUCTIONS
};
#undef I

InstructionInfo InstructionMetadata(Instruction instruction) noexcept;

}
