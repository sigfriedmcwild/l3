#pragma once

#include <deque>
#include <map>
#include <string>
#include <vector>

#include <common/id.h>
#include <common/intrusive.h>

namespace L3
{

namespace Details
{

struct AtomIdTag;
struct ClosureIdTag;
struct ForeignClosureIdTag;
struct StringRefIdTag;

}

enum class ObjectType
{
    Nil,
    Atom,
    Number,
    StringReference,
    StringValue,
    Bool,
    Array,
    Table,
    Closure,
    ForeignClosure,
    Frame,
};

struct Frame
{
    uint32_t FramePointer;
    uint32_t ReturnPointer;
    uint8_t ArgumentCount;
    uint8_t ReturnCount;
};

class Stack;
class Vm;

typedef int64_t ForeignClosure(Vm& vm, Stack& stack, void* environment);

class Object;

using StringT = std::string;
using ArrayT = std::deque<Object>;
using TableT = std::map<Object, Object>;

using AtomId = Details::IdBase<StringT const*, Details::AtomIdTag>;
using ClosureId = Details::IdBase<uint32_t, Details::ClosureIdTag>;
using ForeignClosureId = Details::IdBase<uint32_t, Details::ForeignClosureIdTag>;
using StringRefId = Details::IdBase<StringT const*, Details::StringRefIdTag>;

class Object
{
public:
    Object() noexcept;
    Object(Object const&) noexcept;
    Object(Object&&) noexcept;
    Object& operator=(Object const&) noexcept;
    Object& operator=(Object&&) noexcept;
    ~Object() noexcept;

    explicit Object(AtomId v) noexcept;
    explicit Object(double v) noexcept;
    explicit Object(StringRefId v) noexcept;
    explicit Object(StringT&& v) noexcept;
    explicit Object(bool v) noexcept;
    explicit Object(ArrayT&& v) noexcept;
    explicit Object(TableT&& v) noexcept;
    explicit Object(ClosureId v) noexcept;
    explicit Object(ForeignClosureId v) noexcept;
    explicit Object(Frame const& v) noexcept;

    ObjectType Type() const noexcept;
    AtomId Atom() const noexcept;
    StringT const& AtomName() const noexcept;
    double Number() const noexcept;
    StringRefId StringReference() const noexcept;
    StringT const& StringValue() const noexcept;
    bool Bool() const noexcept;
    ArrayT& Array() const noexcept;
    TableT& Table() const noexcept;
    ClosureId Closure() const noexcept;
    ForeignClosureId ForeignClosure() const noexcept;
    Frame StackFrame() const noexcept;

private:
    struct FramePart
    {
        uint32_t FramePointer;
        uint32_t ReturnPointer;
    };

private:
    void Set(AtomId v) noexcept;
    void Set(double v) noexcept;
    void Set(StringRefId v) noexcept;
    void Set(IntrusivePtr<StringT>&& v) noexcept;
    void Set(bool v) noexcept;
    void Set(IntrusivePtr<ArrayT>&& v) noexcept;
    void Set(IntrusivePtr<TableT>&& v) noexcept;
    void Set(ClosureId v) noexcept;
    void Set(ForeignClosureId v) noexcept;
    void Set(FramePart const& frame, uint8_t args, uint8_t returns) noexcept;

    void Clear() noexcept;

private:
    union
    {
        AtomId v_atom;
        double v_number;
        StringRefId v_stringReference;
        IntrusivePtr<StringT> v_stringValue;
        bool v_bool;
        IntrusivePtr<ArrayT> v_array;
        IntrusivePtr<TableT> v_table;
        ClosureId v_closure;
        ForeignClosureId v_foreignClosure;
        FramePart v_frame;
    };
    uint8_t m_argumentCount = 0;
    uint8_t m_returnCount = 0;
    ObjectType m_type = ObjectType::Nil;
};

static_assert(sizeof(Object) == 16, "Object size is incorrect");

size_t Size(Object const& obj) noexcept;
std::string Stringify(Object const& obj) noexcept;
bool Truth(Object const& obj) noexcept;
int Compare(Object const& lhs, Object const& rhs) noexcept;

bool operator==(Object const& lhs, Object const& rhs) noexcept;
bool operator!=(Object const& lhs, Object const& rhs) noexcept;
bool operator<(Object const& lhs, Object const& rhs) noexcept;
bool operator<=(Object const& lhs, Object const& rhs) noexcept;
bool operator>(Object const& lhs, Object const& rhs) noexcept;
bool operator>=(Object const& lhs, Object const& rhs) noexcept;

}

