#pragma once

#include <memory>

#include <common/id.h>
#include <compiler/compiler.h> // TODO split environmetn out
#include <istring/istring.h>
#include <vm/object.h>

namespace L3
{

namespace Details
{

struct GlobalIdTag;

}

using IString::istring;

using Bytecode = std::vector<uint8_t>; // TODO move to common header?
using GlobalId = Details::IdBase<uint64_t, Details::GlobalIdTag>;

class VmImpl;

namespace Debug
{

struct Global
{
    GlobalId Id;
    istring Name;
};

}

class Stack
{
public:
    Stack() noexcept = default;
    Stack(Stack const&) noexcept = default;
    Stack(Stack&&) noexcept = default;
    Stack& operator=(Stack const&) noexcept = default;
    Stack& operator=(Stack&&) noexcept = default;
    ~Stack() noexcept;

    Stack(std::shared_ptr<VmImpl> const& impl) noexcept;

    explicit operator bool() const noexcept;

    int64_t Top() const noexcept;
    int64_t Bottom() const noexcept;
    Object Read(int64_t index) const;
    void Write(int64_t index, Object&& v);
    void Push(Object&& v);
    Object Pop();

private:
    std::shared_ptr<VmImpl> m_impl;
};

class Vm
{
public:
    Vm() noexcept;
    Vm(Vm const&) noexcept = delete;
    Vm(Vm&&) noexcept = default;
    Vm& operator=(Vm const&) noexcept = delete;
    Vm& operator=(Vm&&) noexcept = default;
    ~Vm() noexcept;

    Vm(std::shared_ptr<VmImpl> const& impl) noexcept;

    void Load(istring&& code);

    Stack Run();

    Bytecode const& Code() const noexcept;

    AtomId DeclareAtom(std::string const& id) noexcept;

    GlobalId DeclareGlobal(istring const& id);
    Object ReadGlobal(GlobalId i) const;
    void WriteGlobal(GlobalId i, Object&& v);

    Object RegisterForeignClosure(ForeignClosure* fp, void* env) noexcept;
    Object RegisterForeignClosure(istring const& name, ForeignClosure* fp, void* env) noexcept;

public: // Debug info
    Environment DebugGlobalEnvironment() const noexcept;
    Debug::Global DebugGlobal(GlobalId i) const;
    FunctionTable const& DebugFunctionTable() const noexcept;
    AtomTable const& DebugAtomTable() const noexcept;
    StringTable const& DebugStringTable() const noexcept;

private:
    std::shared_ptr<VmImpl> m_impl;
};

}
