#include <cassert>
#include <cstdio>

#include <istring/algorithms.h>
#include <vm/object.h>

namespace L3
{

Object::Object() noexcept
    : v_number{0}, m_argumentCount{0}, m_returnCount{0}, m_type{ObjectType::Nil}
{}

Object::Object(Object const& o) noexcept
    : Object{}
{
    switch (o.m_type)
    {
        case ObjectType::Nil: break;
        case ObjectType::Atom: Set(o.v_atom); break;
        case ObjectType::Number: Set(o.v_number); break;
        case ObjectType::StringReference: Set(o.v_stringReference); break;
        case ObjectType::StringValue: Set(IntrusivePtr<StringT>{o.v_stringValue}); break;
        case ObjectType::Bool: Set(o.v_bool); break;
        case ObjectType::Array: Set(IntrusivePtr<ArrayT>{o.v_array}); break;
        case ObjectType::Table: Set(IntrusivePtr<TableT>{o.v_table}); break;
        case ObjectType::Closure: Set(o.v_closure); break;
        case ObjectType::ForeignClosure: Set(o.v_foreignClosure); break;
        case ObjectType::Frame: Set(o.v_frame, o.m_argumentCount, o.m_returnCount); break;
    }
}

Object::Object(Object&& o) noexcept
    : Object{}
{
    switch (o.m_type)
    {
        case ObjectType::Nil: break;
        case ObjectType::Atom: Set(std::move(o.v_atom)); break;
        case ObjectType::Number: Set(std::move(o.v_number)); break;
        case ObjectType::StringReference: Set(std::move(o.v_stringReference)); break;
        case ObjectType::StringValue: Set(std::move(o.v_stringValue)); break;
        case ObjectType::Bool: Set(std::move(o.v_bool)); break;
        case ObjectType::Array: Set(std::move(o.v_array)); break;
        case ObjectType::Table: Set(std::move(o.v_table)); break;
        case ObjectType::Closure: Set(std::move(o.v_closure)); break;
        case ObjectType::ForeignClosure: Set(std::move(o.v_foreignClosure)); break;
        case ObjectType::Frame: Set(o.v_frame, o.m_argumentCount, o.m_returnCount); break;
    }

    o.Clear();
}

Object& Object::operator=(Object const& o) noexcept
{
    if (this == &o) { return *this; }

    if (m_type != o.m_type) { Clear(); }

    switch (o.m_type)
    {
        case ObjectType::Nil: break;
        case ObjectType::Atom: Set(o.v_atom); break;
        case ObjectType::Number: Set(o.v_number); break;
        case ObjectType::StringReference: Set(o.v_stringReference); break;
        case ObjectType::StringValue: Set(IntrusivePtr<StringT>{o.v_stringValue}); break;
        case ObjectType::Bool: Set(o.v_bool); break;
        case ObjectType::Array: Set(IntrusivePtr<ArrayT>{o.v_array}); break;
        case ObjectType::Table: Set(IntrusivePtr<TableT>{o.v_table}); break;
        case ObjectType::Closure: Set(o.v_closure); break;
        case ObjectType::ForeignClosure: Set(o.v_foreignClosure); break;
        case ObjectType::Frame: Set(o.v_frame, o.m_argumentCount, o.m_returnCount); break;
    }

    return *this;
}


Object& Object::operator=(Object&& o) noexcept
{
    if (this == &o) { return *this; }

    if (m_type != o.m_type) { Clear(); }

    switch (o.m_type)
    {
        case ObjectType::Nil: break;
        case ObjectType::Atom: Set(std::move(o.v_atom)); break;
        case ObjectType::Number: Set(std::move(o.v_number)); break;
        case ObjectType::StringReference: Set(std::move(o.v_stringReference)); break;
        case ObjectType::StringValue: Set(std::move(o.v_stringValue)); break;
        case ObjectType::Bool: Set(std::move(o.v_bool)); break;
        case ObjectType::Array: Set(std::move(o.v_array)); break;
        case ObjectType::Table: Set(std::move(o.v_table)); break;
        case ObjectType::Closure: Set(std::move(o.v_closure)); break;
        case ObjectType::ForeignClosure: Set(std::move(o.v_foreignClosure)); break;
        case ObjectType::Frame: Set(o.v_frame, o.m_argumentCount, o.m_returnCount); break;
    }

    o.Clear();

    return *this;
}

Object::~Object() noexcept
{
    Clear();
}

Object::Object(AtomId v) noexcept: Object{} { Set(v); }
Object::Object(double v) noexcept: Object{} { Set(v); }
Object::Object(StringRefId v) noexcept: Object{} { Set(v); }
Object::Object(StringT&& v) noexcept: Object{} { Set(MakeIntrusive<StringT>(std::move(v))); }
Object::Object(bool v) noexcept: Object{} { Set(v); }
Object::Object(ArrayT&& v) noexcept: Object{} { Set(MakeIntrusive<ArrayT>(std::move(v))); }
Object::Object(TableT&& v) noexcept: Object{} { Set(MakeIntrusive<TableT>(std::move(v))); }
Object::Object(ClosureId v) noexcept: Object{} { Set(v); }
Object::Object(ForeignClosureId v) noexcept: Object{} { Set(v); }
Object::Object(Frame const& v) noexcept
    : Object{}
{
    Set(FramePart{v.FramePointer, v.ReturnPointer}, v.ArgumentCount, v.ReturnCount);
}

ObjectType Object::Type() const noexcept
{
    return m_type;
}

AtomId Object::Atom() const noexcept
{
    assert(m_type == ObjectType::Atom);
    return v_atom;
}

StringT const& Object::AtomName() const noexcept
{
    assert(m_type == ObjectType::Atom);
    return *v_atom.Value();
}

double Object::Number() const noexcept
{
    assert(m_type == ObjectType::Number);
    return v_number;
}

StringRefId Object::StringReference() const noexcept
{
    assert(m_type == ObjectType::StringReference);
    return v_stringReference;
}

StringT const& Object::StringValue() const noexcept
{
    assert(m_type == ObjectType::StringReference || m_type == ObjectType::StringValue);
    if (m_type == ObjectType::StringReference)
    {
        return *v_stringReference.Value();
    }
    else
    {
        return *v_stringValue;
    }
}

bool Object::Bool() const noexcept
{
    assert(m_type == ObjectType::Bool);
    return v_bool;
}

ArrayT& Object::Array() const noexcept
{
    assert(m_type == ObjectType::Array);
    return *v_array;
}

TableT& Object::Table() const noexcept
{
    assert(m_type == ObjectType::Table);
    return *v_table;
}

ClosureId Object::Closure() const noexcept
{
    assert(m_type == ObjectType::Closure);
    return v_closure;
}

ForeignClosureId Object::ForeignClosure() const noexcept
{
    assert(m_type == ObjectType::ForeignClosure);
    return v_foreignClosure;
}

Frame Object::StackFrame() const noexcept
{
    assert(m_type == ObjectType::Frame);
    return Frame{v_frame.FramePointer, v_frame.ReturnPointer, m_argumentCount, m_returnCount};
}

void Object::Set(AtomId v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Atom);
    m_type = ObjectType::Atom;
    v_atom = v;
}

void Object::Set(double v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Number);
    m_type = ObjectType::Number;
    v_number = v;
}

void Object::Set(StringRefId v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::StringReference);
    m_type = ObjectType::StringReference;
    v_stringReference = v;
}

void Object::Set(IntrusivePtr<StringT>&& v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::StringValue);
    assert(v);
    if (m_type == ObjectType::Nil)
    {
        m_type = ObjectType::StringValue;
        new (&v_stringValue) IntrusivePtr<StringT>{std::move(v)};
    }
    else
    {
        v_stringValue = std::move(v);
    }
}

void Object::Set(bool v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Bool);
    m_type = ObjectType::Bool;
    v_bool = v;
}

void Object::Set(IntrusivePtr<ArrayT>&& v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Array);
    assert(v);
    if (m_type == ObjectType::Nil)
    {
        m_type = ObjectType::Array;
        new (&v_array) IntrusivePtr<ArrayT>{std::move(v)};
    }
    else
    {
        v_array = std::move(v);
    }
}

void Object::Set(IntrusivePtr<TableT>&& v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Table);
    assert(v);
    if (m_type == ObjectType::Nil)
    {
        m_type = ObjectType::Table;
        new (&v_table) IntrusivePtr<TableT>{std::move(v)};
    }
    else
    {
        v_table = std::move(v);
    }
}

void Object::Set(ClosureId v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Closure);
    m_type = ObjectType::Closure;
    v_closure = v;
}

void Object::Set(ForeignClosureId v) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::ForeignClosure);
    m_type = ObjectType::ForeignClosure;
    v_foreignClosure = v;
}

void Object::Set(FramePart const& frame, uint8_t args, uint8_t returns) noexcept
{
    assert(m_type == ObjectType::Nil || m_type == ObjectType::Frame);
    m_type = ObjectType::Frame;
    v_frame = frame;
    m_argumentCount = args;
    m_returnCount = returns;
}

void Object::Clear() noexcept
{
    switch (m_type)
    {
        case ObjectType::StringValue:
        {
            v_stringValue.~IntrusivePtr<std::string>();
        }
        break;
        case ObjectType::Array: break;
        case ObjectType::Table: break;
        default:
            // nothing
            break;
    }

    m_type = ObjectType::Nil;
    v_number = 0;
    m_argumentCount = 0;
    m_returnCount = 0;
}

size_t Size(Object const& obj) noexcept
{
    switch (obj.Type())
    {
        case ObjectType::StringReference:
        case ObjectType::StringValue:       return obj.StringValue().size();
        case ObjectType::Array:             return obj.Array().size();
        case ObjectType::Table:             return obj.Table().size();
        default:                            assert(false);
    }
}

std::string Stringify(Object const& obj) noexcept
{
    switch (obj.Type())
    {
        case ObjectType::Nil:               return "nil";
        case ObjectType::Atom:              assert(false);
        case ObjectType::Number:            return std::to_string(obj.Number());
        case ObjectType::StringReference:   assert(false);
        case ObjectType::StringValue:       assert(false);
        case ObjectType::Bool:              return obj.Bool() ? "true" : "false";
        case ObjectType::Array:
        {
            char buffer[64] = {};
            std::sprintf(buffer, "{Array 0x%p}", &obj.Array());
            return buffer;
        }
        case ObjectType::Table:
        {
            char buffer[64] = {};
            std::sprintf(buffer, "{Table 0x%p}", &obj.Table());
            return buffer;
        }
        case ObjectType::Closure:
        {
            char buffer[64] = {};
            std::sprintf(buffer, "{Closure %04d}", obj.Closure().Value());
            return buffer;
        }
        case ObjectType::ForeignClosure:
        {
            char buffer[64] = {};
            std::sprintf(buffer, "{ForeignClosure %04u}", obj.ForeignClosure().Value());
            return buffer;
        }
        case ObjectType::Frame:             assert(false);
    }
}

bool Truth(Object const& obj) noexcept
{
    switch (obj.Type())
    {
        case ObjectType::Nil:               return false;
        case ObjectType::Atom:              return true;
        case ObjectType::Number:            return true;
        case ObjectType::StringReference:   return true;
        case ObjectType::StringValue:       return true;
        case ObjectType::Bool:              return obj.Bool();
        case ObjectType::Array:             return true;
        case ObjectType::Table:             return true;
        case ObjectType::Closure:           return true;
        case ObjectType::ForeignClosure:    return true;
        case ObjectType::Frame:             assert(false);
    }
}

int Compare(Object const& lhs, Object const& rhs) noexcept
{
    if (lhs.Type() == rhs.Type())
    {
        switch (lhs.Type())
        {
            case ObjectType::Nil:
                return 0;
            case ObjectType::Atom:
                return static_cast<int>(reinterpret_cast<intptr_t>(lhs.Atom().Value()) -
                    reinterpret_cast<intptr_t>(rhs.Atom().Value()));
            case ObjectType::Number:
                return static_cast<int>(lhs.Number() - rhs.Number());
            case ObjectType::StringReference:
            case ObjectType::StringValue:
            {
                return lhs.StringValue().compare(rhs.StringValue());
            }
            case ObjectType::Bool:
            {
                auto l = lhs.Bool() ? 1 : 0;
                auto r = rhs.Bool() ? 1 : 0;
                return l - r;
            }
            case ObjectType::Array:
                return static_cast<int>(reinterpret_cast<intptr_t>(&lhs.Table()) -
                    reinterpret_cast<intptr_t>(&rhs.Table()));
            case ObjectType::Table:
                return static_cast<int>(reinterpret_cast<intptr_t>(&lhs.Table()) -
                    reinterpret_cast<intptr_t>(&rhs.Table()));
            case ObjectType::Closure:
                return static_cast<int>(lhs.Closure().Value()) -
                    static_cast<int>(rhs.Closure().Value());
            case ObjectType::ForeignClosure:
                return static_cast<int>(lhs.ForeignClosure().Value()) -
                    static_cast<int>(rhs.ForeignClosure().Value());
            case ObjectType::Frame:
                assert(false);
        }
    }
    else if (
        (lhs.Type() == ObjectType::StringReference && rhs.Type() == ObjectType::StringValue) ||
        (lhs.Type() == ObjectType::StringValue && rhs.Type() == ObjectType::StringReference)
    )
    {
        return lhs.StringValue().compare(rhs.StringValue());
    }
    else
    {
        return static_cast<int>(lhs.Type()) - static_cast<int>(rhs.Type());
    }
}

bool operator==(Object const& lhs, Object const& rhs) noexcept
{
    return Compare(lhs, rhs) == 0;
}

bool operator!=(Object const& lhs, Object const& rhs) noexcept
{
    return Compare(lhs, rhs) != 0;
}

bool operator<(Object const& lhs, Object const& rhs) noexcept
{
    return Compare(lhs, rhs) < 0;
}

bool operator<=(Object const& lhs, Object const& rhs) noexcept
{
    return Compare(lhs, rhs) <= 0;
}

bool operator>(Object const& lhs, Object const& rhs) noexcept
{
    return Compare(lhs, rhs) > 0;
}

bool operator>=(Object const& lhs, Object const& rhs) noexcept
{
    return Compare(lhs, rhs) >= 0;
}

}
