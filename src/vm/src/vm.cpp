#include <vm/vm.h>

#include <cassert>
#include <cmath>
#include <cstdio>
#include <set>
#include <vector>

#include <compiler/compiler.h>
#include <lexer/lexer.h>
#include <parser/parser.h>

#define EXECUTION_TRACE 0
#if EXECUTION_TRACE
#define EXEC_TRACE(...) std::printf(__VA_ARGS__)
#else
#define EXEC_TRACE(...)
#endif

namespace L3
{

struct ForeignClosureInfo
{
    ForeignClosure* Address;
    void* Environment; // non owning
    istring Name;
};

using AtomRuntimeTable = std::set<std::string>;
using AtomRuntimeLookup = std::vector<AtomId>;
using ForeignClosureTable = std::vector<ForeignClosureInfo>;
using GlobalTable = std::vector<Object>;

class VmImpl: public std::enable_shared_from_this<VmImpl>
{
public:
    VmImpl() noexcept;
    VmImpl(VmImpl const&) noexcept = delete;
    VmImpl(VmImpl&&) noexcept = delete;
    VmImpl& operator=(VmImpl const&) noexcept = delete;
    VmImpl& operator=(VmImpl&&) noexcept = delete;

public:
    void Load(istring&& code);

    void Run();

    Bytecode const& Code() const noexcept;

public: // stack operations
    int64_t Top() const noexcept;
    int64_t Bottom() const noexcept;
    Object Read(int64_t index) const;
    void Write(int64_t index, Object&& v) noexcept;

    void Push(Object&& v);
    Object Pop() noexcept;

public: // Atoms
    AtomId DeclareAtom(std::string const& id);

public: // Globals
    GlobalId DeclareGlobal(istring const& id);
    Object ReadGlobal(GlobalId i) const;
    void WriteGlobal(GlobalId i, Object&& v);

public: // Foreign Closures
    Object RegisterForeignClosure(istring const& name, ForeignClosure* fp, void* env) noexcept;

public: // Debug info
    Environment DebugGlobalEnvironment() const noexcept;
    Debug::Global DebugGlobal(GlobalId i) const;
    FunctionTable const& DebugFunctionTable() const noexcept;
    AtomTable const& DebugAtomTable() const noexcept;
    StringTable const& DebugStringTable() const noexcept;

private:
    bool DoReturn(int64_t localReturnCount, size_t& instructionPointer) noexcept;

private:
    Bytecode m_code;
    AtomTable m_atoms;
    StringTable m_strings;
    FunctionTable m_functions;
    ForeignClosureTable m_foreignFunctions;
    GlobalTable m_globals;
    std::vector<Object> m_stack;

    AtomRuntimeTable m_atomsRuntime;
    AtomRuntimeLookup m_atomsRuntimeLookup;

    Environment m_globalEnvironment;

    uint64_t m_framePointer = 0;
    uint64_t m_stackPointer = 0;
    uint64_t m_argCount = 0;
};

VmImpl::VmImpl() noexcept
:
    m_globals{Object{}},
    m_stack{10000},
    m_globalEnvironment{Environment::Global()}
{}

void VmImpl::Load(istring&& code)
{
    auto l = Lexer::Init(code);
    auto p = Parser::Init(code);

    while (!p.Terminal())
    {
        l = Lexer::Advance(std::move(l));
        p = Parser::Consume(std::move(p), l.Token());
    }

    assert(p.Ast().Type() != NodeType::Error); // TODO error

    auto r = AstRewrite::Rewrite(p.Ast());

    auto functionList = Compiler::FindFunctions(r);
    auto atoms = Compiler::BuildAtomTable(r);
    auto strings = Compiler::BuildStringTable(r);

    CompilerContext ctx{m_globalEnvironment, functionList, std::move(atoms), std::move(strings)};

    Compiler::CompileRoot(functionList.front(), ctx);
    for (size_t i = 1, endI = functionList.size(); i < endI; ++i)
    {
        Compiler::CompileFunction(functionList[i], ctx);
    }

    m_code = ctx.EjectCode();
    m_atoms = ctx.EjectAtomTable();
    m_strings = ctx.EjectStringTable();
    m_functions = Compiler::BuildFunctionTable(functionList, ctx.Functions());

    for (auto const& a : m_atoms)
    {
        auto res = m_atomsRuntime.emplace(a);
        m_atomsRuntimeLookup.push_back(AtomId{&*res.first});
    }
}

void VmImpl::Run()
{
    size_t instructionPointer = 0;
    m_framePointer = 0;
    m_argCount = 0;

    while (m_stackPointer > 0)
    {
        Pop();
    }

    EXEC_TRACE("SETUP     ");
    Push(Object(Frame{0, 0, 0, 0}));

    while (true)
    {
        EXEC_TRACE("\nEXEC %04zx ", instructionPointer);
        auto instruction = static_cast<Instruction>(m_code[instructionPointer]);
        ++instructionPointer;

        switch (instruction)
        {
            case Instruction::NoOp:
            break;
            case Instruction::Call:
            {
                InstructionArgumentConverter<InstructionArgumentType::ArgReturn> convert;
                convert.Read(m_code, instructionPointer);

                assert(m_stackPointer - m_framePointer > convert.Args());
                auto fi = m_stackPointer - convert.Args() - 1;

                auto obj = m_stack[static_cast<size_t>(fi)];

                switch (obj.Type())
                {
                    case ObjectType::Closure:
                    {
                        auto& info = m_functions[obj.Closure().Value()];

                        EXEC_TRACE("\tARGS  %lld ", convert.Args());
                        EXEC_TRACE("\tEXP   %lld ", info.ArgCount);
                        EXEC_TRACE("\tBASE  %lld ", m_stackPointer - convert.Args());
                        EXEC_TRACE("\tFN    %lld ", m_stackPointer - convert.Args() - 1);
                        EXEC_TRACE("\tFRAME %lld ", m_stackPointer - convert.Args() + info.ArgCount);

                        if (convert.Args() < info.ArgCount)
                        {
                            for (auto i = convert.Args(); i < info.ArgCount; ++i)
                            {
                                Push(Object{});
                            }
                        }
                        else if (convert.Args() > info.ArgCount)
                        {
                            for (auto i = convert.Args(); i > info.ArgCount; --i)
                            {
                                Pop();
                            }
                        }

                        Push(Object{Frame
                        {
                            static_cast<uint32_t>(m_framePointer),
                            static_cast<uint32_t>(instructionPointer),
                            static_cast<uint8_t>(info.ArgCount),
                            static_cast<uint8_t>(convert.Returns()),
                        }});

                        m_framePointer = m_stackPointer - 1;
                        m_argCount = static_cast<uint8_t>(info.ArgCount);
                        instructionPointer = info.Address;
                    }
                    break;
                    case ObjectType::ForeignClosure:
                    {
                        auto info = m_foreignFunctions[obj.ForeignClosure().Value()];
                        auto args = convert.Args();

                        EXEC_TRACE("\tARGS  %lld ", args);
                        EXEC_TRACE("\tBASE  %lld ", m_stackPointer - convert.Args());
                        EXEC_TRACE("\tFN    %lld ", m_stackPointer - convert.Args() - 1);
                        EXEC_TRACE("\tFRAME %llu ", m_stackPointer);

                        {
                            Push(Object{Frame
                            {
                                static_cast<uint32_t>(m_framePointer),
                                static_cast<uint32_t>(instructionPointer),
                                static_cast<uint8_t>(args),
                                static_cast<uint8_t>(convert.Returns()),
                            }});

                            m_framePointer = m_stackPointer - 1;
                            m_argCount = static_cast<uint8_t>(args);
                        }

                        auto vm = Vm{ shared_from_this() };
                        auto stack = Stack{ shared_from_this() };
                        auto returns = info.Address(vm, stack, info.Environment);

                        if (DoReturn(returns, instructionPointer))
                        {
                            return;
                        }
                    }
                    break;
                    default:
                        assert(false); // TODO error
                }
            }
            break;
            case Instruction::Return:
            {
                InstructionArgumentConverter<InstructionArgumentType::Count> convert;
                convert.Read(m_code, instructionPointer);

                if (DoReturn(convert.Get(), instructionPointer))
                {
                    return;
                }
            }
            break;
            case Instruction::Read:
            {
                InstructionArgumentConverter<InstructionArgumentType::StackIndex> convert;
                convert.Read(m_code, instructionPointer);

                auto obj = Read(convert.Get());
                Push(std::move(obj));
            }
            break;
            case Instruction::Write:
            {
                InstructionArgumentConverter<InstructionArgumentType::StackIndex> convert;
                convert.Read(m_code, instructionPointer);

                auto obj = Pop();
                Write(convert.Get(), std::move(obj));
            }
            break;
            case Instruction::GlobalRead:
            {
                InstructionArgumentConverter<InstructionArgumentType::GlobalIndex> convert;
                convert.Read(m_code, instructionPointer);
                auto index = static_cast<size_t>(convert.Get());

                assert(index < m_globals.size());
                auto obj = m_globals[index];
                Push(std::move(obj));
            }
            break;
            case Instruction::GlobalWrite:
            {
                InstructionArgumentConverter<InstructionArgumentType::GlobalIndex> convert;
                convert.Read(m_code, instructionPointer);
                auto index = static_cast<size_t>(convert.Get());

                auto obj = Pop();
                assert(index < m_globals.size());
                m_globals[index] = std::move(obj);
            }
            break;
            case Instruction::Alloc:
            {
                InstructionArgumentConverter<InstructionArgumentType::StackIndex> convert;
                convert.Read(m_code, instructionPointer);

                assert(convert.Get() >= 0);

                for (size_t i = 0, endI = static_cast<size_t>(convert.Get()); i < endI; ++i)
                {
                    Push(Object{});
                }
            }
            break;
            case Instruction::Jump:
            {
                auto jumpBase = static_cast<int64_t>(instructionPointer);

                InstructionArgumentConverter<InstructionArgumentType::AbsoluteIndex> convert;
                convert.Read(m_code, instructionPointer);

                instructionPointer = static_cast<size_t>(jumpBase + convert.Get());
            }
            break;
            case Instruction::JumpRelative:
            {
                auto jumpBase = static_cast<int64_t>(instructionPointer);

                InstructionArgumentConverter<InstructionArgumentType::RelativeIndex> convert;
                convert.Read(m_code, instructionPointer);

                instructionPointer = static_cast<size_t>(jumpBase + convert.Get());
            }
            break;
            case Instruction::Test:
            {
                auto obj = Pop();
                if (Truth(obj))
                    { instructionPointer += 3; } // TODO this is a hack!
            }
            break;
            case Instruction::TestNot:
            {
                auto obj = Pop();
                if (!Truth(obj))
                    { instructionPointer += 3; } // TODO this is a hack!
            }
            break;
            case Instruction::PushNil:
            {
                Push(Object{});
            }
            break;
            case Instruction::PushAtom:
            {
                InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
                convert.Read(m_code, instructionPointer);

                auto index = static_cast<size_t>(convert.Get());
                assert(index < m_atomsRuntimeLookup.size());
                Push(Object{AtomId{m_atomsRuntimeLookup[index]}});
            }
            break;
            case Instruction::PushNumber:
            {
                InstructionArgumentConverter<InstructionArgumentType::Number> convert;
                convert.Read(m_code, instructionPointer);

                Push(Object{convert.Value()});
            }
            break;
            case Instruction::PushString:
            {
                InstructionArgumentConverter<InstructionArgumentType::String> convert;
                convert.Read(m_code, instructionPointer);

                auto index = static_cast<size_t>(convert.Get());
                assert(index < m_strings.size());
                Push(Object{StringRefId{&m_strings[index]}});

            }
            break;
            case Instruction::PushTrue:
            {
                Push(Object{true});
            }
            break;
            case Instruction::PushFalse:
            {
                Push(Object{false});
            }
            break;
            case Instruction::PushList:
            {
                Push(Object{ArrayT{}});
            }
            break;
            case Instruction::PushTable:
            {
                Push(Object{TableT{}});
            }
            break;
            case Instruction::PushClosure:
            {
                InstructionArgumentConverter<InstructionArgumentType::Function> convert;
                convert.Read(m_code, instructionPointer);

                Push(Object{ClosureId{convert.Value()}});
            }
            break;
            case Instruction::Add:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                assert(lhs.Type() == ObjectType::Number); // TODO error
                assert(lhs.Type() == ObjectType::Number); // TODO error

                Push(Object{lhs.Number() + rhs.Number()});
            }
            break;
            case Instruction::Subtract:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                assert(lhs.Type() == ObjectType::Number); // TODO error
                assert(lhs.Type() == ObjectType::Number); // TODO error

                Push(Object{lhs.Number() - rhs.Number()});
            }
            break;
            case Instruction::Multiply:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                assert(lhs.Type() == ObjectType::Number); // TODO error
                assert(lhs.Type() == ObjectType::Number); // TODO error

                Push(Object{lhs.Number() * rhs.Number()});
            }
            break;
            case Instruction::Divide:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                assert(lhs.Type() == ObjectType::Number); // TODO error
                assert(lhs.Type() == ObjectType::Number); // TODO error

                Push(Object{lhs.Number() / rhs.Number()});
            }
            break;
            case Instruction::Modulus:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                assert(lhs.Type() == ObjectType::Number); // TODO error
                assert(lhs.Type() == ObjectType::Number); // TODO error

                Push(Object{std::fmod(lhs.Number(), rhs.Number())});
            }
            break;
            case Instruction::And:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{Truth(lhs) && Truth(rhs)});
            }
            break;
            case Instruction::Or:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{Truth(lhs) || Truth(rhs)});
            }
            break;
            case Instruction::Append:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                switch (lhs.Type())
                {
                    case ObjectType::StringReference:
                    case ObjectType::StringValue:
                    {
                        assert(rhs.Type() == ObjectType::StringReference ||
                            rhs.Type() == ObjectType::StringValue); // TODO error

                        Push(Object{lhs.StringValue() + rhs.StringValue()});
                    }
                    break;
                    case ObjectType::Array:
                    {
                        assert(rhs.Type() == ObjectType::Array); // TODO error

                        auto res = lhs.Array(); // intentional copy
                        res.insert(res.end(), rhs.Array().begin(), rhs.Array().end());

                        Push(Object{std::move(res)});
                    }
                    break;
                    case ObjectType::Table:
                    {
                        assert(rhs.Type() == ObjectType::Table);

                        auto res = lhs.Table(); // intentional copy
                        for (auto const& kv : rhs.Table())
                        {
                            res[kv.first] = kv.second;
                        }

                        Push(Object{std::move(res)});
                    }
                    break;
                    default:
                        assert(false); // TODO error
                }
            }
            break;
            case Instruction::Size:
            {
                auto obj = Pop();
                Push(Object{static_cast<double>(Size(obj))});
            }
            break;
            case Instruction::Negate:
            {
                auto obj = Pop();
                Push(Object{-obj.Number()});
            }
            break;
            case Instruction::Not:
            {
                auto obj = Pop();
                Push(Object{!Truth(obj)});
            }
            break;
            case Instruction::SubscriptRead:
            {
                auto key = Pop();
                auto container = Pop();

                switch (container.Type())
                {
                    case ObjectType::Array:
                    {
                        assert(key.Type() == ObjectType::Number); // TODO error

                        auto& array = container.Array();
                        auto index = key.Number();

                        assert(0 <= index); // TODO error
                        assert(index == std::trunc(index)); // TODO error

                        if (index < array.size())
                            { Push(Object{array[static_cast<size_t>(index)]}); }
                        else
                            { Push(Object{}); }
                    }
                    break;
                    case ObjectType::Table:
                    {
                        assert(key.Type() != ObjectType::Nil); // TODO error

                        auto& table = container.Table();

                        auto it = table.find(key);
                        if (it != table.end())
                            { Push(Object{it->second}); }
                        else
                            { Push(Object{}); }
                    }
                    break;
                    default:
                        assert(false); // TODO error
                }
            }
            break;
            case Instruction::SubscriptWrite:
            {
                auto key = Pop();
                auto container = Pop();
                auto value = Pop();

                switch (container.Type())
                {
                    case ObjectType::Array:
                    {
                        assert(key.Type() == ObjectType::Number); // TODO error

                        auto& array = container.Array();
                        auto index = key.Number();

                        assert(0 <= index); // TODO error
                        assert(index == std::trunc(index)); // TODO error

                        if (index >= array.size())
                            { array.resize(static_cast<size_t>(index) + 1); }

                        array[static_cast<size_t>(index)] = std::move(value);
                    }
                    break;
                    case ObjectType::Table:
                    {
                        assert(key.Type() != ObjectType::Nil); // TODO error

                        auto& table = container.Table();

                        if (value.Type() == ObjectType::Nil)
                            { table.erase(key); }
                        else
                            { table[key] = std::move(value); }
                    }
                    break;
                    default:
                        assert(false); // TODO error
                }
            }
            break;
            case Instruction::Equal:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{lhs == rhs});
            }
            break;
            case Instruction::NotEqual:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{lhs != rhs});
            }
            break;
            case Instruction::Less:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{lhs < rhs});

            }
            break;
            case Instruction::LessEqual:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{lhs <= rhs});
            }
            break;
            case Instruction::Greater:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{lhs > rhs});
            }
            break;
            case Instruction::GreaterEqual:
            {
                auto rhs = Pop();
                auto lhs = Pop();

                Push(Object{lhs >= rhs});
            }
            break;
            case Instruction::PreIncrement:
            {
                assert(false);
            }
            break;
            case Instruction::PreDecrement:
            {
                assert(false);
            }
            break;
            case Instruction::PostIncrement:
            {
                assert(false);
            }
            break;
            case Instruction::PostDecrement:
            {
                assert(false);
            }
            break;
            case Instruction::SubscriptPreIncrement:
            {
                assert(false);
            }
            break;
            case Instruction::SubscriptPreDecrement:
            {
                assert(false);
            }
            break;
            case Instruction::SubscriptPostIncrement:
            {
                assert(false);
            }
            break;
            case Instruction::SubscriptPostDecrement:
            {
                assert(false);
            }
            break;
            case Instruction::SubscriptMemberRead:
            {
                InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
                convert.Read(m_code, instructionPointer);

                auto obj = Pop();
                assert(obj.Type() == ObjectType::Table); // TODO error

                auto& table = obj.Table();

                auto index = static_cast<size_t>(convert.Get());
                assert(index < m_atomsRuntimeLookup.size());
                auto key = m_atomsRuntimeLookup[index];

                auto it = table.find(Object{key});
                if (it != table.end())
                    { Push(Object{it->second}); }
                else
                    { Push(Object{}); }
            }
            break;
            case Instruction::SubscriptMemberWrite:
            {
                InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
                convert.Read(m_code, instructionPointer);

                auto container = Pop();
                auto value = Pop();

                auto& table = container.Table();

                auto index = static_cast<size_t>(convert.Get());
                assert(index < m_atomsRuntimeLookup.size());
                auto key = m_atomsRuntimeLookup[index];

                if (value.Type() == ObjectType::Nil)
                    { table.erase(Object{key}); }
                else
                    { table[Object{key}] = std::move(value); }
            }
            break;
            case Instruction::SubscriptSelfCall:
            {
                InstructionArgumentConverter<InstructionArgumentType::Atom> convert;
                convert.Read(m_code, instructionPointer);

                auto obj = Pop();
                assert(obj.Type() == ObjectType::Table); // TODO error

                auto& table = obj.Table();

                auto index = static_cast<size_t>(convert.Get());
                assert(index < m_atomsRuntimeLookup.size());
                auto key = m_atomsRuntimeLookup[index];

                auto it = table.find(Object{key});
                if (it != table.end())
                    { Push(Object{it->second}); }
                else
                    { Push(Object{}); } // call will handle the error

                Push(std::move(obj));
            }
            break;
        }
    }
}

Bytecode const& VmImpl::Code() const noexcept
{
    return m_code;
}

int64_t VmImpl::Top() const noexcept
{
    return static_cast<int64_t>(m_stackPointer - m_framePointer);
}

int64_t VmImpl::Bottom() const noexcept
{
    return -static_cast<int64_t>(m_argCount);
}

Object VmImpl::Read(int64_t index) const
{
    assert(index != 0 || m_framePointer == 0); // TODO error, better check for no frames
    assert(index >= -static_cast<int64_t>(m_argCount + 1)); // TODO error

    auto absoluteIndex = static_cast<size_t>(static_cast<int64_t>(m_framePointer) + index);
    assert(absoluteIndex < m_stackPointer); // TODO error
    EXEC_TRACE("\tREAD  %zu ", absoluteIndex);

    return m_stack[absoluteIndex];
}

void VmImpl::Write(int64_t index, Object&& v) noexcept
{
    assert(index != 0 || m_framePointer == 0); // TODO error, better check for no frames
    assert(index >= -static_cast<int64_t>(m_argCount + 1)); // TODO error

    auto absoluteIndex = static_cast<size_t>(static_cast<int64_t>(m_framePointer) + index);
    assert(absoluteIndex < m_stackPointer); // TODO error
    EXEC_TRACE("\tWRITE %zu ", absoluteIndex);

    m_stack[absoluteIndex] = std::move(v);
}

void VmImpl::Push(Object&& v)
{
    EXEC_TRACE("\tPUSH  %llu ", m_stackPointer);
    assert(m_stackPointer < m_stack.capacity()); // TODO error

    m_stack[m_stackPointer] = std::move(v);
    ++m_stackPointer;
}

Object VmImpl::Pop() noexcept
{
    EXEC_TRACE("\tPOP   %llu ", m_stackPointer - 1);
    assert(m_stackPointer != 0); // TODO error

    --m_stackPointer;
    return std::move(m_stack[m_stackPointer]);
}

AtomId VmImpl::DeclareAtom(std::string const& id)
{
    auto res = m_atomsRuntime.emplace(id);
    return AtomId{&*res.first};
}

GlobalId VmImpl::DeclareGlobal(istring const& id)
{
    m_globalEnvironment.DeclareGlobal(id);

    auto info = m_globalEnvironment.Lookup(id);
    assert(info.Index >= 0);
    assert(info.IsGlobal);

    auto index = static_cast<uint64_t>(info.Index);
    assert(index == m_globals.size());

    m_globals.push_back(Object{});
    return GlobalId{index};
}

Object VmImpl::ReadGlobal(GlobalId i) const
{
    assert(i.Value() < m_globals.size()); // TODO error
    return m_globals[i.Value()];
}

void VmImpl::WriteGlobal(GlobalId i, Object&& v)
{
    assert(i.Value() < m_globals.size()); // TODO error
    m_globals[i.Value()] = std::move(v);
}
Object VmImpl::RegisterForeignClosure(istring const& name, ForeignClosure* fp, void* env) noexcept
{
    auto i = m_foreignFunctions.size();
    m_foreignFunctions.push_back(ForeignClosureInfo{fp, env, name});

    return Object{ForeignClosureId{static_cast<uint32_t>(i)}};
}

Environment VmImpl::DebugGlobalEnvironment() const noexcept
{
    return m_globalEnvironment;
}

Debug::Global VmImpl::DebugGlobal(GlobalId i) const
{
    assert(i.Value() < m_globals.size()); // TODO error
    auto& data = m_globalEnvironment.ReverseLookup(static_cast<int64_t>(i.Value()));

    assert(static_cast<int64_t>(i.Value()) == data.second.Index);
    return Debug::Global{i, data.first};
}

FunctionTable const& VmImpl::DebugFunctionTable() const noexcept
{
    return m_functions;
}

AtomTable const& VmImpl::DebugAtomTable() const noexcept
{
    return m_atoms;
}

StringTable const& VmImpl::DebugStringTable() const noexcept
{
    return m_strings;
}

bool VmImpl::DoReturn(int64_t localReturnCount_signed, size_t& instructionPointer) noexcept
{
    auto localReturnCount = static_cast<uint64_t>(localReturnCount_signed);

    auto obj = m_stack[m_framePointer];
    assert(obj.Type() == ObjectType::Frame);
    auto frame = obj.StackFrame();

    auto externalReturn = frame.ReturnPointer == 0;

    assert(m_framePointer - frame.ArgumentCount - 1 >= frame.FramePointer);
    auto outReturnsBase = m_framePointer - frame.ArgumentCount;
    if (!externalReturn)
    {
        outReturnsBase -= 1;
    }

    assert(m_stackPointer - m_framePointer > localReturnCount);
    auto inReturnsBase = m_stackPointer - localReturnCount;

    auto expectedReturnCount = static_cast<uint64_t>(frame.ReturnCount);
    if (externalReturn)
    {
        // external calls do not set the expected return count value
        expectedReturnCount = localReturnCount;
    }

    auto actualReturns = std::min(expectedReturnCount, localReturnCount);
    EXEC_TRACE("\tEXT   %s ", externalReturn ? "Y" : "N");
    EXEC_TRACE("\tRET   %llu ", actualReturns);
    EXEC_TRACE("\tFRAME %llu ", m_framePointer);
    EXEC_TRACE("\tARGS  %u ", frame.ArgumentCount);
    EXEC_TRACE("\tBASE  %llu ", outReturnsBase);
    EXEC_TRACE("\tFROM  %llu ", inReturnsBase);
    EXEC_TRACE("\tPAD   %llu ", expectedReturnCount - actualReturns);

    assert(m_argCount == frame.ArgumentCount);

    for (auto i = 0llu, endI = actualReturns; i < endI; ++i)
    {
        auto retObj = std::move(m_stack[inReturnsBase + i]);
        m_stack[outReturnsBase + i] = std::move(retObj);
    }

    while (m_stackPointer > outReturnsBase + actualReturns)
    {
        Pop();
    }

    while (m_stackPointer < outReturnsBase + expectedReturnCount)
    {
        Push(Object{});
    }

    if (externalReturn)
    {
        EXEC_TRACE("\n");
        return true;
    }

    m_framePointer = frame.FramePointer;
    m_argCount = m_stack[m_framePointer].StackFrame().ArgumentCount;
    instructionPointer = frame.ReturnPointer;

    return false;
}

Stack::~Stack() noexcept
{}

Stack::Stack(std::shared_ptr<VmImpl> const& impl) noexcept
    : m_impl{impl}
{}

Stack::operator bool() const noexcept
{
    return static_cast<bool>(m_impl);
}

int64_t Stack::Top() const noexcept
{
    if (m_impl)
    {
        return m_impl->Top();
    }
    else
    {
        return 0;
    }
}

int64_t Stack::Bottom() const noexcept
{
    if (m_impl)
    {
        return m_impl->Bottom();
    }
    else
    {
        return 0;
    }
}

Object Stack::Read(int64_t index) const
{
    assert(m_impl);
    return m_impl->Read(index);
}

void Stack::Write(int64_t index, Object&& v)
{
    assert(m_impl);
    m_impl->Write(index, std::move(v));
}

void Stack::Push(Object&& v)
{
    assert(m_impl);
    return m_impl->Push(std::move(v));
}

Object Stack::Pop()
{
    assert(m_impl);
    return m_impl->Pop();
}

Vm::Vm() noexcept
    : m_impl{std::make_shared<VmImpl>()}
{}

Vm::~Vm() noexcept
{}

Vm::Vm(std::shared_ptr<VmImpl> const& impl) noexcept
    : m_impl{impl}
{
    assert(impl);
}

void Vm::Load(istring&& code)
{
    m_impl->Load(std::move(code));
}

Stack Vm::Run()
{
    m_impl->Run();
    return Stack{m_impl};
}

Bytecode const& Vm::Code() const noexcept
{
    return m_impl->Code();
}

AtomId Vm::DeclareAtom(std::string const& id) noexcept
{
    return m_impl->DeclareAtom(id);
}

GlobalId Vm::DeclareGlobal(istring const& id)
{
    return m_impl->DeclareGlobal(id);
}

Object Vm::ReadGlobal(GlobalId i) const
{
    return m_impl->ReadGlobal(i);
}

void Vm::WriteGlobal(GlobalId i, Object&& v)
{
    m_impl->WriteGlobal(i, std::move(v));
}

Object Vm::RegisterForeignClosure(ForeignClosure* fp, void* env) noexcept
{
    return RegisterForeignClosure(istring{}, fp, env);
}

Object Vm::RegisterForeignClosure(istring const& name, ForeignClosure* fp, void* env) noexcept
{
    return m_impl->RegisterForeignClosure(name, fp, env);
}

Environment Vm::DebugGlobalEnvironment() const noexcept
{
    return m_impl->DebugGlobalEnvironment();
}

Debug::Global Vm::DebugGlobal(GlobalId i) const
{
    return m_impl->DebugGlobal(i);
}

FunctionTable const& Vm::DebugFunctionTable() const noexcept
{
    return m_impl->DebugFunctionTable();
}

AtomTable const& Vm::DebugAtomTable() const noexcept
{
    return m_impl->DebugAtomTable();
}

StringTable const& Vm::DebugStringTable() const noexcept
{
    return m_impl->DebugStringTable();
}

}
