#pragma once

#include <memory>
#include <vector>

#include <lexer/lexer.h>

namespace L3
{

using IString::istring;

enum class NodeType
{
    Nil,
    Error,
    Root,
    Block,
    Statement,
    StatementLast,
    SideEffect,
    Definition,
    DefinitionAtom,
    DefinitionVariable,
    Expression,
    ExpressionMath0,
    ExpressionMath1,
    ExpressionMath2,
    ExpressionMath3,
    ExpressionMath4,
    ExpressionMath5,
    ExpressionMath6,
    ExpressionList,
    If,
    IfCondition,
    Loop,
    LoopWhile,
    LoopDo,
    LoopFor,
    Lval,
    LvalList,
    Rval,
    Object,
    ObjectFunction,
    ObjectSubscript,
    ObjectMemberAccess,
    ObjectMemberCall,
    IdAtom,
    IdVar,
    IdVarList,
    Constructor,
    ConstructorList,
    ConstructorTable,
    ConstructorFunction,
    String,
    StringToken,
    Number,
    Break,
    Continue,
    Recurse,
    Return,
    KNil,
    KTrue,
    KFalse,
    OAdd,
    OSubtract,
    OMultiply,
    ODivide,
    OModulus,
    ONegate,
    OSize,
    OPreIncrement,
    OPreDecrement,
    OPostIncrement,
    OPostDecrement,
    OEqual,
    ONotEqual,
    OLess,
    OLessEqual,
    OGreater,
    OGreaterEqual,
    OAnd,
    OOr,
    ONot,
    OAppend,
    OAssign,
    OAddAssign,
    OSubtractAssign,
    OMultiplyAssign,
    ODivideAssign,
    OModulusAssign,
    OAndAssign,
    OOrAssign,
    OAppendAssign,
};

class Node;

using NodeList = std::vector<Node>;

class Node
{
public:
    Node() noexcept = default;
    Node(Node const&) noexcept = default;
    Node(Node&&) noexcept = default;
    Node& operator=(Node const&) noexcept = default;
    Node& operator=(Node&&) noexcept = default;

    Node(NodeType t, ValueHolder&& v, NodeList&& c, SourceReference&& s) noexcept;

    NodeType Type() const noexcept;
    SourceReference const& Source() const noexcept;
    ValueHolder const& Value() const noexcept;
    NodeList const& Children() const noexcept;

    uintptr_t Identity() const noexcept;

    explicit operator bool() const noexcept;

    bool operator<(Node const& other) const noexcept;

private:
    struct Impl
    {
        Impl() noexcept = delete;
        Impl(Impl const&) noexcept = delete;
        Impl(Impl&&) noexcept = delete;
        Impl& operator=(Impl const&) noexcept = delete;
        Impl& operator=(Impl&&) noexcept = delete;

        Impl(NodeType t, ValueHolder&& v, NodeList&& c, SourceReference&& s) noexcept;

        NodeType Type;
        SourceReference Source;
        ValueHolder Value;
        NodeList Children;
    };

    std::shared_ptr<const Impl> m_impl;
};

class NodeBuilder
{
public:
    NodeBuilder() noexcept;
    NodeBuilder(NodeBuilder const&) = delete;
    NodeBuilder(NodeBuilder&&) = default;
    NodeBuilder& operator=(NodeBuilder const&) = delete;
    NodeBuilder& operator=(NodeBuilder&&) = default;

    NodeBuilder(NodeType t, SourceReference const& s) noexcept;
    NodeBuilder(NodeType t, SourceReference const& s, uint8_t f) noexcept;

    void SetSource(SourceReference&& s) noexcept;
    void SetValue(ValueHolder&& v) noexcept;
    void AppendChild(Node&& c) noexcept;

    NodeType Type() const noexcept;
    SourceReference const& Source() const noexcept;
    ValueHolder const& Value() const noexcept;
    NodeList const& Children() const noexcept;

    void SetBuildState(uint8_t s) noexcept;
    uint8_t BuildState() const noexcept;

    uint8_t GetParentFallbackBuildState() noexcept;
    bool IsTentative() const noexcept;

    void SetHasConsumedInput() noexcept;
    bool HasConsumedInput() const noexcept;

    bool TypeAllowsElision() const noexcept;

    static Node Make(NodeBuilder&& b) noexcept;

private:
    NodeType m_type = NodeType::Nil;
    SourceReference m_source;
    ValueHolder m_value;
    NodeList m_children;

    uint8_t m_buildState = 0;
    uint8_t m_parentFallbackBuildState = 255;
    bool m_hasConsumedInput = false;
};

using ParseStack = std::vector<NodeBuilder>;

class ParserState
{
public:
    ParserState() noexcept = default;
    ParserState(ParserState const&) = delete;
    ParserState(ParserState&&) noexcept = default;
    ParserState& operator=(ParserState const&) = delete;
    ParserState& operator=(ParserState&&) = default;

    ParserState(Node&& r, std::vector<NodeBuilder>&& s, istring&& source) noexcept;

    bool Terminal() const noexcept;
    std::vector<NodeBuilder> const& Stack() const noexcept;
    Node Ast() const noexcept;
    istring Source() const noexcept;

    static ParseStack Consume(ParserState&& s) noexcept;

private:
    Node m_root;
    ParseStack m_stack;
    istring m_source;
};

class Parser
{
public:
    static ParserState Init(istring const& source) noexcept;
    static ParserState Consume(ParserState&& s, Token const& t) noexcept;

private:
    static ParserState Root(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Block(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Statement(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState StatementLast(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState SideEffect(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState DefinitionAtom(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState DefinitionVariable(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Expression(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath0(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath1(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath2(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath3(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath4(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath5(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionMath6(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ExpressionList(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState If(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState IfCondition(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Loop(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState LoopWhile(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState LoopDo(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState LoopFor(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Lval(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Rval(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Object(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ObjectFunction(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ObjectSubscript(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ObjectMemberAccess(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ObjectMemberCall(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState IdVarList(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Constructor(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ConstructorList(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ConstructorTable(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState ConstructorFunction(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState String(ParserState&& s, Token const& t, bool& consumed) noexcept;
    static ParserState Error(ParserState&& s, ErrorType e, Token const& t, bool& consumed) noexcept;

    static ParserState Push(ParserState&& s, uint8_t bs, NodeBuilder&& n) noexcept; // add a new node builder on stack
    static ParserState Pop(ParserState&& s) noexcept; // pop current node builder and add as child of new current
    static ParserState Pop(ParserState&& s, Cursor endMarker) noexcept;
    static ParserState ConsumeToken(ParserState&& s, uint8_t bs, Token const& t, bool& consumed) noexcept;
    static ParserState UpdateBuildState(ParserState&& s, uint8_t bs) noexcept;
    static ParserState AppendTerminal(ParserState&& s, uint8_t bs, Token const& t, bool& consumed) noexcept;
    static ParserState AppendTerminal(
        ParserState&& s,
        uint8_t bs,
        NodeType n,
        Token const& t,
        bool& consumed
    ) noexcept;
};

class AstRewrite
{
public:
    static Node Rewrite(Node const& n) noexcept;

private:
    static Node Recurse(Node const& n) noexcept;
    static Node StatementLast(Node const& n) noexcept;
    static Node SideEffect(Node const& n) noexcept;
    static Node BinaryExpression(Node const& n) noexcept;
    static Node UnaryExpression(Node const& n) noexcept;
    static Node LoopFor(Node const& n) noexcept;
    static Node Object(Node const& n) noexcept;

    static void SetNodeBuilderSource(NodeBuilder& n) noexcept;
};

}
