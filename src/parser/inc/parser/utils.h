#pragma once

#include <cstdio>

#include <common/utils.h>
#include <istring/string_view.h>
#include <istring/utility.h>
#include <parser/parser.h>

namespace L3
{

using IString::istring_view;

inline
istring_view NodeTypeName(NodeType t) noexcept
{
    switch (t)
    {
        case NodeType::Nil:                 return "Nil";
        case NodeType::Error:               return "Error";
        case NodeType::Root:                return "Root";
        case NodeType::Block:               return "Block";
        case NodeType::Statement:           return "Statement";
        case NodeType::StatementLast:       return "StatementLast";
        case NodeType::SideEffect:          return "SideEffect";
        case NodeType::Definition:          return "Definition";
        case NodeType::DefinitionAtom:      return "DefinitionAtom";
        case NodeType::DefinitionVariable:  return "DefinitionVariable";
        case NodeType::Expression:          return "Expression";
        case NodeType::ExpressionMath0:     return "ExpressionMath0";
        case NodeType::ExpressionMath1:     return "ExpressionMath1";
        case NodeType::ExpressionMath2:     return "ExpressionMath2";
        case NodeType::ExpressionMath3:     return "ExpressionMath3";
        case NodeType::ExpressionMath4:     return "ExpressionMath4";
        case NodeType::ExpressionMath5:     return "ExpressionMath5";
        case NodeType::ExpressionMath6:     return "ExpressionMath6";
        case NodeType::ExpressionList:      return "ExpressionList";
        case NodeType::If:                  return "If";
        case NodeType::IfCondition:         return "IfCondition";
        case NodeType::Loop:                return "Loop";
        case NodeType::LoopWhile:           return "LoopWhile";
        case NodeType::LoopDo:              return "LoopDo";
        case NodeType::LoopFor:             return "LoopFor";
        case NodeType::Lval:                return "Lval";
        case NodeType::LvalList:            return "LvalList";
        case NodeType::Rval:                return "Rval";
        case NodeType::Object:              return "Object";
        case NodeType::ObjectFunction:      return "ObjectFunction";
        case NodeType::ObjectSubscript:     return "ObjectSubscript";
        case NodeType::ObjectMemberAccess:  return "ObjectMemberAccess";
        case NodeType::ObjectMemberCall:    return "ObjectMemberCall";
        case NodeType::IdAtom:              return "IdAtom";
        case NodeType::IdVar:               return "IdVar";
        case NodeType::IdVarList:           return "IdVarList";
        case NodeType::Constructor:         return "Constructor";
        case NodeType::ConstructorList:     return "ConstructorList";
        case NodeType::ConstructorTable:    return "ConstructorTable";
        case NodeType::ConstructorFunction: return "ConstructorFunction";
        case NodeType::String:              return "String";
        case NodeType::StringToken:         return "StringToken";
        case NodeType::Number:              return "Number";
        case NodeType::Break:               return "Break";
        case NodeType::Continue:            return "Continue";
        case NodeType::Recurse:             return "Recurse";
        case NodeType::Return:              return "Return";
        case NodeType::KNil:                return "KNil";
        case NodeType::KTrue:               return "KTrue";
        case NodeType::KFalse:              return "KFalse";
        case NodeType::OAdd:                return "OAdd";
        case NodeType::OSubtract:           return "OSubtract";
        case NodeType::OMultiply:           return "OMultiply";
        case NodeType::ODivide:             return "ODivide";
        case NodeType::OModulus:            return "OModulus";
        case NodeType::ONegate:             return "ONegate";
        case NodeType::OSize:               return "OSize";
        case NodeType::OPreIncrement:       return "OPreIncrement";
        case NodeType::OPreDecrement:       return "OPreDecrement";
        case NodeType::OPostIncrement:      return "OPostIncrement";
        case NodeType::OPostDecrement:      return "OPostDecrement";
        case NodeType::OEqual:              return "OEqual";
        case NodeType::ONotEqual:           return "ONotEqual";
        case NodeType::OLess:               return "OLess";
        case NodeType::OLessEqual:          return "OLessEqual";
        case NodeType::OGreater:            return "OGreater";
        case NodeType::OGreaterEqual:       return "OGreaterEqual";
        case NodeType::OAnd:                return "OAnd";
        case NodeType::OOr:                 return "OOr";
        case NodeType::ONot:                return "ONot";
        case NodeType::OAppend:             return "OAppend";
        case NodeType::OAssign:             return "OAssign";
        case NodeType::OAddAssign:          return "OAddAssign";
        case NodeType::OSubtractAssign:     return "OSubtractAssign";
        case NodeType::OMultiplyAssign:     return "OMultiplyAssign";
        case NodeType::ODivideAssign:       return "ODivideAssign";
        case NodeType::OModulusAssign:      return "OModulusAssign";
        case NodeType::OAndAssign:          return "OAndAssign";
        case NodeType::OOrAssign:           return "OOrAssign";
        case NodeType::OAppendAssign:       return "OAppendAssign";
        //default:                            return istring_view{"<INVALID NODE TYPE>"};
    }
}

namespace Detail
{

inline
void PrintNodeInfo(
    NodeType t,
    ValueHolder const& vh,
    NodeList const& c,
    SourceReference const& s,
    uint32_t indent
) noexcept
{
    static size_t const indentAllowance = 32;
    static size_t const typeNameWidth = 20;
    static size_t const valueMaxWidth = 30;

    auto n = NodeTypeName(t);
    auto v = to_istring(vh).substr(0, valueMaxWidth);
    if (v.empty())
    {
        v = istring{"nil"};
    }

    auto indentSize = 1 + 4 * indent;
    auto pad = typeNameWidth + valueMaxWidth + indentAllowance - n.size() - v.size() - indentSize;
    pad = pad < 1 ? 1 : pad;
    std::printf("%*c%.*s: %.*s%*c - ",
        static_cast<int>(indentSize), ' ',
        PRINTF_STR(n),
        PRINTF_STR(v),
        static_cast<int>(pad), ' '
    );
    PrintSourceReferenceLine(s);

    for (auto& child : c)
    {
        PrintNodeInfo(child.Type(), child.Value(), child.Children(), child.Source(), indent + 1);
    }
}

}

inline
void PrintNode(Node const& n) noexcept
{
    if (n)
    {
        Detail::PrintNodeInfo(n.Type(), n.Value(), n.Children(), n.Source(), 0);
    }
    else
    {
        std::printf(" %.*s\n", PRINTF_STR(NodeTypeName(n.Type())));
    }
}

inline
void PrintNodeBuilder(NodeBuilder const& n) noexcept
{
    Detail::PrintNodeInfo(n.Type(), n.Value(), n.Children(), n.Source(), 0);
}

inline
void PrintParseStack(ParseStack const& s) noexcept
{
    for (auto& n : s)
    {
        PrintNodeBuilder(n);
        std::printf("--------------\n");
    }
}

inline
bool operator==(Node const& l, Node const& r) noexcept
{
    if (l.Identity() == r.Identity())
    {
        return true;
    }
    else if (l.Type() != r.Type())
    {
        return false;
    }
    else if (l.Type() == NodeType::Nil)
    {
        return true;
    }
    else
    {
        return l.Source() == r.Source()
            && l.Value() == r.Value()
            && l.Children() == r.Children();
    }
}

inline
bool operator!=(Node const& l, Node const & r) noexcept
{
    return !(l == r);
}

inline
bool operator==(NodeBuilder const& l, NodeBuilder const& r) noexcept
{
    return l.Type() == r.Type()
        && l.Source() == r.Source()
        && l.Value() == r.Value()
        && l.Children() == r.Children();
}

inline
bool operator!=(NodeBuilder const& l, NodeBuilder const & r) noexcept
{
    return !(l == r);
}

}
