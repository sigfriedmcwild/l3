#include <parser/parser.h>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <utility>

namespace L3
{

Node::Node(NodeType t, ValueHolder&& v, NodeList&& c, SourceReference&& s) noexcept
    : m_impl{std::make_shared<Impl>(t, std::move(v), std::move(c), std::move(s))}
    // terminate on out of memory
{
    assert(t != NodeType::Nil);
}

NodeType Node::Type() const noexcept
{
    if (m_impl)
    {
        return m_impl->Type;
    }
    else
    {
        return NodeType::Nil;
    }
}

SourceReference const& Node::Source() const noexcept
{
    assert(m_impl);
    return m_impl->Source;
}

ValueHolder const& Node::Value() const noexcept
{
    assert(m_impl);
    return m_impl->Value;
}

NodeList const& Node::Children() const noexcept
{
    assert(m_impl);
    return m_impl->Children;
}

uintptr_t Node::Identity() const noexcept
{
    return reinterpret_cast<uintptr_t>(m_impl.get());
}

Node::operator bool() const noexcept
{
    return static_cast<bool>(m_impl);
}

bool Node::operator<(Node const& other) const noexcept
{
    return m_impl < other.m_impl;
}

Node::Impl::Impl(NodeType t, ValueHolder&& v, NodeList&& c, SourceReference&& s) noexcept
    : Type{t}, Source{std::move(s)}, Value{std::move(v)}, Children{std::move(c)}
{}

NodeBuilder::NodeBuilder(NodeType t, SourceReference const& s) noexcept
    : m_type{t}, m_source{s}
{
    assert(t != NodeType::Nil);
}

NodeBuilder::NodeBuilder(NodeType t, SourceReference const& s, uint8_t f) noexcept
    : m_type{t}, m_source{s}, m_parentFallbackBuildState{f}
{
    assert(t != NodeType::Nil);
    assert(f != 255);
}

void NodeBuilder::SetSource(SourceReference&& s) noexcept
{
    m_source = std::move(s);
}

void NodeBuilder::SetValue(ValueHolder&& v) noexcept
{
    m_value = std::move(v);
}

void NodeBuilder::AppendChild(Node&& c) noexcept
{
    m_children.push_back(std::move(c));
    // terminate on out of memory
}

NodeType NodeBuilder::Type() const noexcept
{
    return m_type;
}

SourceReference const& NodeBuilder::Source() const noexcept
{
    return m_source;
}

ValueHolder const& NodeBuilder::Value() const noexcept
{
    return m_value;
}

NodeList const& NodeBuilder::Children() const noexcept
{
    return m_children;
}

void NodeBuilder::SetBuildState(uint8_t s) noexcept
{
    m_buildState = s;
}

uint8_t NodeBuilder::BuildState() const noexcept
{
    return m_buildState;
}

uint8_t NodeBuilder::GetParentFallbackBuildState() noexcept
{
    assert(m_parentFallbackBuildState != 255);
    return m_parentFallbackBuildState;
}

bool NodeBuilder::IsTentative() const noexcept
{
    return m_parentFallbackBuildState != 255;
}

void NodeBuilder::SetHasConsumedInput() noexcept
{
    m_hasConsumedInput = true;
}

bool NodeBuilder::HasConsumedInput() const noexcept
{
    return m_hasConsumedInput;
}

bool NodeBuilder::TypeAllowsElision() const noexcept
{
    switch (m_type)
    {
        case NodeType::Root:
        case NodeType::Statement:
        case NodeType::StatementLast:
        case NodeType::SideEffect:
        case NodeType::Definition:
        case NodeType::Expression:
        case NodeType::ExpressionMath0:
        case NodeType::ExpressionMath1:
        case NodeType::ExpressionMath2:
        case NodeType::ExpressionMath3:
        case NodeType::ExpressionMath4:
        case NodeType::ExpressionMath5:
        case NodeType::ExpressionMath6:
        case NodeType::Loop:
        case NodeType::Lval:
        case NodeType::Rval:
        case NodeType::Object:
        case NodeType::Constructor:
            return true;
        default:
            return false;
    }
}

Node NodeBuilder::Make(NodeBuilder&& b) noexcept
{
    assert(b.m_type != NodeType::Nil);
    auto n = Node{b.m_type, std::move(b.m_value), std::move(b.m_children), std::move(b.m_source)};
    b.m_type = NodeType::Nil;
    return n;
}

ParserState::ParserState(Node&& r, std::vector<NodeBuilder>&& s, istring&& source) noexcept
    : m_root{std::move(r)}, m_stack{std::move(s)}, m_source{std::move(source)}
{}

bool ParserState::Terminal() const noexcept
{
    return static_cast<bool>(m_root);
}

std::vector<NodeBuilder> const& ParserState::Stack() const noexcept
{
    return m_stack;
}

Node ParserState::Ast() const noexcept
{
    return m_root;
}

istring ParserState::Source() const noexcept
{
    return m_source;
}

std::vector<NodeBuilder> ParserState::Consume(ParserState&& s) noexcept
{
    return std::move(s.m_stack);
}

ParserState Parser::Init(istring const& source) noexcept
{
    auto s = ParserState{Node{}, ParseStack{}, istring{source}};
    s = Push(std::move(s), 0, NodeBuilder{NodeType::Root, SourceReference{}});
    return s;
}

ParserState Parser::Consume(ParserState&& s, Token const& t) noexcept
{
    if (s.Terminal())
    {
        return std::move(s);
    }

    auto consumed = false;

    if (t.Type() == TokenType::Error)
    {
        return Error(std::move(s), t.Value().ErrorValue(), t, consumed);
    }
    else if (t.Type() == TokenType::Comment)
    {
        return std::move(s);
    }

    while (!consumed)
    {
        auto& current = s.Stack().back();
        switch (current.Type())
        {
            case NodeType::Root:
                s = Root(std::move(s), t, consumed);
                break;
            case NodeType::Block:
                s = Block(std::move(s), t, consumed);
                break;
            case NodeType::Statement:
                s = Statement(std::move(s), t, consumed);
                break;
            case NodeType::StatementLast:
                s = StatementLast(std::move(s), t, consumed);
                break;
            case NodeType::SideEffect:
                s = SideEffect(std::move(s), t, consumed);
                break;
            case NodeType::DefinitionAtom:
                s = DefinitionAtom(std::move(s), t, consumed);
                break;
            case NodeType::DefinitionVariable:
                s = DefinitionVariable(std::move(s), t, consumed);
                break;
            case NodeType::Expression:
                s = Expression(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath0:
                s = ExpressionMath0(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath1:
                s = ExpressionMath1(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath2:
                s = ExpressionMath2(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath3:
                s = ExpressionMath3(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath4:
                s = ExpressionMath4(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath5:
                s = ExpressionMath5(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionMath6:
                s = ExpressionMath6(std::move(s), t, consumed);
                break;
            case NodeType::ExpressionList:
                s = ExpressionList(std::move(s), t, consumed);
                break;
            case NodeType::If:
                s = If(std::move(s), t, consumed);
                break;
            case NodeType::IfCondition:
                s = IfCondition(std::move(s), t, consumed);
                break;
            case NodeType::Loop:
                s = Loop(std::move(s), t, consumed);
                break;
            case NodeType::LoopWhile:
                s = LoopWhile(std::move(s), t, consumed);
                break;
            case NodeType::LoopDo:
                s = LoopDo(std::move(s), t, consumed);
                break;
            case NodeType::LoopFor:
                s = LoopFor(std::move(s), t, consumed);
                break;
            case NodeType::Lval:
                s = Lval(std::move(s), t, consumed);
                break;
            case NodeType::Rval:
                s = Rval(std::move(s), t, consumed);
                break;
            case NodeType::Object:
                s = Object(std::move(s), t, consumed);
                break;
            case NodeType::ObjectFunction:
                s = ObjectFunction(std::move(s), t, consumed);
                break;
            case NodeType::ObjectSubscript:
                s = ObjectSubscript(std::move(s), t, consumed);
                break;
            case NodeType::ObjectMemberAccess:
                s = ObjectMemberAccess(std::move(s), t, consumed);
                break;
            case NodeType::ObjectMemberCall:
                s = ObjectMemberCall(std::move(s), t, consumed);
                break;
            case NodeType::IdVarList:
                s = IdVarList(std::move(s), t, consumed);
                break;
            case NodeType::Constructor:
                s = Constructor(std::move(s), t, consumed);
                break;
            case NodeType::ConstructorList:
                s = ConstructorList(std::move(s), t, consumed);
                break;
            case NodeType::ConstructorTable:
                s = ConstructorTable(std::move(s), t, consumed);
                break;
            case NodeType::ConstructorFunction:
                s = ConstructorFunction(std::move(s), t, consumed);
                break;
            case NodeType::String:
                s = String(std::move(s), t, consumed);
                break;

            default:
                s = Error(std::move(s), ErrorType::Generic, t, consumed);
                break;
        }
    }

    return std::move(s);
}

ParserState Parser::Root(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Root);

    switch (current.BuildState())
    {
        case 0:
            switch (t.Type())
            {
                case TokenType::Begin:
                    return ConsumeToken(std::move(s), 1, t, consumed);
                default:
                    return Push(std::move(s), 2, NodeBuilder{NodeType::Block, t.Source()});
            }
        case 1:
            return Push(std::move(s), 2, NodeBuilder{NodeType::Block, t.Source()});
        case 2:
            switch (t.Type())
            {
                case TokenType::Eof:
                {
                    consumed = true;
                    return Pop(std::move(s));
                }
                default:
                    return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
            }
        default:
            assert(false);
    }
}

ParserState Parser::Block(ParserState&& s, Token const& t, bool& /*consumed*/) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Block);

    switch (current.BuildState())
    {
        case 0: // accept Statement
            return Push(std::move(s), 0, NodeBuilder{NodeType::Statement, t.Source(), 1});
        case 1: // accept StatementLast
            return Push(std::move(s), 2, NodeBuilder{NodeType::StatementLast, t.Source(), 2});
        case 2:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::Statement(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Statement);

    switch (current.BuildState())
    {
        case 0: // accept SideEffect
            return Push(std::move(s), 200, NodeBuilder{NodeType::SideEffect, t.Source(), 1});
        case 1: // accept DefinitionAtom
            return Push(std::move(s), 200, NodeBuilder{NodeType::DefinitionAtom, t.Source(), 2});
        case 2: // accept If
            return Push(std::move(s), 201, NodeBuilder{NodeType::If, t.Source(), 3});
        case 3: // accept Loop
            return Push(std::move(s), 201, NodeBuilder{NodeType::Loop, t.Source(), 4});
        case 4: // accept {
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 41, t, consumed);}
            else
                {return UpdateBuildState(std::move(s), 5);}
        case 5: // out of options
            return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
        case 41: // expect Block
            return Push(std::move(s), 42, NodeBuilder{NodeType::Block, t.Source()});
        case 42: // expect }
            if (t.Type() == TokenType::PCloseCurly)
                {return ConsumeToken(std::move(s), 201, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 200: // expect ;
            if (t.Type() == TokenType::PSemicolon)
                {return ConsumeToken(std::move(s), 201, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 201: // done
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::StatementLast(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::StatementLast);

    switch (current.BuildState())
    {
        case 0:
        {
            switch (t.Type())
            {
                case TokenType::KBreak:
                case TokenType::KContinue:
                    return AppendTerminal(std::move(s), 2, t, consumed);
                case TokenType::KReturn:
                    return AppendTerminal(std::move(s), 1, t, consumed);
                default:
                    return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
            }
        }
        case 1: // accept ExpressionList
            return Push(std::move(s), 2, NodeBuilder{NodeType::ExpressionList, t.Source(), 2});
        case 2: // expect ;
            if (t.Type() == TokenType::PSemicolon)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::SideEffect(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::SideEffect);

    switch (current.BuildState())
    {
        case 0: // expect Lval
            return Push(std::move(s), 1, NodeBuilder{NodeType::Lval, t.Source()});
        case 1: // accept op assignement
            switch (t.Type())
            {
                case TokenType::OAddAssign:         case TokenType::OSubtractAssign:
                case TokenType::OMultiplyAssign:    case TokenType::ODivideAssign:
                case TokenType::OModulusAssign:     case TokenType::OAppendAssign:
                case TokenType::OAndAssign:         case TokenType::OOrAssign:
                    return AppendTerminal(std::move(s), 2, t, consumed);
                default:
                    return UpdateBuildState(std::move(s), 10);
            }
        case 2: // expect Expression
            return Push(std::move(s), 200, NodeBuilder{NodeType::Expression, t.Source()});
        case 10: // accept ,
            if (t.Type() == TokenType::PComma)
                {return ConsumeToken(std::move(s), 11, t, consumed);}
            else
                {return UpdateBuildState(std::move(s), 20);}
        case 11: // expect Lval
            return Push(std::move(s), 12, NodeBuilder{NodeType::Lval, t.Source()});
        case 12: // accept ,
            if (t.Type() == TokenType::PComma)
                {return ConsumeToken(std::move(s), 11, t, consumed);}
            else
                {return UpdateBuildState(std::move(s), 21);}
        case 20: // accept =
            if (t.Type() == TokenType::OAssign)
                {return AppendTerminal(std::move(s), 22, t, consumed);}
            else
                {return Pop(std::move(s));}
        case 21: // expect =
            if (t.Type() == TokenType::OAssign)
                {return AppendTerminal(std::move(s), 22, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 22: // expect ExpressionList
            return Push(std::move(s), 200, NodeBuilder{NodeType::ExpressionList, t.Source()});
        case 200:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::DefinitionAtom(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::DefinitionAtom);

    switch (current.BuildState())
    {
        case 0: // expect atom
            if (t.Type() == TokenType::KAtom)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect IdAtom
            if (t.Type() == TokenType::IdAtom)
            {
                s = AppendTerminal(std::move(s), 255, t, consumed);
                return Pop(std::move(s));
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::DefinitionVariable(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::DefinitionVariable);

    switch (current.BuildState())
    {
        case 0: // expect var
            if (t.Type() == TokenType::KVar)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect IdVar
            if (t.Type() == TokenType::IdVar)
            {
                s = AppendTerminal(std::move(s), 255, t, consumed);
                return Pop(std::move(s));
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::Expression(ParserState&& s, Token const& t, bool& /*consumed*/) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Expression);

    switch (current.BuildState())
    {
        case 0: // expect ExpressionMath0
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath0, t.Source()});
        case 1: // done
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath0(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath0);

    switch (current.BuildState())
    {
        case 0: // expect ExpressionMath1
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath1, t.Source()});
        case 1: // accept || or &&
            switch (t.Type())
            {
                case TokenType::OAnd:
                case TokenType::OOr:
                    return AppendTerminal(std::move(s), 0, t, consumed);
                default:
                    return Pop(std::move(s));
            }
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath1(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath1);

    switch (current.BuildState())
    {
        case 0: // expect ExpressionMath2
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath2, t.Source()});
        case 1: // accept ==, !=, <, <=, >, or >=
            switch (t.Type())
            {
                case TokenType::OEqual:
                case TokenType::ONotEqual:
                case TokenType::OLess:
                case TokenType::OLessEqual:
                case TokenType::OGreater:
                case TokenType::OGreaterEqual:
                    return AppendTerminal(std::move(s), 2, t, consumed);
                default:
                    return Pop(std::move(s));
            }
        case 2: // expect ExpressionMath2
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath2, t.Source()});
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath2(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath2);

    switch (current.BuildState())
    {
        case 0: // expect ExpressionMath3
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath3, t.Source()});
        case 1: // accept ..
            switch (t.Type())
            {
                case TokenType::OAppend:
                    return AppendTerminal(std::move(s), 0, t, consumed);
                default:
                    return Pop(std::move(s));
            }
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath3(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath3);

    switch (current.BuildState())
    {
        case 0: // expect ExpressionMath4
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath4, t.Source()});
        case 1: // accept + or -
            switch (t.Type())
            {
                case TokenType::OAdd:
                case TokenType::OSubtract:
                    return AppendTerminal(std::move(s), 0, t, consumed);
                default:
                    return Pop(std::move(s));
            }
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath4(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath4);

    switch (current.BuildState())
    {
        case 0: // expect ExpressionMath5
            return Push(std::move(s), 1, NodeBuilder{NodeType::ExpressionMath5, t.Source()});
        case 1: // accept *, /, or %
            switch (t.Type())
            {
                case TokenType::OMultiply:
                case TokenType::ODivide:
                case TokenType::OModulus:
                    return AppendTerminal(std::move(s), 0, t, consumed);
                default:
                    return Pop(std::move(s));
            }
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath5(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath5);

    switch (current.BuildState())
    {
        case 0: // accept !, -, #, ++x, --x, or ExpressionMath6
            switch (t.Type())
            {
                case TokenType::OSubtract:
                    return AppendTerminal(std::move(s), 1, NodeType::ONegate, t, consumed);
                case TokenType::OSize:
                case TokenType::ONot:
                    return AppendTerminal(std::move(s), 1, t, consumed);
                case TokenType::OIncrement:
                    return AppendTerminal(std::move(s), 1, NodeType::OPreIncrement, t, consumed);
                case TokenType::ODecrement:
                    return AppendTerminal(std::move(s), 1, NodeType::OPreDecrement, t, consumed);
                default:
                    return UpdateBuildState(std::move(s), 11);
            }
        case 1: // expect ExpressionMath6
            return Push(std::move(s), 2, NodeBuilder{NodeType::ExpressionMath6, t.Source()});
        case 2: // we're done
            return Pop(std::move(s));
        case 11: // expect ExpressionMath6
            return Push(std::move(s), 12, NodeBuilder{NodeType::ExpressionMath6, t.Source()});
        case 12: // accept x++, x--
            switch (t.Type())
            {
                case TokenType::OIncrement:
                    s = AppendTerminal(std::move(s), 255, NodeType::OPostIncrement, t, consumed);
                    return Pop(std::move(s));
                case TokenType::ODecrement:
                    s = AppendTerminal(std::move(s), 255, NodeType::OPostDecrement, t, consumed);
                    return Pop(std::move(s));
                default:
                    return Pop(std::move(s));
            }
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionMath6(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionMath6);

    switch (current.BuildState())
    {
        case 0: // expect rval or (
            switch (t.Type())
            {
                case TokenType::POpen:
                    return ConsumeToken(std::move(s), 10, t, consumed);
                default:
                    return Push(std::move(s), 20, NodeBuilder{NodeType::Rval, t.Source()});
            }
        case 10: // expect expression
            return Push(std::move(s), 11, NodeBuilder{NodeType::Expression, t.Source()});
        case 11: // expect )
            if (t.Type() == TokenType::PClose)
                {return ConsumeToken(std::move(s), 20, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 20:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::ExpressionList(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ExpressionList);

    switch (current.BuildState())
    {
        case 0: // expect Expression
            return Push(std::move(s), 1, NodeBuilder{NodeType::Expression, t.Source()});
        case 1: // accept ,
            if (t.Type() == TokenType::PComma)
                {return ConsumeToken(std::move(s), 0, t, consumed);}
            else
                {return Pop(std::move(s));}
        default:
            assert(false);
    }
}

ParserState Parser::If(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::If);

    switch (current.BuildState())
    {
        case 0: // expect if
            if (t.Type() == TokenType::KIf)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect IfCondition
            return Push(std::move(s), 2, NodeBuilder{NodeType::IfCondition, t.Source()});
        case 2: // accept else
            if (t.Type() == TokenType::KElse)
                {return ConsumeToken(std::move(s), 3, t, consumed);}
            else
                {return Pop(std::move(s));}
        case 3: // accept if, {
            switch (t.Type())
            {
                case TokenType::KIf:
                    return ConsumeToken(std::move(s), 1, t, consumed);
                case TokenType::POpenCurly:
                    return ConsumeToken(std::move(s), 4, t, consumed);
                default:
                    return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
            }
        case 4: // expect Block
            return Push(std::move(s), 5, NodeBuilder{NodeType::Block, t.Source()});
        case 5: // expect }
            if (t.Type() == TokenType::PCloseCurly)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::IfCondition(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::IfCondition);

    switch (current.BuildState())
    {
        case 0: // expect expression
            return Push(std::move(s), 1, NodeBuilder{NodeType::Expression, t.Source()});
        case 1: // expect {
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 2, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect Block
            return Push(std::move(s), 3, NodeBuilder{NodeType::Block, t.Source()});
        case 3: // expect }
            if (t.Type() == TokenType::PCloseCurly)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::Loop(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Loop);

    switch (current.BuildState())
    {
        case 0: // accept while
            return Push(std::move(s), 100, NodeBuilder{NodeType::LoopWhile, t.Source(), 1});
        case 1: // accept do
            return Push(std::move(s), 100, NodeBuilder{NodeType::LoopDo, t.Source(), 2});
        case 2: // accept for
            return Push(std::move(s), 100, NodeBuilder{NodeType::LoopFor, t.Source(), 3});
        case 3: // out of options
            return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
        case 100:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::LoopWhile(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::LoopWhile);

    switch (current.BuildState())
    {
        case 0: // expect while
            if (t.Type() == TokenType::KWhile)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect Expression
            return Push(std::move(s), 2, NodeBuilder{NodeType::Expression, t.Source()});
        case 2: // expect {
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 3, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 3: // expect Block
            return Push(std::move(s), 4, NodeBuilder{NodeType::Block, t.Source()});
        case 4: // expect }
            if (t.Type() == TokenType::PCloseCurly)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::LoopDo(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::LoopDo);

    switch (current.BuildState())
    {
        case 0: // expect do
            if (t.Type() == TokenType::KDo)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect {
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 2, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect Block
            return Push(std::move(s), 3, NodeBuilder{NodeType::Block, t.Source()});
        case 3: // expect }
            if (t.Type() == TokenType::PCloseCurly)
                {return ConsumeToken(std::move(s), 4, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 4: // expect while
            if (t.Type() == TokenType::KWhile)
                {return ConsumeToken(std::move(s), 5, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 5: // expect Expression
            return Push(std::move(s), 6, NodeBuilder{NodeType::Expression, t.Source()});
        case 6: // expect ;
            if (t.Type() == TokenType::PSemicolon)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::LoopFor(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::LoopFor);

    switch (current.BuildState())
    {
        case 0: // expect for
            if (t.Type() == TokenType::KFor)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect IdVar
            if (t.Type() == TokenType::IdVar)
                {return AppendTerminal(std::move(s), 2, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect =
            if (t.Type() == TokenType::OAssign)
                {return ConsumeToken(std::move(s), 3, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 3: // expect Expression
            return Push(std::move(s), 4, NodeBuilder{NodeType::Expression, t.Source()});
        case 4: // expect ,
            if (t.Type() == TokenType::PComma)
                {return ConsumeToken(std::move(s), 5, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 5: // expect Expression
            return Push(std::move(s), 6, NodeBuilder{NodeType::Expression, t.Source()});
        case 6: // accept , or skip to 8
            if (t.Type() == TokenType::PComma)
                {return ConsumeToken(std::move(s), 7, t, consumed);}
            else
                {return UpdateBuildState(std::move(s), 8);}
        case 7: // expect Expression
            return Push(std::move(s), 8, NodeBuilder{NodeType::Expression, t.Source()});
        case 8: // expect {
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 9, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 9: // expect Block
            return Push(std::move(s), 10, NodeBuilder{NodeType::Block, t.Source()});
        case 10: // expect }
            if (t.Type() == TokenType::PCloseCurly)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::Lval(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Lval);

    switch (current.BuildState())
    {
        case 0: // accept DefinitionVariable
            return Push(std::move(s), 200, NodeBuilder{NodeType::DefinitionVariable, t.Source(), 1});
        case 1: // accept Object
            return Push(std::move(s), 200, NodeBuilder{NodeType::Object, t.Source(), 2});
        case 2: // out of options
            return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
        case 200:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::Rval(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Rval);

    switch (current.BuildState())
    {
        case 0: // accept nil, true, false, IdAtom, Number
            switch (t.Type())
            {
                case TokenType::IdAtom:
                case TokenType::Number:
                case TokenType::KFalse:
                case TokenType::KNil:
                case TokenType::KTrue:
                    return AppendTerminal(std::move(s), 200, t, consumed);
                default:
                    return UpdateBuildState(std::move(s), 1);
            }
        case 1: // accept string
            return Push(std::move(s), 200, NodeBuilder{NodeType::String, t.Source(), 2});
        case 2: // accept Constructor
            return Push(std::move(s), 200, NodeBuilder{NodeType::Constructor, t.Source(), 3});
        case 3: // accept object
            return Push(std::move(s), 200, NodeBuilder{NodeType::Object, t.Source(), 4});
        case 4: // out of options
            return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
        case 200:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::Object(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Object);

    switch (current.BuildState())
    {
        case 0: // expect IdVar
            if (t.Type() == TokenType::IdVar)
                {return AppendTerminal(std::move(s), 10, t, consumed);}
            else if (t.Type() == TokenType::KRecurse)
                {return AppendTerminal(std::move(s), 2, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect ObjectFunction
            return Push(std::move(s), 10, NodeBuilder{NodeType::ObjectFunction, t.Source()});
        case 10: // accept ObjectFunction
            return Push(std::move(s), 10, NodeBuilder{NodeType::ObjectFunction, t.Source(), 11});
        case 11: // accept ObjectSubscript
            return Push(std::move(s), 10, NodeBuilder{NodeType::ObjectSubscript, t.Source(), 12});
        case 12: // accept ObjectMemberAccess
            return Push(std::move(s), 10, NodeBuilder{NodeType::ObjectMemberAccess, t.Source(), 13});
        case 13: // accept ObjectMemberCall
            return Push(std::move(s), 10, NodeBuilder{NodeType::ObjectMemberCall, t.Source(), 99});
        case 99:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::ObjectFunction(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ObjectFunction);

    switch (current.BuildState())
    {
        case 0: // expect (
            if (t.Type() == TokenType::POpen)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // accept ExpressionList
            return Push(std::move(s), 2, NodeBuilder{NodeType::ExpressionList, t.Source(), 2});
        case 2: // expect )
            if (t.Type() == TokenType::PClose)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::ObjectSubscript(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ObjectSubscript);

    switch (current.BuildState())
    {
        case 0: // expect [
            if (t.Type() == TokenType::POpenSquare)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect Expression
            return Push(std::move(s), 2, NodeBuilder{NodeType::Expression, t.Source()});
        case 2: // expect )
            if (t.Type() == TokenType::PCloseSquare)
            {
                consumed = true;
                return Pop(std::move(s), t.Source().End);
            }
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        default:
            assert(false);
    }
}

ParserState Parser::ObjectMemberAccess(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ObjectMemberAccess);

    switch (current.BuildState())
    {
        case 0: // expect .
            if (t.Type() == TokenType::OMemberAccess)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect ID_VAR
            if (t.Type() == TokenType::IdVar)
                {return AppendTerminal(std::move(s), 2, NodeType::IdAtom, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::ObjectMemberCall(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ObjectMemberCall);

    switch (current.BuildState())
    {
        case 0: // expect :
            if (t.Type() == TokenType::OMemberCall)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect ID_VAR
            if (t.Type() == TokenType::IdVar)
                {return AppendTerminal(std::move(s), 2, NodeType::IdAtom, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect ObjectFunction
            return Push(std::move(s), 3, NodeBuilder{NodeType::ObjectFunction, t.Source()});
        case 3:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::IdVarList(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::IdVarList);

    switch (current.BuildState())
    {
        case 0: // accept LIdVar
            if (t.Type() == TokenType::IdVar)
                {return AppendTerminal(std::move(s), 1, t, consumed);}
            else
                {return UpdateBuildState(std::move(s), 200);}
        case 1: // accept ,
            if (t.Type() == TokenType::PComma)
                {return ConsumeToken(std::move(s), 2, t, consumed);}
            else
                {return UpdateBuildState(std::move(s), 200);}
        case 2: // expect LIdVar
            if (t.Type() == TokenType::IdVar)
                {return AppendTerminal(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 200:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::Constructor(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::Constructor);

    switch (current.BuildState())
    {
        case 0: // accept ConstructorList
            return Push(std::move(s), 200, NodeBuilder{NodeType::ConstructorList, t.Source(), 1});
        case 1: // accept ConstructorTable
            return Push(std::move(s), 200, NodeBuilder{NodeType::ConstructorTable, t.Source(), 2});
        case 2: // accept ConstructorFunction
            return Push(std::move(s), 200, NodeBuilder{NodeType::ConstructorFunction, t.Source(), 3});
        case 3:
            return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);
        case 200:
            return Pop(std::move(s));
        default:
            assert(false);
    }}

ParserState Parser::ConstructorList(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ConstructorList);

    switch (current.BuildState())
    {
        case 0: // expect lst
            if (t.Type() == TokenType::KLst)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect [
            if (t.Type() == TokenType::POpenSquare)
                {return ConsumeToken(std::move(s), 2, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect ]
            if (t.Type() == TokenType::PCloseSquare)
                {return ConsumeToken(std::move(s), 3, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 3:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::ConstructorTable(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ConstructorTable);

    switch (current.BuildState())
    {
        case 0: // expect tbl
            if (t.Type() == TokenType::KTbl)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect [
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 2, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 2: // expect ]
            if (t.Type() == TokenType::PCloseCurly)
                {return ConsumeToken(std::move(s), 3, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 3:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::ConstructorFunction(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::ConstructorFunction);

    switch (current.BuildState())
    {
        case 0: // expect fn
            if (t.Type() == TokenType::KFn)
                {return ConsumeToken(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // expect IdVarList
            return Push(std::move(s), 2, NodeBuilder{NodeType::IdVarList, t.Source()});
        case 2: // expect {
            if (t.Type() == TokenType::POpenCurly)
                {return ConsumeToken(std::move(s), 3, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 3: // expect Block
            return Push(std::move(s), 4, NodeBuilder{NodeType::Block, t.Source()});
        case 4: // expect }
            if (t.Type() == TokenType::PCloseCurly)
                {return ConsumeToken(std::move(s), 5, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 5:
            return Pop(std::move(s));
        default:
            assert(false);
    }
}

ParserState Parser::String(ParserState&& s, Token const& t, bool& consumed) noexcept
{
    auto& current = s.Stack().back();
    assert(current.Type() == NodeType::String);

    switch (current.BuildState())
    {
        case 0: // expect String
            if (t.Type() == TokenType::String)
                {return AppendTerminal(std::move(s), 1, t, consumed);}
            else
                {return Error(std::move(s), ErrorType::UnexpectedToken, t, consumed);}
        case 1: // accept String
            if (t.Type() == TokenType::String)
                {return AppendTerminal(std::move(s), 1, t, consumed);}
            else
                {return Pop(std::move(s));}
        default:
            assert(false);
    }
}

ParserState Parser::Error(ParserState&& s, ErrorType e, Token const& t, bool& consumed) noexcept
{
    auto source = s.Source();
    auto stack = ParserState::Consume(std::move(s));

    for (auto i = stack.rbegin(), endI = stack.rend(); i != endI; ++i)
    {
        if (i->IsTentative())
        {
            if (i->HasConsumedInput())
            {
                break;
            }

            auto fallback = i->GetParentFallbackBuildState();
            stack.erase(prev(i.base()), stack.end());

            auto& current = stack.back();
            current.SetBuildState(fallback);

            return ParserState{Node{}, std::move(stack), std::move(source)};
        }
    }

    consumed = true;
    return ParserState
    {
        Node
        {
            NodeType::Error,
            ValueHolder{e},
            NodeList{},
            SourceReference{t.Source()}
        },
        std::move(stack),
        std::move(source)
    };
}

ParserState Parser::Push(ParserState&& s, uint8_t bs, NodeBuilder&& n) noexcept
{
    auto source = s.Source();
    auto stack = ParserState::Consume(std::move(s));

    if (!stack.empty())
    {
        stack.back().SetBuildState(bs);
    }

    stack.push_back(std::move(n));
    return ParserState{Node{}, std::move(stack), std::move(source)};
}

ParserState Parser::Pop(ParserState&& s) noexcept
{
    assert(!s.Stack().empty());
    auto endMarker = s.Stack().back().Source().End;

    return Pop(std::move(s), endMarker);
}

ParserState Parser::Pop(ParserState&& s, Cursor endMarker) noexcept
{
    auto source = s.Source();
    auto stack = ParserState::Consume(std::move(s));
    assert(!stack.empty());

    auto current = std::move(stack.back());
    stack.pop_back();

    auto completedNode = Node{};

    // if the node has only one child it can be elided
    if (current.Children().size() == 1 && current.TypeAllowsElision())
    {
        completedNode = std::move(current.Children().front());

        if (!stack.empty())
        {
            // expand the extent of the parent to cover the extent of the elided node
            auto parentExtent = stack.back().Source();
            parentExtent.End = std::max(parentExtent.End, endMarker);

            stack.back().SetSource(std::move(parentExtent));
        }
    }
    else
    {
        // if the node has children set the source to the extent of all the children
        // plus the initial source reference if any
        if (!current.Children().empty())
        {
            auto currentExtent = current.Source();

            auto begin = current.Children().front().Source().Begin;
            auto end = current.Children().back().Source().End;

            currentExtent.Begin = std::min(currentExtent.Begin, begin);
            currentExtent.End = std::max(endMarker, end);

            currentExtent.Text = source.substr(
                currentExtent.Begin.Index,
                currentExtent.End.Index - currentExtent.Begin.Index
            );

            current.SetSource(std::move(currentExtent));
        }

        completedNode = NodeBuilder::Make(std::move(current));
    }

    assert(completedNode);

    if (stack.empty())
    {
        return ParserState{std::move(completedNode), std::vector<NodeBuilder>{}, std::move(source)};
    }
    else
    {
        stack.back().AppendChild(std::move(completedNode));
        return ParserState{Node{}, std::move(stack), std::move(source)};
    }
}

ParserState Parser::ConsumeToken(ParserState&& s, uint8_t bs, Token const& t, bool& consumed) noexcept
{
    auto source = s.Source();
    auto stack = ParserState::Consume(std::move(s));
    assert(!stack.empty());

    auto& current = stack.back();
    current.SetBuildState(bs);
    current.SetHasConsumedInput();

    // expand the extent of the node to cover the extent of the token
    auto extent = current.Source();
    extent.End = std::max(extent.End, t.Source().End);
    current.SetSource(std::move(extent));

    consumed = true;
    return ParserState{Node{}, std::move(stack), std::move(source)};
}

ParserState Parser::UpdateBuildState(ParserState&& s, uint8_t bs) noexcept
{
    auto source = s.Source();
    auto stack = ParserState::Consume(std::move(s));
    assert(!stack.empty());

    stack.back().SetBuildState(bs);

    return ParserState{Node{}, std::move(stack), std::move(source)};
}

ParserState Parser::AppendTerminal(ParserState&& s, uint8_t bs, Token const& t, bool& consumed) noexcept
{
    auto nodeType = NodeType::Nil;
    switch (t.Type())
    {
        case TokenType::IdAtom:                     nodeType = NodeType::IdAtom; break;
        case TokenType::IdVar:                      nodeType = NodeType::IdVar; break;
        case TokenType::Number:                     nodeType = NodeType::Number; break;
        case TokenType::String:                     nodeType = NodeType::StringToken; break;
        case TokenType::KBreak:                     nodeType = NodeType::Break; break;
        case TokenType::KContinue:                  nodeType = NodeType::Continue; break;
        case TokenType::KFalse:                     nodeType = NodeType::KFalse; break;
        case TokenType::KNil:                       nodeType = NodeType::KNil; break;
        case TokenType::KRecurse:                   nodeType = NodeType::Recurse; break;
        case TokenType::KReturn:                    nodeType = NodeType::Return; break;
        case TokenType::KTrue:                      nodeType = NodeType::KTrue; break;
        case TokenType::OAdd:                       nodeType = NodeType::OAdd; break;
        case TokenType::OSubtract:                  nodeType = NodeType::OSubtract; break;
        case TokenType::OMultiply:                  nodeType = NodeType::OMultiply; break;
        case TokenType::ODivide:                    nodeType = NodeType::ODivide; break;
        case TokenType::OModulus:                   nodeType = NodeType::OModulus; break;
        case TokenType::OSize:                      nodeType = NodeType::OSize; break;
        case TokenType::OEqual:                     nodeType = NodeType::OEqual; break;
        case TokenType::ONotEqual:                  nodeType = NodeType::ONotEqual; break;
        case TokenType::OLess:                      nodeType = NodeType::OLess; break;
        case TokenType::OLessEqual:                 nodeType = NodeType::OLessEqual; break;
        case TokenType::OGreater:                   nodeType = NodeType::OGreater; break;
        case TokenType::OGreaterEqual:              nodeType = NodeType::OGreaterEqual; break;
        case TokenType::OAnd:                       nodeType = NodeType::OAnd; break;
        case TokenType::OOr:                        nodeType = NodeType::OOr; break;
        case TokenType::ONot:                       nodeType = NodeType::ONot; break;
        case TokenType::OAppend:                    nodeType = NodeType::OAppend; break;
        case TokenType::OAssign:                    nodeType = NodeType::OAssign; break;
        case TokenType::OAddAssign:                 nodeType = NodeType::OAddAssign; break;
        case TokenType::OSubtractAssign:            nodeType = NodeType::OSubtractAssign; break;
        case TokenType::OMultiplyAssign:            nodeType = NodeType::OMultiplyAssign; break;
        case TokenType::ODivideAssign:              nodeType = NodeType::ODivideAssign; break;
        case TokenType::OModulusAssign:             nodeType = NodeType::OModulusAssign; break;
        case TokenType::OAndAssign:                 nodeType = NodeType::OAndAssign; break;
        case TokenType::OOrAssign:                  nodeType = NodeType::OOrAssign; break;
        case TokenType::OAppendAssign:              nodeType = NodeType::OAppendAssign; break;

        default:
            assert(false);
    }

    return AppendTerminal(std::move(s), bs, nodeType, t, consumed);
}

ParserState Parser::AppendTerminal(
    ParserState&& s,
    uint8_t bs,
    NodeType n,
    Token const& t,
    bool& consumed
) noexcept
{
    auto source = s.Source();
    auto stack = ParserState::Consume(std::move(s));
    assert(!stack.empty());

    auto node = NodeBuilder{n, t.Source()};
    node.SetValue(ValueHolder{t.Value()});

    stack.back().SetBuildState(bs);
    stack.back().AppendChild(NodeBuilder::Make(std::move(node)));
    stack.back().SetHasConsumedInput();

    consumed = true;
    return ParserState{Node{}, std::move(stack), std::move(source)};
}

Node AstRewrite::Rewrite(Node const& n) noexcept
{
    switch (n.Type())
    {
        case NodeType::Error:
            return n;

        // simple cases (just recurse or leaves)
        case NodeType::Block:
        case NodeType::DefinitionAtom:
        case NodeType::DefinitionVariable:
        case NodeType::ExpressionList:
        case NodeType::If:
        case NodeType::IfCondition:
        case NodeType::LoopWhile:
        case NodeType::LoopDo:
        case NodeType::IdAtom:
        case NodeType::IdVar:
        case NodeType::IdVarList:
        case NodeType::ConstructorList:
        case NodeType::ConstructorTable:
        case NodeType::ConstructorFunction:
        case NodeType::String:
        case NodeType::StringToken:
        case NodeType::Number:
        case NodeType::Break:
        case NodeType::Continue:
        case NodeType::Recurse:
        case NodeType::Return:
        case NodeType::KNil:
        case NodeType::KTrue:
        case NodeType::KFalse:
            return Recurse(n);

        // rewrite cases
        case NodeType::StatementLast:
            return StatementLast(n);
        case NodeType::SideEffect:
            return SideEffect(n);
        case NodeType::ExpressionMath0:
        case NodeType::ExpressionMath1:
        case NodeType::ExpressionMath2:
        case NodeType::ExpressionMath3:
        case NodeType::ExpressionMath4:
            return BinaryExpression(n);
        case NodeType::ExpressionMath5:
            return UnaryExpression(n);
        case NodeType::LoopFor:
            return LoopFor(n);
        case NodeType::Object:
            return Object(n);

        // unexpected cases (these should always be elided)
        case NodeType::Root:
        case NodeType::Statement:
        case NodeType::Definition:
        case NodeType::Expression:
        case NodeType::ExpressionMath6:
        case NodeType::Loop:
        case NodeType::Lval:
        case NodeType::Rval:
        case NodeType::Constructor:
        default:
            assert(false);
    }
}

Node AstRewrite::Recurse(Node const& n) noexcept
{
    if (n.Children().empty())
    {
        return n;
    }

    auto newNode = NodeBuilder{n.Type(), n.Source()};
    newNode.SetValue(ValueHolder{n.Value()});

    for (auto& child : n.Children())
    {
        newNode.AppendChild(Rewrite(child));
    }

    SetNodeBuilderSource(newNode);
    return NodeBuilder::Make(std::move(newNode));
}

Node AstRewrite::StatementLast(Node const& n) noexcept
{
    assert(!n.Children().empty());

    auto& keyword = n.Children().front();
    switch (keyword.Type())
    {
        case NodeType::Break:
        case NodeType::Continue:
            return keyword;
        case NodeType::Return:
        {
            assert(n.Children().size() == 2);

            auto newReturn = NodeBuilder{keyword.Type(), keyword.Source()};
            assert(!keyword.Value());
            assert(keyword.Children().empty());

            auto& expList = n.Children().back();
            assert(expList.Type() == NodeType::ExpressionList);
            for (auto& child : expList.Children())
            {
                newReturn.AppendChild(Rewrite(child));
            }

            SetNodeBuilderSource(newReturn);
            return NodeBuilder::Make(std::move(newReturn));
        }
        default:
            assert(false);
    }
}

Node AstRewrite::SideEffect(Node const& n) noexcept
{
    assert(n.Children().size() >= 3); // lval+ op ExpressionList

    auto opI = n.Children().end() - 2;

    auto sideEffect = NodeBuilder{opI->Type(), opI->Source()};
    assert(!opI->Value());
    assert(opI->Children().empty());

    if (opI->Type() == NodeType::OAssign)
    {
        auto lvalList = NodeBuilder{NodeType::LvalList, n.Children().front().Source()};
        for (auto i = n.Children().begin(); i != opI; ++i)
        {
            lvalList.AppendChild(Rewrite(*i));
        }
        SetNodeBuilderSource(lvalList);

        sideEffect.AppendChild(NodeBuilder::Make(std::move(lvalList)));

        auto& expList = *next(opI);
        assert(expList.Type() == NodeType::ExpressionList);
        sideEffect.AppendChild(Rewrite(expList));
    }
    else
    {
        assert(n.Children().size() == 3); // lval op ExpressionList
        sideEffect.AppendChild(Rewrite(n.Children()[0]));
        sideEffect.AppendChild(Rewrite(n.Children()[2]));
    }

    SetNodeBuilderSource(sideEffect);
    return NodeBuilder::Make(std::move(sideEffect));
}

Node AstRewrite::BinaryExpression(Node const& n) noexcept
{
    assert(n.Children().size() >= 3);       // subexp (op subexp)+
    assert(n.Children().size() % 2 == 1);   // passthrough expressions are already elided

    auto lhs = Rewrite(n.Children()[0]);

    // fold the list into a left associative tree
    for (size_t i = 1, endI = n.Children().size(); i < endI; i += 2)
    {
        auto op = n.Children()[i];
        auto rhs = Rewrite(n.Children()[i + 1]);

        auto subExp = NodeBuilder{op.Type(), op.Source()};
        assert(!op.Value());
        assert(op.Children().empty());

        subExp.AppendChild(std::move(lhs));
        subExp.AppendChild(std::move(rhs));

        SetNodeBuilderSource(subExp);
        lhs = NodeBuilder::Make(std::move(subExp));
    }

    return lhs;
}

Node AstRewrite::UnaryExpression(Node const& n) noexcept
{
    assert(n.Children().size() == 2); // (op subexp) | (subexp op)

    auto lhs = n.Children()[0];
    auto rhs = n.Children()[1];


    if (rhs.Type() == NodeType::OPostIncrement || rhs.Type() == NodeType::OPostDecrement)
    {
        // normalize structure
        std::swap(lhs, rhs);
    }

    // at this point lhs is always the operator and rhs is always the subexp
    assert(!lhs.Value());
    assert(lhs.Children().empty());

    if (lhs.Type() == NodeType::ONegate && rhs.Type() == NodeType::Number)
    {
        // precompute negative constants
        auto subExp = NodeBuilder{NodeType::Number, lhs.Source()};
        subExp.SetValue(-rhs.Value().DoubleValue());

        SetNodeBuilderSource(subExp);
        return NodeBuilder::Make(std::move(subExp));
    }

    auto subExp = NodeBuilder{lhs.Type(), lhs.Source()};
    subExp.AppendChild(Rewrite(rhs));

    SetNodeBuilderSource(subExp);
    return NodeBuilder::Make(std::move(subExp));
}

Node AstRewrite::LoopFor(Node const& n) noexcept
{
    assert(n.Children().size() == 4 || n.Children().size() == 5); // IdVar exp exp [exp] block

    auto hasIncrement = n.Children().size() == 5;

    auto var = Rewrite(n.Children()[0]);
    auto initExp = Rewrite(n.Children()[1]);
    auto endExp = Rewrite(n.Children()[2]);
    auto incExp = hasIncrement ? n.Children()[3] : Node{};
    auto block = Rewrite(n.Children()[hasIncrement ? 4 : 3]);

    // rewrite the 4th child as increment if necessary
    // OPreIncrement
    //     var
    if (!incExp)
    {
        auto nb = NodeBuilder{NodeType::Number, var.Source()};
        nb.SetValue(1);

        incExp = NodeBuilder::Make(std::move(nb));
    }
    else
    {
        incExp = Rewrite(incExp);
    }

    auto f = NodeBuilder{NodeType::LoopFor, n.Source()};
    f.AppendChild(std::move(var));
    f.AppendChild(std::move(initExp));
    f.AppendChild(std::move(endExp));
    f.AppendChild(std::move(incExp));
    f.AppendChild(std::move(block));

    return NodeBuilder::Make(std::move(f));
}

Node AstRewrite::Object(Node const& n) noexcept
{
    assert(n.Children().size() >= 2); // (IdVar | recurse | Constructor) (ObjectFunction | ObjectSubscript | ObjectMember | ObjectMemberCall)+

    auto lhs = Rewrite(n.Children()[0]);

    // fold the list into a left associative tree
    for (size_t i = 1, endI = n.Children().size(); i < endI; ++i)
    {
        auto op = n.Children()[i];

        auto subExp = NodeBuilder{op.Type(), op.Source()};
        assert(!op.Value());

        subExp.AppendChild(std::move(lhs));

        switch (op.Type())
        {
            case NodeType::ObjectFunction:
            {
                if (!op.Children().empty())
                {
                    assert(op.Children().size() == 1);

                    auto exps = op.Children()[0];
                    assert(exps.Type() == NodeType::ExpressionList);
                    for (auto& exp : exps.Children())
                    {
                        subExp.AppendChild(Rewrite(exp));
                    }
                }
            }
            break;
            case NodeType::ObjectSubscript:
            {
                assert(op.Children().size() == 1);

                auto exp = op.Children()[0];
                subExp.AppendChild(Rewrite(exp));
            }
            break;
            case NodeType::ObjectMemberAccess:
            {
                assert(op.Children().size() == 1);

                auto atom = op.Children()[0];
                assert(atom.Type() == NodeType::IdAtom);
                subExp.AppendChild(Rewrite(atom));
            }
            break;
            case NodeType::ObjectMemberCall:
            {
                assert(op.Children().size() == 2);

                auto atom = op.Children()[0];
                assert(atom.Type() == NodeType::IdAtom);
                subExp.AppendChild(Rewrite(atom));

                auto call = op.Children()[1];
                assert(call.Type() == NodeType::ObjectFunction);
                if (!call.Children().empty())
                {
                    assert(call.Children().size() == 1);

                    auto exps = call.Children()[0];
                    assert(exps.Type() == NodeType::ExpressionList);
                    for (auto& exp : exps.Children())
                    {
                        subExp.AppendChild(Rewrite(exp));
                    }
                }
            }
            break;
            default:
                assert(false);
        }

        SetNodeBuilderSource(subExp);
        lhs = NodeBuilder::Make(std::move(subExp));
    }

    return lhs;
}

void AstRewrite::SetNodeBuilderSource(NodeBuilder& n) noexcept
{
    auto source = n.Source();

    for (auto& child : n.Children())
    {
        source.Begin = std::min(source.Begin, child.Source().Begin);
        source.End = std::max(source.End, child.Source().End);
    }

    n.SetSource(std::move(source));
}

}
