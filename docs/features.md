# Features

+ Basic language: Lua
+ With C style blocks                                                         P1
+ No metatables (for now)
+ No modules (for now)
+ No sugared helpers (for now)
+ With explicit list/array type                                               P1
    * With slicing                                                            P2
    * Use deque underneath for fast append at both ends?
    * Functional linked lists?
    * Pattern matching on list?
+ Mandatory variable declaration                                              P1
    * No default to global!
    * Const variable binding                                                  P3
+ Function definition                                                         P1
+ Closure definition                                                          P3
+ Pure function & closure                                                     P4
    * Upvalues are read only
    * Only const binding is allowed
+ Everything is an expression?                                                P2 (if impl)
    * Cheat and give statements value of nil?
+ Atoms                                                                       P1
    * Require definition
+ Numbers
    * Double                                                                  P1
    * Int & float?                                                            P2
+ Pattern matching for multiple returns
    * Match nil vs non nil arguments                                          P3
    * Match other return types                                                p4
    * try/catch sugar for error handling?                                     P4

********************************************************************************

# Thoughts

##evaluation order
###of function args
    left to right? right to left? which is going to be more efficient?
###of assignments
    do lhs after rhs?
##multiple returns
    pattern match on returns?
        just nil / not nil?
##tuples?
    does it need to be explicit with multiple returns + vararg?
##array + slices
    pattern match on array / slice?
##pure vs impure functions
    track difference in ast?
##let vs var
    track binding vs lvalue vs rvalue?
##expression everything?

##upvalues
    evnironment is a table
    import onto stack at start of function
    spill modified upvalues into table on return
