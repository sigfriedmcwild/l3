    // "primitive" types
    // nil                 // nil_t
    // 0 1.5 .3            // number <- double vs float vs int
    // "string"            // string
    // true false          // bool

    // constructors
    // []                  // array
    // {}                  // table
    // fn (...) { ... }    // closure

    // atom definition
    atom @Atom;

    // function definition
    //fn(arg1, arg2)
    //{
    //    return arg1 + arg2;
    //}

    //a, var b = blah();

    // var def
    var v0;             // defaults to nil
    var v1 = 0;         // number
    var v2 = "blah";    // string
    var v3 = true;      // bool
    var v4 = [];        // array
    var v5 = {};        // table
    var v6 = @Atom;     // atom
    var v7 = fn {};     // function

    // basics
    var basics = fn
    {
        var a, b, c = 1, "foo", @Atom;
        var e;

        var foo = fn { return nil, 3; };
        e, var res = foo();
    };

    // arrays
    var arrays = fn
    {
        var a = []; // defaults to empty
        a[0] = "stuff";
        a[1] = {};
        a[5] = @Atom;

        var s = #a; // size

        var n = a[4]; // defaults to nil

        push_back(a, 5);
        var v0 = pop_back(a);

        var a0 = [];
        var a1 = a .. a0; // concatenation

        // maybe
        push_front(a, -1);
        var v1 = pop_front(a);

        // soon
        // iteration
        // slicing

        // later
        //a.push_back(2);
        //var v0 = a.pop_back();
        //a.push_front(3);
        //var v1 = a.pop_front();
        //
        //var a2 = [1, 2, 3];
    };

    // tables
    var tables = fn
    {
        var t = {}; // defaults to empty

        t[0] = true;
        var n = t[0];

        t["key"] = "value";
        var s = t["key"];
        t["key"] = nil; // clears the value at key

        var e = #t; // number of elements

        var t0 = {};
        var t1 = t .. t0; // merge tables (rhs overwrites lhs in case of conflict)

        // soon
        // iteration

        // later
        //t.key = @Atom;
        //var a = t.key;

        //var t2 = {["a"] = 2, [5] = "f"};
    };

    // control flow
    var control_flow = fn
    {
        if bool_expression
        {
            code
        }

        if bool_expression
        {
            code
        }
        else
        {
            code
        }

        if bool_expression
        {
            code
        }
        else if bool_expression
        {
            code
        }
        else
        {
            code
        }

        while bool_expression
        {
            code
        }

        // do while?

        // for forms?
    };

    // number syntax
    var numbers = fn
    {
        var abs = #(-5); // gives the magnitude

        var v0 = 0;
        var v1 = 0.;
        var v2 = .0;
        var v3 = 1;
        var v4 = 1.;
        var v5 = 1.1;
        var v6 = .1;
        var v0 = 0e1;
        var v1 = 0.e1;
        var v2 = .0e1;
        var v3 = 1e1;
        var v4 = 1.e1;
        var v5 = 1.1e1;
        var v6 = .1e1;
        var v0 = 0e+1;
        var v1 = 0.e+1;
        var v2 = .0e+1;
        var v3 = 1e+1;
        var v4 = 1.e+1;
        var v5 = 1.1e+1;
        var v6 = .1e+1;
        var v0 = 0e-1;
        var v1 = 0.e-1;
        var v2 = .0e-1;
        var v3 = 1e-1;
        var v4 = 1.e-1;
        var v5 = 1.1e-1;
        var v6 = .1e-1;
    };

    // things the grammar allows that I don't like
    var bad_stuff = fn
    {
        v0; // **
        bad_stuff() = 5; // **
    };
