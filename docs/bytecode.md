# Bytecode

## Instructions

### No op
    pre:  _
    post: _
+ noop

### Control Flow
+ call ARGS RETURNS
    pre:  _:f:args[ARGS]
    post: _:f(args)[RETURNS]
+ return COUNT
    pre:  _:a[COUNT]
    post: special
+ jump ADDRESS
    pre:  _
    post: _
+ jump_relative OFFSET
    pre:  _
    post: _
+ jump_if: ADDRESS
    pre:  _:v
    post: _
+ jump_relative_if: OFFSET
    pre:  _:v
    post: _
+ jump_if_not: ADDRESS
    pre:  _:v
    post: _
+ jump_relative_if_not: OFFSET
    pre:  _:v
    post: _

### Push
    pre:  _
    post: _:v
+ push_nil
+ push_atom ATOM_TABLE_REF
+ push_number VALUE
+ push_string STRING_TABLE_REF
+ push_true
+ push_false
+ push_list
+ push_table
+ push_closure FUNCTION_POINTER

### Pop
    pre:  _:v
    post: _
+ pop

### Binary operators
    pre:  _:a:b
    post: _:(a op b)
+ add
+ subtract
+ multiply
+ divide
+ modulus
+ and
+ or
+ append

### Unary operators
    pre:  _:a
    post: _:a'
+ size
+ negate
+ not

### Stack read write
+ read INDEX
    pre:  _:a(INDEX):_
    post: _:a(INDEX):_:a
+ write INDEX
    pre:  _:a(INDEX):_:b
    post: _:b(INDEX):_
+ level INDEX
    pre:  _(INDEX):_
    post: _(INDEX)
    or
    pre:  _: : (INDEX)
    post: _:nil:nil(INDEX)
+ alloc INDEX

### Indexing into lists and tables
+ subscript_read
    pre:  _:t:k
    post: _:t[k]
+ subscript_write
    pre:  _:v:t:k
    post: _

### Comparisons
    pre:  _:a:b
    post: _:bool
+ equal
+ not_equal
+ less
+ less_equal
+ greater
+ greater_equal

### Increments
    pre:  _:a(INDEX):_
    post: _:a'(INDEX):_
+ pre_increment INDEX
+ pre_decrement INDEX
+ post_increment INDEX
+ post_decrement INDEX

### Indexed Increments
    pre:  _:t:k
    post: _:t[k]'
+ subscript_pre_increment
+ subscript_pre_decrement
+ subscript_post_increment
+ subscritp_post_decrement

### Classes
+ subscript_member_read ATOM_TABLE_REF
    pre:  _:t
    post: _:t[ATOM_TABLE_REF]
+ subscript_member_write ATOM_TABLE_REF
    pre:  _:v:t
    post: _
+ subscript_self_call_prep ATOM_TABLE_REF
    pre:  _:t
    post: _:t[ATOM_TABLE_REF]:t
