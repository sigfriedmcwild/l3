# Grammar

    root = [BEGIN] [block] EOF

    block = {statement} [statement_last]

    statement = ((side_effect | definition_atom) ";") | if | loop | "{" block "}"
    statement_last = ("break" | "continue" | "return" [expression_list]) ";"

    side_effect = lval [(("+=" | "-=" | "*=" | "/=" | "%=" | "&&=" | "||=" | "..=") expression | {"," lval} "=" expression_list)]

    definition_atom = "atom" ID_ATOM
    definition_variable = "var" ID_VAR

    expression = expression_math_0
    expression_math_0 = expression_math_1 {("||" | "&&") expression_math_1}
    expression_math_1 = expression_math_2 ("==" | "!=" | "<" | "<=" | ">" | ">=") expression_math_2
    expression_math_2 = expression_math_3 {".." expression_math_3}
    expression_math_3 = expression_math_4 {("+" | "-") expression_math_4}
    expression_math_4 = expression_math_5 {("*" | "/" | "%") expression_math_5}
    expression_math_5 = ("!" | "-" | "#" | "++" | "--") expression_math_6 | expression_math_6 ["++" | "--"]
    expression_math_6 = rval | "(" expression ")"

    expression_list = expression {, expression}

    if = "if" expression "{" block "}" {"else" "if" expression "{" block "}"} ["else" "{" block "}"]

    loop = loop_while | loop_do | loop_for
    loop_while = "while" expression "{" block "}"
    loop_do = "do" "{" block "}" "while" expression ";"
    loop_for = "for" ID_VAR "=" expression "," expression ["," expression] "{" block "}"

    lval = definition_variable | object
    rval = "nil" | "true" | "false" | ID_ATOM | NUMBER | string | constructor | object

    object = (ID_VAR | recurse object_function) {object_function | object_subscript | object_member | object_memberCall}
    object_function = "(" [expression_list] ")"
    object_subscript = "[" expression "]"
    object_member = . ID_VAR
    object_memberCall = : ID_VAR object_function

    ID_VAR = ({"_"} ID_SUFFIX_NO_UNDERSCORE | LETTER) {ID_SUFFIX}
    ID_ATOM = "@" ID_VAR

    id_var_list = ID_VAR {"," ID_VAR}

    // TODO consider using lst and tbl to introduce list and table constructors
    // this will allow using tables constructors as free standing statements with
    // no collision with blocks
    constructor = "lst" "[" "]" | "tbl" "{" "}" | "fn" id_var_list "{" block "}"

    string = STRING {STRING}

    NUMBER = ("0" ["."] | DIGIT_NON_ZERO {DIGIT} ["."] | [DIGIT_NON_ZERO {DIGIT}] "." DIGIT {DIGIT}) [("e" | "E") ["+" | "-"] DIGIT_NON_ZERO {DIGIT}]

    STRING = ("\"" {*} "\"") | ("'" {*} "'")

    COMMENT = "//" {*} NEWLINE

    LETTER =
        "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" |
        "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z" |
        "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" | "K" | "L" | "M" |
        "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z"

    DIGIT = "0" | DIGIT_NON_ZERO
    DIGIT_NON_ZERO = "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"

    ID_SUFFIX_NO_UNDERSCORE = LETTER | DIGIT
    ID_SUFFIX = ID_SUFFIX_NO_UNDERSCORE | "_"

    NEWLINE = "[\r]\n"

# Keyword list

+ atom
+ break
+ continue
+ do
+ else
+ false
+ for
+ fn
+ if
+ lst
+ nil
+ recurse
+ return
+ tbl
+ true
+ var
+ while

# Operators

+ +
+ -
+ *
+ /
+ %
+ # (unary)
+ - (unary)
+ ++ (prefix)
+ ++ (postfix)
+ -- (prefix)
+ -- (postfix)
+ ==
+ !=
+ <
+ <=
+ \>
+ \>=
+ &&
+ ||
+ ! (unary)
+ ..
+ .
+ :
+ =
+ []
+ +=
+ -=
+ *=
+ /=
+ %=
+ &&=
+ ||=
+ ..=

# Symbols

+ @
+ (
+ )
+ {
+ }
+ ;
+ '
+ "
+ ,

# String escape sequences

+ \n
+ \r
+ \t
+ \0
+ \"
+ \'
+ \\
