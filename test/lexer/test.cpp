#include <istring/utility.h>
#include <lexer/lexer.h>
#include <lexer/utils.h>

#include "../harness.h"

using IString::istring;

template<size_t SIZE>
L3::LexerState SetupLexer(char const (&text)[SIZE])
{
    return L3::Lexer::Init(IString::istring{text});
}

L3::Cursor Cursor(uint32_t i)
{
    return L3::Cursor{i, 1, i + 1};
}

L3::Token TError(L3::ErrorType v, istring&& s, uint32_t b, uint32_t e)
{
    auto sr = L3::SourceReference{std::move(s), Cursor(b), Cursor(e)};
    return L3::Token::Error(v, std::move(sr));
}

L3::Token TIdAtom(istring&& v, istring&& s, uint32_t b, uint32_t e)
{
    auto sr = L3::SourceReference{std::move(s), Cursor(b), Cursor(e)};
    return L3::Token::IdAtom(std::move(v), std::move(sr));
}

L3::Token TIdVar(istring&& v, istring&& s, uint32_t b, uint32_t e)
{
    auto sr = L3::SourceReference{std::move(s), Cursor(b), Cursor(e)};
    return L3::Token::IdVar(std::move(v), std::move(sr));
}

L3::Token TNumber(double v, istring&& s, uint32_t b, uint32_t e)
{
    auto sr = L3::SourceReference{std::move(s), Cursor(b), Cursor(e)};
    return L3::Token::Number(v, std::move(sr));
}

L3::Token TSimple(L3::TokenType v, istring&& s, uint32_t b, uint32_t e)
{
    auto sr = L3::SourceReference{std::move(s), Cursor(b), Cursor(e)};
    return L3::Token::Simple(v, std::move(sr));
}

L3::Token TString(istring&& v, istring&& s, uint32_t b, uint32_t e)
{
    auto sr = L3::SourceReference{std::move(s), Cursor(b), Cursor(e)};
    return L3::Token::String(std::move(v), std::move(sr));
}

void TestEmpty(Test& t)
{
    t.BeginSection("Empty");

    char const text[] = "";

    auto l = SetupLexer(text);
    t.RecordOutcome("begin", TSimple(L3::TokenType::Begin, istring{""}, 0, 0), l);
    t.RecordOutcome("begin, is terminal", false, l.Terminal());

    l = L3::Lexer::Advance(std::move(l));
    t.RecordOutcome("eof", TSimple(L3::TokenType::Eof, istring{""}, 0, 0), l);
    t.RecordOutcome("eof, is terminal", true, l.Terminal());
}

void TestIdVar(Test& t)
{
    t.BeginSection("IdVar");

    char const text[] = "a ab abcdefg a1 a1b a23456 a_ a_b _a _abcd __a";
    auto l = SetupLexer(text);

    auto helper = [&](char const* n, auto const& ev, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TIdVar(istring{ev}, istring{ev}, eb, ee), l);
    };

    helper("one letter", "a", 0, 1);
    helper("2 letters", "ab", 2, 4);
    helper("many letters", "abcdefg", 5, 12);
    helper("letter + number", "a1", 13, 15);
    helper("embedded number", "a1b", 16, 19);
    helper("numbers", "a23456", 20, 26);
    helper("trailing underscore", "a_", 27, 29);
    helper("embedded underscore", "a_b", 30, 33);
    helper("leading underscore", "_a", 34, 36);
    helper("leading udnerscore 2", "_abcd", 37, 42);
    helper("double underscore", "__a", 43, 46);
}

void TestIdVarInvalid(Test& t)
{
    t.BeginSection("IdVar, invalid");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::InvalidId, istring{ev}, eb, ee), l);
        char terminal[255] = {};
        std::snprintf(terminal, 255, "%s, is terminal", n);
        t.RecordOutcome(terminal, true, l.Terminal());
    };

    helper("single underscore 1", "_", "_", 0, 1);
    helper("single underscore 2", "_ ", "_", 0, 1);
    helper("double underscore", "__", "__", 0, 2);
    helper("double underscore 2", "__ ", "__", 0, 2);
}

void TestIdAtom(Test& t)
{
    t.BeginSection("IdAtom");

    char const text[] = "@a @ab @abcdefg @a1 @a1b @a23456 @a_ @a_b @_a @_abcd @__a";
    auto l = SetupLexer(text);

    auto helper = [&](char const* n, auto const& ev, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TIdAtom(istring{ev}.substr(1), istring{ev}, eb, ee), l);
    };

    helper("one letter", "@a", 0, 2);
    helper("2 letters", "@ab", 3, 6);
    helper("many letters", "@abcdefg", 7, 15);
    helper("letter + number", "@a1", 16, 19);
    helper("embedded number", "@a1b", 20, 24);
    helper("numbers", "@a23456", 25, 32);
    helper("trailing underscore", "@a_", 33, 36);
    helper("embedded underscore", "@a_b", 37, 41);
    helper("leading underscore", "@_a", 42, 45);
    helper("leading udnerscore 2", "@_abcd", 46, 52);
    helper("double underscore", "@__a", 53, 57);
}

void TestIdAtomInvalid(Test& t)
{
    t.BeginSection("IdAtom, invalid");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::InvalidAtom, istring{ev}, eb, ee), l);
        char terminal[255] = {};
        std::snprintf(terminal, 255, "%s, is terminal", n);
        t.RecordOutcome(terminal, true, l.Terminal());
    };

    helper("no id 1", "@", "@", 0, 1);
    helper("no id 2", "@ ", "@", 0, 1);
    helper("no id 3", "@ blah", "@", 0, 1);
    helper("single underscore 1", "@_", "@_", 0, 2);
    helper("single underscore 2", "@_ ", "@_", 0, 2);
    helper("double underscore", "@__", "@__", 0, 3);
    helper("double underscore 2", "@__ ", "@__", 0, 3);
}

void TestDelimiters(Test& t)
{
    t.BeginSection("Delimiters");

    char const text[] = "(){}[],;";
    auto l = SetupLexer(text);

    auto helper =
        [&](char const* n, L3::TokenType et, auto const& es, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TSimple(et, istring{es}, eb, ee), l);
    };

    helper("open parenthesis", L3::TokenType::POpen, "(", 0, 1);
    helper("close parenthesis", L3::TokenType::PClose, ")", 1, 2);
    helper("open curly brace", L3::TokenType::POpenCurly, "{", 2, 3);
    helper("close curly brace", L3::TokenType::PCloseCurly, "}", 3, 4);
    helper("open square bracket", L3::TokenType::POpenSquare, "[", 4, 5);
    helper("close square bracket", L3::TokenType::PCloseSquare, "]", 5, 6);
    helper("comma", L3::TokenType::PComma, ",", 6, 7);
    helper("semicolon", L3::TokenType::PSemicolon, ";", 7, 8);
}

void TestKeywords(Test& t)
{
    t.BeginSection("Keywords");

    char const text[] = "atom break continue do else false for fn if lst nil recurse return tbl"
        " true var while";
    auto l = SetupLexer(text);

    auto helper = [&](char const* n, L3::TokenType et, auto const& es, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TSimple(et, istring{es}, eb, ee), l);
    };

    helper("atom", L3::TokenType::KAtom, "atom", 0, 4);
    helper("break", L3::TokenType::KBreak, "break", 5, 10);
    helper("continue", L3::TokenType::KContinue, "continue", 11, 19);
    helper("do", L3::TokenType::KDo, "do", 20, 22);
    helper("else", L3::TokenType::KElse, "else", 23, 27);
    helper("false", L3::TokenType::KFalse, "false", 28, 33);
    helper("for", L3::TokenType::KFor, "for", 34, 37);
    helper("fn", L3::TokenType::KFn, "fn", 38, 40);
    helper("if", L3::TokenType::KIf, "if", 41, 43);
    helper("lst", L3::TokenType::KLst, "lst", 44, 47);
    helper("nil", L3::TokenType::KNil, "nil", 48, 51);
    helper("recurse", L3::TokenType::KRecurse, "recurse", 52, 59);
    helper("return", L3::TokenType::KReturn, "return", 60, 66);
    helper("tbl", L3::TokenType::KTbl, "tbl", 67, 70);
    helper("true", L3::TokenType::KTrue, "true", 71, 75);
    helper("var", L3::TokenType::KVar, "var", 76, 79);
    helper("while", L3::TokenType::KWhile, "while", 80, 85);
}

void TestOperatorsSimple(Test& t)
{
    t.BeginSection("Operators, simple cases");

    char const text[] = "+ - * / % # ++ -- == != < <= > >= && || ! . : .."
        " = += -= *= /= %= &&= ||= ..=";

    auto l = SetupLexer(text);

    auto helper = [&](char const* n, L3::TokenType et, auto const& es, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TSimple(et, istring{es}, eb, ee), l);
    };

    helper("+", L3::TokenType::OAdd, "+", 0, 1);
    helper("-", L3::TokenType::OSubtract, "-", 2, 3);
    helper("*", L3::TokenType::OMultiply, "*", 4, 5);
    helper("/", L3::TokenType::ODivide, "/", 6, 7);
    helper("%", L3::TokenType::OModulus, "%", 8, 9);
    helper("#", L3::TokenType::OSize, "#", 10, 11);
    helper("++", L3::TokenType::OIncrement, "++", 12, 14);
    helper("--", L3::TokenType::ODecrement, "--", 15, 17);
    helper("==", L3::TokenType::OEqual, "==", 18, 20);
    helper("!=", L3::TokenType::ONotEqual, "!=", 21, 23);
    helper("<", L3::TokenType::OLess, "<", 24, 25);
    helper("<=", L3::TokenType::OLessEqual, "<=", 26, 28);
    helper(">", L3::TokenType::OGreater, ">", 29, 30);
    helper(">=", L3::TokenType::OGreaterEqual, ">=", 31, 33);
    helper("&&", L3::TokenType::OAnd, "&&", 34, 36);
    helper("||", L3::TokenType::OOr, "||", 37, 39);
    helper("!", L3::TokenType::ONot, "!", 40, 41);
    helper(".", L3::TokenType::OMemberAccess, ".", 42, 43);
    helper(":", L3::TokenType::OMemberCall, ":", 44, 45);
    helper("..", L3::TokenType::OAppend, "..", 46, 48);
    helper("=", L3::TokenType::OAssign, "=", 49, 50);
    helper("+=", L3::TokenType::OAddAssign, "+=", 51, 53);
    helper("-=", L3::TokenType::OSubtractAssign, "-=", 54, 56);
    helper("*=", L3::TokenType::OMultiplyAssign, "*=", 57, 59);
    helper("/=", L3::TokenType::ODivideAssign, "/=", 60, 62);
    helper("%=", L3::TokenType::OModulusAssign, "%=", 63, 65);
    helper("&&=", L3::TokenType::OAndAssign, "&&=", 66, 69);
    helper("||=", L3::TokenType::OOrAssign, "||=", 70, 73);
    helper("..=", L3::TokenType::OAppendAssign, "..=", 74, 77);
}

void TestOperatorsNoSpaces(Test& t)
{
    t.BeginSection("Operators, no spaces");

    char const text[] = "+-*/%#++--==!=<<=>>=&&||!.:..+=-=*=/=%=&&=||=..==";

    auto l = SetupLexer(text);

    auto helper = [&](char const* n, L3::TokenType et, auto const& es, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TSimple(et, istring{es}, eb, ee), l);
    };

    helper("+", L3::TokenType::OAdd, "+", 0, 1);
    helper("-", L3::TokenType::OSubtract, "-", 1, 2);
    helper("*", L3::TokenType::OMultiply, "*", 2, 3);
    helper("/", L3::TokenType::ODivide, "/", 3, 4);
    helper("%", L3::TokenType::OModulus, "%", 4, 5);
    helper("#", L3::TokenType::OSize, "#", 5, 6);
    helper("++", L3::TokenType::OIncrement, "++", 6, 8);
    helper("--", L3::TokenType::ODecrement, "--", 8, 10);
    helper("==", L3::TokenType::OEqual, "==", 10, 12);
    helper("!=", L3::TokenType::ONotEqual, "!=", 12, 14);
    helper("<", L3::TokenType::OLess, "<", 14, 15);
    helper("<=", L3::TokenType::OLessEqual, "<=", 15, 17);
    helper(">", L3::TokenType::OGreater, ">", 17, 18);
    helper(">=", L3::TokenType::OGreaterEqual, ">=", 18, 20);
    helper("&&", L3::TokenType::OAnd, "&&", 20, 22);
    helper("||", L3::TokenType::OOr, "||", 22, 24);
    helper("!", L3::TokenType::ONot, "!", 24, 25);
    helper(".", L3::TokenType::OMemberAccess, ".", 25, 26);
    helper(":", L3::TokenType::OMemberCall, ":", 26, 27);
    helper("..", L3::TokenType::OAppend, "..", 27, 29);
    helper("+=", L3::TokenType::OAddAssign, "+=", 29, 31);
    helper("-=", L3::TokenType::OSubtractAssign, "-=", 31, 33);
    helper("*=", L3::TokenType::OMultiplyAssign, "*=", 33, 35);
    helper("/=", L3::TokenType::ODivideAssign, "/=", 35, 37);
    helper("%=", L3::TokenType::OModulusAssign, "%=", 37, 39);
    helper("&&=", L3::TokenType::OAndAssign, "&&=", 39, 42);
    helper("||=", L3::TokenType::OOrAssign, "||=", 42, 45);
    helper("..=", L3::TokenType::OAppendAssign, "..=", 45, 48);
    helper("=", L3::TokenType::OAssign, "=", 48, 49);
}

void TestOperatorsAmbiguous(Test& t)
{
    t.BeginSection("Operators, ambiguous");

    char const text[] = "+++ --- ++++ ---- +++= ++= --= **= %%= ...";

    auto l = SetupLexer(text);

    auto helper = [&](char const* n, L3::TokenType et, auto const& es, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TSimple(et, istring{es}, eb, ee), l);
    };

    helper("+++ 1", L3::TokenType::OIncrement, "++", 0, 2);
    helper("+++ 2", L3::TokenType::OAdd, "+", 2, 3);
    helper("--- 1", L3::TokenType::ODecrement, "--", 4, 6);
    helper("--- 2", L3::TokenType::OSubtract, "-", 6, 7);
    helper("++++ 1", L3::TokenType::OIncrement, "++", 8, 10);
    helper("++++ 2", L3::TokenType::OIncrement, "++", 10, 12);
    helper("---- 1", L3::TokenType::ODecrement, "--", 13, 15);
    helper("---- 2", L3::TokenType::ODecrement, "--", 15, 17);
    helper("+++= 1", L3::TokenType::OIncrement, "++", 18, 20);
    helper("+++= 2", L3::TokenType::OAddAssign, "+=", 20, 22);
    helper("++= 1", L3::TokenType::OIncrement, "++", 23, 25);
    helper("++= 2", L3::TokenType::OAssign, "=", 25, 26);
    helper("--= 1", L3::TokenType::ODecrement, "--", 27, 29);
    helper("--= 2", L3::TokenType::OAssign, "=", 29, 30);
    helper("**= 1", L3::TokenType::OMultiply, "*", 31, 32);
    helper("**= 2", L3::TokenType::OMultiplyAssign, "*=", 32, 34);
    helper("%%= 1", L3::TokenType::OModulus, "%", 35, 36);
    helper("%%= 2", L3::TokenType::OModulusAssign, "%=", 36, 38);
    helper("... 1", L3::TokenType::OAppend, "..", 39, 41);
    helper("... 2", L3::TokenType::OMemberAccess, ".", 41, 42);
}

void TestOperatorError(Test& t)
{
    t.BeginSection("Operator, error cases");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::InvalidToken, istring{ev}, eb, ee), l);
    };

    helper("&", "&", "&", 0, 1);
    helper("&=", "&=", "&", 0, 1);
    helper("|", "|", "|", 0, 1);
    helper("|=", "|=", "|", 0, 1);
}

void TestComment(Test& t)
{
    t.BeginSection("Comment");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TSimple(L3::TokenType::Comment, istring{ev}, eb, ee), l);
    };

    helper("to end of line \\n", "//blah +-!\n stuff", "//blah +-!", 0, 10);
    helper("to end of line \\r", "//blah +-!\r\n stuff", "//blah +-!", 0, 10);
    helper("to end of file", "//blah +-!", "//blah +-!", 0, 10);
}

void TestNewline(Test& t)
{
    t.BeginSection("Newline");

    char const text[] = "a\nb c\r\nd\ne";
    auto l = SetupLexer(text);

    auto helper = [&](char const* n, auto const& ev, L3::Cursor eb, L3::Cursor ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        auto sr = L3::SourceReference{istring{ev}, eb, ee};
        t.RecordOutcome(n, L3::Token::IdVar(istring{ev}, std::move(sr)), l);
    };

    helper("line 1", "a", L3::Cursor{0, 1, 1}, L3:: Cursor{1, 1, 2});
    helper("line 2 pt 1", "b", L3::Cursor{2, 2, 1}, L3::Cursor{3, 2, 2});
    helper("line 2 pt 2", "c", L3::Cursor{4, 2, 3}, L3::Cursor{5, 2, 4});
    helper("line 3", "d", L3::Cursor{7, 3, 1}, L3::Cursor{8, 3, 2});
    helper("line 4", "e", L3::Cursor{9, 4, 1}, L3::Cursor{10, 4, 2});
}

void TestInvalidCharacters(Test& t)
{
    t.BeginSection("Invalid Characters");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::InvalidCharacter, istring{ev}, eb, ee), l);
    };

    helper("lonely \\r", "\r stuff", "\r", 0, 1);

    char const text[6] = {' ', ' ', ' ', '\0', ' ', '\0'};
    char const expected[2] = {'\0', '\0'};
    helper("embedded null", text, expected, 3, 4);
}

void TestString(Test& t)
{
    t.BeginSection("String");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TString(istring{ev}, istring{text}, eb, ee), l);
    };

    helper("\" delimiter", "\"blah\"", "blah", 0, 6);
    helper("\" delimiter", "'blah'", "blah", 0, 6);
    helper("escapes 1", "'<\\n \\r \\t \\0>'", "<\n \r \t \0>", 0, 15);
    helper("escapes 2", "'<\\\\ \\\" \\'>'", "<\\ \" '>", 0, 12);
    helper("escaped escapes", "'<\\\\t \\\\\\t>'", "<\\t \\\t>", 0, 12);
}

void TestStringInvalid(Test& t)
{
    t.BeginSection("String, invalid");

    auto helper1 = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::UnterminatedString, istring{ev}, eb, ee), l);
    };

    helper1("unterminated 1", "\"blah", "\"blah", 0, 5);
    helper1("unterminated 2", "'blah", "'blah", 0, 5);
    helper1("mismatched delimiters 1", "\"blah'", "\"blah'", 0, 6);
    helper1("mismatched delimiters 2", "'blah\"", "'blah\"", 0, 6);

    auto helper2 = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::InvalidEscape, istring{ev}, eb, ee), l);
    };

    helper2("unterminated escape", "'blah\\", "'blah\\", 0, 6);
    helper2("bad escape", "'blah\\a'", "'blah\\a", 0, 7);
}

void TestNumbersSimple(Test& t)
{
    t.BeginSection("Numbers, simple");

    char const text[] = "0 0. 0.0 .0 1 1. 1.1 .1 123 123. 123.123 .123 "
                        "1e0 1e1 1e123 1e+1 1e-1 1E0 1E1 1E123 1E+1 1E-1 0e0 0E0";
    auto l = SetupLexer(text);

    auto helper = [&](char const* n, double ev, auto const& et, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TNumber(ev, istring{et}, eb, ee), l);
    };

    helper("0",         0,          "0",        0, 1);
    helper("0.",        0,          "0.",       2, 4);
    helper("0.0",       0,          "0.0",      5, 8);
    helper(".0",        0,          ".0",       9, 11);
    helper("1",         1,          "1",        12, 13);
    helper("1.",        1,          "1.",       14, 16);
    helper("1.1",       1.1,        "1.1",      17, 20);
    helper(".1",        0.1,        ".1",       21, 23);
    helper("123",       123,        "123",      24, 27);
    helper("123.",      123.,       "123.",     28, 32);
    helper("123.123",   123.123,    "123.123",  33, 40);
    helper(".123",      .123,       ".123",     41, 45);
    helper("1e0",       1,          "1e0",      46, 49);
    helper("1e1",       10,         "1e1",      50, 53);
    helper("1e123",     1e123,      "1e123",    54, 59);
    helper("1e+1",      10,         "1e+1",     60, 64);
    helper("1e-1",      0.1,        "1e-1",     65, 69);
    helper("1E0",       1,          "1E0",      70, 73);
    helper("1E1",       10,         "1E1",      74, 77);
    helper("1E123",     1e123,      "1E123",    78, 83);
    helper("1E+1",      10,         "1E+1",     84, 88);
    helper("1E-1",      0.1,        "1E-1",     89, 93);
    helper("0e0",       1,          "0e0",      94, 97);
    helper("0E0",       1,          "0E0",      98, 101);
}

void TestNumberAmbiguous(Test& t)
{
    t.BeginSection("Numbers, ambiguous");

    char const text[] = "1..2";
    auto l = SetupLexer(text);

    auto helper = [&](char const* n, double ev, auto const& et, uint32_t eb, uint32_t ee)
    {
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TNumber(ev, istring{et}, eb, ee), l);
    };

    helper("1..2 1",    1,          "1.",        0, 2);
    helper("1..2 2",    0.2,        ".2",        2, 4);
}

void TestNumberInvalid(Test& t)
{
    t.BeginSection("Numbers, invalid");

    auto helper = [&](char const* n, auto const& text, auto const& ev, uint32_t eb, uint32_t ee)
    {
        auto l = SetupLexer(text);
        l = L3::Lexer::Advance(std::move(l));
        t.RecordOutcome(n, TError(L3::ErrorType::InvalidNumber, istring{ev}, eb, ee), l);
    };

    helper("leading 0", "01234", "01234", 0, 5);
    helper("exponent leading 0", "1e01234", "1e01234", 0, 7);
    helper("unterminated exponent 1", "1eblah", "1e", 0, 2);
    helper("unterminated exponent 2", "1e+blah", "1e+", 0, 3);
    helper("unterminated exponent 3", "1e-blah", "1e-", 0, 3);
}

int main()
{
    Test t{"lexer"};

    TestEmpty(t);
    TestIdVar(t);
    TestIdVarInvalid(t);
    TestIdAtom(t);
    TestIdAtomInvalid(t);
    TestDelimiters(t);
    TestKeywords(t);
    TestOperatorsSimple(t);
    TestOperatorsNoSpaces(t);
    TestOperatorsAmbiguous(t);
    TestOperatorError(t);
    TestComment(t);
    TestNewline(t);
    TestInvalidCharacters(t);
    TestString(t);
    TestStringInvalid(t);
    TestNumbersSimple(t);
    TestNumberAmbiguous(t);
    TestNumberInvalid(t);

    t.PrintTotal();

    return 0;
}
