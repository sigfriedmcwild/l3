#include <memory>
#include <vector>

#include <istring/utility.h>
#include <lexer/lexer.h>
#include <parser/parser.h>
#include <parser/utils.h>

#include "../harness.h"

using IString::istring;

L3::Cursor Cursor(uint32_t i)
{
    return L3::Cursor{i, 1, i + 1};
}

L3::Node NSimple(L3::NodeType t, istring&& s, uint32_t b, uint32_t e)
{
    return L3::Node
    {
        t,
        L3::ValueHolder{},
        L3::NodeList{},
        L3::SourceReference{std::move(s), Cursor(b), Cursor(e)}
    };
}

L3::NodeBuilder NBSimple(L3::NodeType t, istring&& s, uint32_t b, uint32_t e)
{
    auto nb = L3::NodeBuilder{t, L3::SourceReference{s, Cursor(b), Cursor(e)}};
    nb.SetSource(L3::SourceReference{std::move(s), Cursor(b), Cursor(e)});
    return nb;
}

template<size_t SIZE>
void Parse(Test& t, char const (&str)[SIZE], bool expectSuccess)
{
    auto text = IString::istring{str};
    auto l = L3::Lexer::Init(text);
    auto p = L3::Parser::Init(text);

    while (!p.Terminal())
    {
        l = L3::Lexer::Advance(std::move(l));
        p = L3::Parser::Consume(std::move(p), l.Token());
    }

    t.RecordOutcome(str, expectSuccess, p);

    if (!expectSuccess)
    {
        return;
    }

    auto r = L3::AstRewrite::Rewrite(p.Ast());
    std::printf("Rewritten Ast\n");
    PrintNode(r);
    std::printf("\n\n");
}

void TestEmpty(Test& t)
{
    t.BeginSection("Empty");

    auto Token = [](L3::TokenType v)
    {
        auto sr = L3::SourceReference{istring{}, Cursor(0), Cursor(0)};
        return L3::Token::Simple(v, std::move(sr));
    };

    {
        auto p = L3::Parser::Init(istring{});

        p = L3::Parser::Consume(std::move(p), Token(L3::TokenType::Begin));
        t.RecordOutcome("1 - After BEGIN, terminal", false, p.Terminal());
        auto exp = std::vector<L3::NodeBuilder>{};
        exp.push_back(NBSimple(L3::NodeType::Root, istring{}, 0, 0));
        t.RecordOutcome("1 - After BEGIN, children",
            exp, p);
        t.RecordOutcome("1 - After BEGIN, ast", L3::Node{}, p);

        p = L3::Parser::Consume(std::move(p), Token(L3::TokenType::Eof));
        t.RecordOutcome("1 - After EOF, terminal", true, p.Terminal());
        t.RecordOutcome("1 - After EOF, children",
            std::vector<L3::NodeBuilder>{}, p);
        t.RecordOutcome("1 - After EOF, ast", NSimple(L3::NodeType::Block, istring{}, 0, 0), p);
    }

    {
        auto p = L3::Parser::Init(istring{});

        p = L3::Parser::Consume(std::move(p), Token(L3::TokenType::Eof));
        t.RecordOutcome("2 - After EOF, terminal", true, p.Terminal());
        t.RecordOutcome("2 - After EOF, children",
            std::vector<L3::NodeBuilder>{}, p);
        t.RecordOutcome("2 - After EOF, ast", NSimple(L3::NodeType::Block, istring{}, 0, 0), p);
    }
}

void TestStatement(Test& t)
{
    t.BeginSection("Statements");

    Parse(t, "", true);
    Parse(t, "break;", true);
    Parse(t, "continue;", true);
    Parse(t, "return a;", true);
    Parse(t, "return a, b, 1 + 3;", true);
    Parse(t, "a; b; c;", true);
    Parse(t, "a; b; break;", true);
    Parse(t, "a; b; continue;", true);
    Parse(t, "a; b; return c;", true);
    Parse(t, "{}", true);
    Parse(t, "a; {} b;", true);
    Parse(t, "a; {b; c;} d;", true);
    Parse(t, "a; {b; break;} d;", true);
    Parse(t, "a; {b; continue;} d;", true);
    Parse(t, "a; {b; return c;} d;", true);
    Parse(t, "a; {break;} d;", true);
    Parse(t, "a; {continue;} d;", true);
    Parse(t, "a; {return c;} d;", true);
    Parse(t, "return;", true);
}

void TestStatementErrors(Test& t)
{
    t.BeginSection("Statements, errors");

    Parse(t, "a; b; break c;", false);
    Parse(t, "a; b; continue c;", false);
    Parse(t, "a; b; return; c; d;", false);
    Parse(t, "a; b; break; c;", false);
    Parse(t, "a; b; continue; c;", false);
    Parse(t, "a; b; return c; d;", false);
    Parse(t, "{", false);
    Parse(t, "{{}", false);
    Parse(t, "a; }", false);
    Parse(t, "a;;", false);
    Parse(t, "a; b; c", false);
    Parse(t, "var f = fn {} return;", false);
}

void TestAssignment(Test& t)
{
    t.BeginSection("Assignments");

    Parse(t, "a = a;", true);
    Parse(t, "a, b = a;", true);
    Parse(t, "a = a, b;", true);
    Parse(t, "a, b = a, b;", true);
    Parse(t, "var a, b = a;", true);
    Parse(t, "a, var b = a, b;", true);
    Parse(t, "a += b;", true);
    Parse(t, "a -= b;", true);
    Parse(t, "a *= b;", true);
    Parse(t, "a /= b;", true);
    Parse(t, "a %= b;", true);
    Parse(t, "a &&= b;", true);
    Parse(t, "a ||= b;", true);
    Parse(t, "a ..= b;", true);
}

void TestAssignmentError(Test& t)
{
    t.BeginSection("Assignments, errors");

    Parse(t, "a = ;", false);
    Parse(t, " = a;", false);
    Parse(t, "a, b = ;", false);
    Parse(t, " = a, b;", false);
    Parse(t, "a, b += b;", false);
    Parse(t, "a, b -= b;", false);
    Parse(t, "a, b *= b;", false);
    Parse(t, "a, b /= b;", false);
    Parse(t, "a, b %= b;", false);
    Parse(t, "a, b &&= b;", false);
    Parse(t, "a, b ||= b;", false);
    Parse(t, "a, b ..= b;", false);
    Parse(t, "a += b, c;", false);
    Parse(t, "a -= b, c;", false);
    Parse(t, "a *= b, c;", false);
    Parse(t, "a /= b, c;", false);
    Parse(t, "a %= b, c;", false);
    Parse(t, "a &&= b, c;", false);
    Parse(t, "a ||= b, c;", false);
    Parse(t, "a ..= b, c;", false);
    Parse(t, "@a = a;", false);
    Parse(t, "1 = a;", false);
    Parse(t, "'a' = a;", false);
}


void TestDefinition(Test& t)
{
    t.BeginSection("Definitions");

    Parse(t, "var a;", true);
    Parse(t, "atom @a;", true);
}

void TestDefinitionError(Test& t)
{
    t.BeginSection("Definitions, errors");

    Parse(t, "var 1;", false);
    Parse(t, "var @a;", false);
    Parse(t, "var 'a';", false);
    Parse(t, "atom 1;", false);
    Parse(t, "atom a;", false);
    Parse(t, "atom 'a';", false);
}

void TestExpression(Test& t)
{
    t.BeginSection("Expressions");

    Parse(t, "a = a;", true);
    Parse(t, "a = (1);", true);
    Parse(t, "a = 1 + a;", true);
    Parse(t, "a = 1 - a;", true);
    Parse(t, "a = 1 * a;", true);
    Parse(t, "a = 1 / a;", true);
    Parse(t, "a = 1 % a;", true);
    Parse(t, "a = 1 * 1 - a;", true);
    Parse(t, "a = 1 + 1 + a;", true);
    Parse(t, "a = 1 / 1 * a;", true);
    Parse(t, "a = !a;", true);
    Parse(t, "a = #a;", true);
    Parse(t, "a = -a;", true);
    Parse(t, "a = ++a;", true);
    Parse(t, "a = --a;", true);
    Parse(t, "a = 1++;", true);
    Parse(t, "a = 1--;", true);
    Parse(t, "a = 1 * (1 - 1);", true);
    Parse(t, "a = -(1 + 1 + -1);", true);
    Parse(t, "a = -1;", true);
    Parse(t, "a = 1 > 2 && #(-5) == 6 || 7 * 10 + d;", true);
    Parse(t, "a = @a == @a;", true);
}

void TestExpressionError(Test& t)
{
    t.BeginSection("Expression, errors");

    Parse(t, "a = 1 +;", false);
    Parse(t, "a = 1 + * a;", false);
    Parse(t, "a = 1 + 1e;", false);
    Parse(t, "a = ++1++;", false);
}

void TestIf(Test& t)
{
    t.BeginSection("If");

    Parse(t, "if 1 {a;}", true);
    Parse(t, "if 1 {a;} else {b;}", true);
    Parse(t, "if 1 {a;} else if 2 {b;}", true);
    Parse(t, "if 1 {a;} else if 2 {b;} else {c;}", true);
    Parse(t, "if 1 {a;} else if 2 {b;} else if 3 {c;}", true);
}

void TestIfError(Test& t)
{
    t.BeginSection("If, errors");

    Parse(t, "if {a;}", false);
    Parse(t, "if a; {a;}", false);
    Parse(t, "if a; a;}", false);
    Parse(t, "if 1 a;}", false);
    Parse(t, "if a;", false);
    Parse(t, "if 1", false);
    Parse(t, "if a; {a;", false);
    Parse(t, "if 1 {a; else {b;}", false);
    Parse(t, "if 1 {a;} else b;}", false);
    Parse(t, "if 1 {a;} else {b;", false);
}

void TestLoop(Test& t)
{
    t.BeginSection("Loops");

    Parse(t, "while 1 {a;}", true);
    Parse(t, "do {a;} while a;", true);
    Parse(t, "for a = 0, 1 {a;}", true);
    Parse(t, "for a = 0, 1, 2 {a;}", true);
}

void TestLoopError(Test& t)
{
    t.BeginSection("Loops, errors");

    // TODO
}

void TestRval(Test& t)
{
    t.BeginSection("Rvals");

    Parse(t, "a = nil;", true);
    Parse(t, "a = true;", true);
    Parse(t, "a = false;", true);
    Parse(t, "a = 'a';", true);
    Parse(t, "a = 'a' 'b';", true);
    Parse(t, "a = @a;", true);
}

void TestObject(Test& t)
{
    t.BeginSection("Objects");

    Parse(t, "a;", true);
    Parse(t, "a();", true);
    Parse(t, "a(a);", true);
    Parse(t, "a(a, b, c);", true);
    Parse(t, "a[a];", true);
    Parse(t, "a()[a];", true);
    Parse(t, "a[a]();", true);
    Parse(t, "a()(a[a])[a(a, b)][a[a]];", true);
    Parse(t, "recurse();", true);
    Parse(t, "recurse(a, b);", true);
    Parse(t, "a.b;", true);
    Parse(t, "a.b.c;", true);
    Parse(t, "a[b].c().d;", true);
    Parse(t, "a:b();", true);
    Parse(t, "a:b(c);", true);
    Parse(t, "a:b()():c()[0]:d();", true);
}

void TestObjectError(Test& t)
{
    t.BeginSection("Objects, errors");

    Parse(t, "recurse;", false);
    Parse(t, "a:b;", false);
    // TODO
}

void TestConstructor(Test& t)
{
    t.BeginSection("Constructors");

    Parse(t, "a = lst[];", true);
    Parse(t, "a = tbl{};", true);
    Parse(t, "a = fn {};", true);
    Parse(t, "a = fn a, b {a;};", true);
}

void TestConstructorError(Test& t)
{
    t.BeginSection("Constructors, errors");

    Parse(t, "a = lst{};", false);
    Parse(t, "a = lst[};", false);
    Parse(t, "a = lst{];", false);
    Parse(t, "a = [];", false);
    Parse(t, "a = tlb[];", false);
    Parse(t, "a = tbl{];", false);
    Parse(t, "a = tbl[};", false);
    Parse(t, "a = {};", false);
    Parse(t, "a = fn a, {a;};", false);
    Parse(t, "a = fn a;", false);

    // TODO
}

int main()
{
    Test t{"parser"};

    TestEmpty(t);
    TestStatement(t);
    TestStatementErrors(t);
    TestAssignment(t);
    TestAssignmentError(t);
    TestDefinition(t);
    TestDefinitionError(t);
    TestExpression(t);
    TestExpressionError(t);
    TestIf(t);
    TestIfError(t);
    TestLoop(t);
    TestRval(t);
    TestObject(t);
    TestObjectError(t);
    TestConstructor(t);
    TestConstructorError(t);

    t.PrintTotal();

    return 0;
}
