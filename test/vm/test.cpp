#include <chrono>
#include <random>

#include <compiler/utils.h>
#include <vm/vm.h>

#include "../harness.h"

using IString::istring;

int64_t Println(L3::Vm& /*vm*/, L3::Stack& stack, void*)
{
#ifdef PERF_TEST
    (void)stack;
#else
    for (auto i = stack.Bottom(), endI = 0ll; i < endI; ++i)
    {
        auto obj = stack.Read(i);
        switch (obj.Type())
        {
            case L3::ObjectType::Atom:
                std::printf("%.*s", PRINTF_STR(obj.AtomName())); break;
            case L3::ObjectType::StringReference:
            case L3::ObjectType::StringValue:
                std::printf("%.*s", PRINTF_STR(obj.StringValue())); break;
            default:
                std::printf("%.*s", PRINTF_STR(Stringify(obj))); break;
        }
        std::printf(" ");
    }
    std::printf("\n");
#endif

    return 0;
}

int64_t GetAtom(L3::Vm& vm, L3::Stack& stack, void*)
{
    auto id = vm.DeclareAtom("Blah");
    stack.Push(L3::Object{id});
    return 1;
}

//int64_t Rand(L3::Vm& /*vm*/, L3::Stack& stack, void*)
//{
//    static std::mt19937_64 gen{0};
//
//}

template<size_t SIZE>
L3::Stack Run(char const (&str)[SIZE])
{
    std::printf("\nCode: %s\n", str);

    auto vm = L3::Vm{};

    auto gIndex = vm.DeclareGlobal(istring{"g"});
    vm.WriteGlobal(gIndex, L3::Object{22.0});

    vm.DeclareAtom(std::string{"LolCat"});
    auto printIndex = vm.DeclareGlobal(istring{"Println"});
    auto printValue = vm.RegisterForeignClosure(&Println, nullptr);
    vm.WriteGlobal(printIndex, std::move(printValue));
    auto getAtomIndex = vm.DeclareGlobal(istring{"GetAtom"});
    auto getAtomValue = vm.RegisterForeignClosure(&GetAtom, nullptr);
    vm.WriteGlobal(getAtomIndex, std::move(getAtomValue));

    vm.Load(IString::istring{str});
    L3::PrintDisassembly(
        vm.Code(),
        vm.DebugFunctionTable(),
        vm.DebugGlobalEnvironment(),
        vm.DebugAtomTable(),
        vm.DebugStringTable()
    );

#ifdef PERF_TEST
    static auto const ITERATIONS = 10000;

    L3::Stack s;
    s = vm.Run();

    auto testStart = std::chrono::high_resolution_clock::now();
    for (auto i = 0; i < ITERATIONS; ++i)
    {
        vm.Run();
    }
    auto testEnd = std::chrono::high_resolution_clock::now();

    auto durationS = std::chrono::duration_cast<std::chrono::seconds>(testEnd - testStart);
    auto durationNS = std::chrono::duration_cast<std::chrono::nanoseconds>(testEnd - testStart);
    auto durationSFraction = durationNS % std::chrono::seconds{1};

    std::printf("Test took %lld.%09llds, %lldns per test\n",
        durationS.count(), durationSFraction.count(), durationNS.count() / ITERATIONS);

    return s;
#else
    return vm.Run();
#endif
}

template<size_t SIZE>
void RunVoid(Test& t, char const (&str)[SIZE])
{
    auto res = Run(str);
    t.RecordOutcome("result count", 0ll, res.Top());
}

template<size_t SIZE>
void RunBool(Test& t, char const (&str)[SIZE], bool expected)
{
    auto res = Run(str);
    t.RecordOutcome("result count", 1ll, res.Top());
    t.RecordOutcome(
        "result type",
        static_cast<size_t>(L3::ObjectType::Bool),
        static_cast<size_t>(res.Read(0).Type())
    );
    t.RecordOutcome("result value", expected, res.Read(0).Bool());
}

template<size_t SIZE>
void RunNumber(Test& t, char const (&str)[SIZE], double expected)
{
    auto res = Run(str);
    t.RecordOutcome("result count", 1ll, res.Top());
    t.RecordOutcome(
        "result type",
        static_cast<size_t>(L3::ObjectType::Number),
        static_cast<size_t>(res.Read(0).Type())
    );
    t.RecordOutcome("result value", expected, res.Read(0).Number());
}

void TestSimple(Test& t)
{
    t.BeginSection("Simple");

    RunVoid(t, "");
    RunVoid(t, "var a = 0;");
    RunVoid(t, "var a = 1 + 2;");
    RunNumber(t, "return 1;", 1);
    RunNumber(t, "return 1 + 2;", 3);
    RunNumber(t, "return g;", 22);
    RunNumber(t, "var x = 1; return g + x;", 23);
    RunNumber(t, "var f = fn x { return x; }; return f(4);", 4);
    RunNumber(t, "var f = fn x { var z = fn { return g; }; return x + z(); }; return f(2);", 24);
    RunNumber(t, "var f = fn x, y { return x - y, y - x; }; return f(g, 15);", 7);
    RunNumber(t, "var f = fn { g = 17; }; f(); return g;", 17);
    RunVoid(t, "var s = 'a string!'; Println(s);");
    RunVoid(t, "Println(@LolCat);");
    RunVoid(t, "var a = @foo; Println(a);");
    RunVoid(t, "Println(GetAtom());");
    RunBool(t, "return @foo == GetAtom();", false);
    RunBool(t, "return @Blah == GetAtom();", true);
    RunBool(t, "return GetAtom() == GetAtom();", true);
}

void TestObject(Test& t)
{
    t.BeginSection("Objects");

    RunVoid(t, "var a = lst[]; Println(a);");
    RunNumber(t, "var a = lst[]; a[2] = -5; return a[2];", -5);
    RunVoid(t, "var t = tbl{}; Println(t);");
    RunNumber(t, "var t = tbl{}; t[2] = -5; return t[2];", -5);

    RunNumber(t, "var t = tbl{}; t.foo = 5; return t.foo;", 5);
    RunNumber(t, "var t = tbl{}; t.v = 5; t.f = fn s { return s.v + 1; }; return t:f();", 6);
}

void TestIf(Test& t)
{
    t.BeginSection("If");

    RunNumber(t, "var a; if true { a = 1; } return a;", 1);
    RunNumber(t, "var a; if true { a = 1; } else { a = 2; } return a;", 1);
    RunNumber(t, "var a; if false { a = 1; } else { a = 2; } return a;", 2);
    RunNumber(t, "var a; if true { a = 1; } else if true { a = 2; } return a;", 1);
    RunNumber(t, "var a; if false { a = 1; } else if true { a = 2; } return a;", 2);
    RunNumber(t, "var a; if true { a = 1; } else if true { a = 2; } else { a = 3;} return a;", 1);
    RunNumber(t, "var a; if false { a = 1; } else if true { a = 2; } else { a = 3;} return a;", 2);
    RunNumber(t, "var a; if false { a = 1; } else if false { a = 2; } else { a = 3;} return a;", 3);
}

void TestLoop(Test& t)
{
    t.BeginSection("Loops");

    RunBool(t, "var a = true; while a { a = false; } return a;", false);
    RunBool(t, "var a = false; while a { a = true; } return a;", false);
    RunBool(t, "var a = 23; do { a = !a; } while a; return a;", false);
    RunBool(t, "var a = false; do { a = !a; } while a; return a;", false);
}

void TestOrder(Test& t)
{
    t.BeginSection("Order");

    RunBool(t, "return nil == nil;", true);
    RunBool(t, "return 1 == 1;", true);
    RunBool(t, "return 1 == 2;", false);
    RunBool(t, "return 2 == 1;", false);
    RunBool(t, "return true == true;", true);
    RunBool(t, "return true == false;", false);
    RunBool(t, "return false == true;", false);
    RunBool(t, "return false == false;", true);
    RunBool(t, "return 'a' == 'a';", true);
    RunBool(t, "return 'a' == 'b';", false);
    RunBool(t, "return 'b' == 'a';", false);
    RunBool(t, "return @a == @a;", true);
    RunBool(t, "return @a == @b;", false);
    RunBool(t, "return @b == @a;", false);

    RunBool(t, "return nil != nil;", false);
    RunBool(t, "return 1 != 1;", false);
    RunBool(t, "return 1 != 2;", true);
    RunBool(t, "return 2 != 1;", true);
    RunBool(t, "return true != true;", false);
    RunBool(t, "return true != false;", true);
    RunBool(t, "return false != true;", true);
    RunBool(t, "return false != false;", false);
    RunBool(t, "return 'a' != 'a';", false);
    RunBool(t, "return 'a' != 'b';", true);
    RunBool(t, "return 'b' != 'a';", true);
    RunBool(t, "return @a != @a;", false);
    RunBool(t, "return @a != @b;", true);
    RunBool(t, "return @b != @a;", true);

    RunBool(t, "return nil < nil;", false);
    RunBool(t, "return 1 < 1;", false);
    RunBool(t, "return 1 < 2;", true);
    RunBool(t, "return 2 < 1;", false);
    RunBool(t, "return true < true;", false);
    RunBool(t, "return true < false;", false);
    RunBool(t, "return false < true;", true);
    RunBool(t, "return false < false;", false);
    RunBool(t, "return @a < @a;", false);

    RunBool(t, "return nil <= nil;", true);
    RunBool(t, "return 1 <= 1;", true);
    RunBool(t, "return 1 <= 2;", true);
    RunBool(t, "return 2 <= 1;", false);
    RunBool(t, "return true <= true;", true);
    RunBool(t, "return true <= false;", false);
    RunBool(t, "return false <= true;", true);
    RunBool(t, "return false <= false;", true);
    RunBool(t, "return @a <= @a;", true);

    RunBool(t, "return nil > nil;", false);
    RunBool(t, "return 1 > 1;", false);
    RunBool(t, "return 1 > 2;", false);
    RunBool(t, "return 2 > 1;", true);
    RunBool(t, "return true > true;", false);
    RunBool(t, "return true > false;", true);
    RunBool(t, "return false > true;", false);
    RunBool(t, "return false > false;", false);
    RunBool(t, "return @a > @a;", false);

    RunBool(t, "return nil >= nil;", true);
    RunBool(t, "return 1 >= 1;", true);
    RunBool(t, "return 1 >= 2;", false);
    RunBool(t, "return 2 >= 1;", true);
    RunBool(t, "return true >= true;", true);
    RunBool(t, "return true >= false;", true);
    RunBool(t, "return false >= true;", false);
    RunBool(t, "return false >= false;", true);
    RunBool(t, "return @a >= @a;", true);
}

void TestAppend(Test& t)
{
    t.BeginSection("Append");

    RunVoid(t, R"code(
        var s = 'a' .. 'b';
        Println(s);
    )code");
    /*RunVoid(t, R"code(
        var s = 'a' .. 2;
        Println(s);
    )code");*/
    /*    RunVoid(t, R"code(
        var f = fn {};
        var s = g .. f .. Println .. 'a' .. 0 .. true;
        Println(s);
    )code");*/
    RunBool(t, R"code(
        var a = lst[]; a[0] = 1;
        var b = lst[]; b[0] = 2;
        var c = a .. b;
        return c[0] == 1 && c[1] == 2;
    )code", true);
    RunBool(t, R"code(
        var a = tbl{}; a[0] = 1; a[1] = 2;
        var b = tbl{}; b[1] = 3; b[2] = 4;
        var c = a .. b;
        return c[0] == 1 && c[1] == 3 && c[2] == 4;
    )code", true);

}

void TestSize(Test& t)
{
    t.BeginSection("Size");

    RunNumber(t, "return #'blah';", 4);
    RunNumber(t, "var s = 'blah'; return #s;", 4);
    RunNumber(t, "var s = 'bl' .. 'ah'; return #s;", 4);
    RunNumber(t, "var a = lst[]; return #a;", 0);
    RunNumber(t, "var a = lst[]; a[0] = 1; return #a;", 1);
    RunNumber(t, "var a = lst[]; a[5] = 1; return #a;", 6);
    RunNumber(t, "var t = tbl{}; return #t;", 0);
    RunNumber(t, "var t = tbl{}; t['a'] = true; return #t;", 1);
    RunNumber(t, "var t = tbl{}; t['a'] = true; t['a'] = nil; return #t;", 0);
}

void TestFFI(Test& t)
{
    t.BeginSection("FFI");

    RunVoid(t, "Println(1, 2, 3);");
}

void TestComplex(Test& t)
{
    t.BeginSection("Complex");

    RunNumber(t, R"code(
        // factorial
        var f = fn i
        {
            if i <= 1 { return 1; }
            return i * recurse(i - 1);
        };
        return f(5);
    )code", 120);

    RunNumber(t, R"code(
        // fibonacci (recursive)
        var f = fn i
        {
            if i <= 0 { return 0; }
            else if i == 1 { return 1; }
            return recurse(i - 1) + recurse(i - 2);
        };
        return f(g);
    )code", 17711);

    RunNumber(t, R"code(
        // fibonacci (iterative)
        var f = fn n
        {
            if n <= 0 { return 0; }
            else if n == 1 { return 1; }
            var a, var b, var i = 0, 1, 1;
            do
            {
                Println(a, b);
                a, b = b, a + b;
                i = i + 1;
            }
            while i < n;
            return b;
        };
        return f(g);
    )code", 17711);

    RunNumber(t, R"code(
        // quicksort
        var a = lst[];
        a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9] =
            49, 13, 6, 38, 48, 7, 37, 43, 14, 39;

        var qs = fn a
        {
            if #a < 2 { return a; }
            var pivot = a[0];
            var left = lst[];
            var right = lst[];
            var index = 1;
            do
            {
                var v = a[index];
                if (v < pivot)
                {
                    left[#left] = v;
                }
                else
                {
                    right[#right] = v;
                }

                index = index + 1;
            } while (index < #a);

            var p = recurse(left);
            p[#p] = pivot;
            return p .. recurse(right);
        };

        Println('before:', a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]);
        var b = qs(a);
        Println('after: ', b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7], b[8], b[9]);
        return b[0];
    )code", 6);

    RunNumber(t, R"code(
        // quicksort (in place, Hoare's partioning)
        var a = lst[];
        a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9] =
            49, 13, 6, 38, 48, 7, 37, 43, 14, 39;

        var qs = fn a
        {
            var internal = fn a, lo, hi
            {
                if lo == hi { return; }

                var p;
                {
                    var pivot = a[lo];
                    var i, var j = lo - 1, hi + 1;
                    var loop = true;
                    do
                    {
                        do
                        {
                            i = i + 1;
                        } while a[i] < pivot && i <= hi;

                        do
                        {
                            j = j - 1;
                        } while a[j] > pivot && j >= lo;

                        if i >= j
                        {
                            loop, p = false, j;
                        }
                        else
                        {
                            a[i], a[j] = a[j], a[i];
                        }
                    } while loop;
                }

                recurse(a, lo, p);
                recurse(a, p + 1, hi);
            };

            internal(a, 0, #a - 1);
        };

        Println('before:', a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]);
        qs(a);
        Println('after: ', a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]);
        return a[0];
    )code", 6);

    RunVoid(t, R"code(
        // number of 3d6 rolls with 2 dice showing the same result
        var rolls, var doubles, var triples = 0, 0, 0;
        var i = 1;
        do
        {
            var j = 1;
            do
            {
                var k = 1;
                do
                {
                    if i == j && i == k
                    {
                        triples = triples + 1;
                    }
                    else if i == j || i == k || j == k
                    {
                        doubles = doubles + 1;
                    }

                    rolls = rolls + 1;
                    k = k + 1;
                } while k <= 6;

                j = j + 1;
            } while j <= 6;

            i = i + 1;
        } while i <= 6;

        Println('rolls:', rolls, '- doubles:', doubles, '- triples:', triples);
        Println('chance of doubles:', (doubles + triples) / rolls);
    )code");
}

int main()
{
    Test t{"vm"};

    TestSimple(t);
    TestObject(t);
    TestIf(t);
    TestLoop(t);
    TestOrder(t);
    TestAppend(t);
    TestSize(t);
    TestFFI(t);
    TestComplex(t);

    t.PrintTotal();

    return 0;
}
