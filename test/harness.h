#include <cstdint>
#include <cstdio>
#include <vector>

#include <compiler/compiler.h>
#include <compiler/utils.h>
#include <lexer/lexer.h>
#include <lexer/utils.h>
#include <parser/parser.h>
#include <parser/utils.h>

inline
char const* boolToString(bool v)
{
    return v ? "true" : "false";
}

class Test
{
public:
    Test(char const* file)
    {
        printf("-----------------------------------------------------------\n");
        printf("--- %s\n\n", file);
    }

    void BeginSection(char const* section)
    {
        EndCurrentSection();
        m_section = section;

        printf("section: %s\n", m_section);
    }

    void RecordOutcome(char const* title, char expected, char actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("'%c'", v); });
    }

    void RecordOutcome(char const* title, size_t expected, size_t actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("%zu", v); });
    }

    void RecordOutcome(char const* title, int expected, int actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("%d", v); });
    }

    void RecordOutcome(char const* title, int64_t expected, int64_t actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("%lld", v); });
    }

    void RecordOutcome(char const* title, unsigned int expected, unsigned int actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("%u", v); });
    }

    void RecordOutcome(char const* title, double expected, double actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("%f", v); });
    }

    void RecordOutcome(char const* title, bool expected, bool actual)
    {
        RecordOutcome(title, expected, actual, [](auto v) { printf("%s", boolToString(v)); });
    }

    void RecordOutcome(char const* title, L3::Token const& expected, L3::LexerState const& actual)
    {
        RecordOutcome(title, expected, actual.Token(), [](auto const& v)
        {
            L3::PrintToken(v);
        });
    }

    void RecordOutcome(
        char const* title,
        std::vector<L3::NodeBuilder> const& expected,
        L3::ParserState const& actual
    )
    {
        RecordOutcome(title, expected, actual.Stack(), [](auto const& v)
        {
            PrintParseStack(v);
        });
    }

    void RecordOutcome(
        char const* title,
        bool expectSuccess,
        L3::ParserState const& actual
    )
    {
        auto astBuilt = false;
        if (actual.Terminal())
        {
            auto astType = actual.Ast().Type();
            astBuilt = astType != L3::NodeType::Error && astType != L3::NodeType::Nil;
        }
        auto callCount = 0;
        RecordOutcome(title, expectSuccess, astBuilt, [&](auto const&)
        {
            if (callCount == 0 && !expectSuccess == astBuilt)
            {
                ++callCount;
                if (expectSuccess)
                {
                    std::printf("successful parse");
                }
                else
                {
                    std::printf("unsuccessful parse");
                }
            }
            else
            {
                std::printf("\n");
                std::printf("Source: '%.*s'\n", PRINTF_STR(actual.Source()));
                PrintNode(actual.Ast());
                if (!actual.Stack().empty())
                {
                    std::printf("*************\n");
                    PrintParseStack(actual.Stack());
                }
            }
        });
    }

    void RecordOutcome(
        char const* title,
        L3::Node const& expected,
        L3::ParserState const& actual
    )
    {
        RecordOutcome(title, expected, actual.Ast(), [](auto const& v)
        {
            PrintNode(v);
        });
    }

    void RecordOutcome(
        char const* title,
        std::vector<std::string> const& expected,
        std::vector<std::string> const& actual
    )
    {
        RecordOutcome(title, expected, actual, [](auto const& v)
        {
            std::printf("\n");
            std::printf("{ ");
            for (auto& s : v)
            {
                std::printf("'%.*s', ", PRINTF_STR(s));
            }
            std::printf("}\n");
        });
    }

    void RecordOutcome(
        char const* title,
        bool expectSuccess,
        L3::CompilerContext&& actual,
        L3::FunctionList const& functions
    )
    {
        auto callCount = 0;
        RecordOutcome(title, expectSuccess, true, [&](auto const&)
        {
            if (callCount == 0 && !expectSuccess)
            {
                ++callCount;
                if (expectSuccess)
                {
                    std::printf("successful compilation");
                }
                else
                {
                    std::printf("unsuccessful compilation");
                }
            }
            else
            {
                std::printf("\n");
                L3::PrintDisassembly(actual, functions);
            }
        });
    }

    void PrintTotal()
    {
        EndCurrentSection();
        printf(" total: %3u/%3u tests passed\n", m_passCount, m_testCount);
        printf("-----------------------------------------------------------\n");
    }

private:
    template<class TVal, class TFunc>
    void RecordOutcome(
        char const* title,
        TVal const& expected,
        TVal const& actual,
        TFunc const& printValue
    )
    {
        auto pass = expected == actual;

        auto padSize = 30 - static_cast<int>(std::strlen(title));
        padSize = padSize < 1 ? 1 : padSize;

        printf("[%3u][%s] %s:", ++m_sectionTestCount, pass ? "pass" : "FAIL", title);
        ++m_testCount;

        if (pass)
        {
            ++m_passCount;
            ++m_sectionPassCount;

            printf("%*c", padSize, ' ');
            printValue(actual);
        }
        else
        {
            padSize -= 9;
            padSize = padSize < 0 ? 0 : padSize;
            printf("%*cexpected ", padSize, ' ');
            printValue(expected);
            printf(", actual ");
            printValue(actual);
        }

        printf("\n");
    }

    void EndCurrentSection()
    {
        if (!m_section)
        {
            return;
        }

        printf("        %3u/%3u tests passed\n\n", m_sectionPassCount, m_sectionTestCount);

        m_section = nullptr;
        m_sectionTestCount = 0;
        m_sectionPassCount = 0;
    }

private:
    uint32_t m_testCount = 0;
    uint32_t m_passCount = 0;

    char const* m_section = nullptr;
    uint32_t m_sectionTestCount = 0;
    uint32_t m_sectionPassCount = 0;
};
