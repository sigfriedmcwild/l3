#include <memory>
#include <vector>

#include <compiler/compiler.h>
#include <compiler/utils.h>
#include <istring/utility.h>
#include <parser/parser.h>
#include <parser/utils.h>

#include "../harness.h"

using IString::istring;

template<size_t SIZE>
void Compile(Test& t, char const (&str)[SIZE], bool expectSuccess)
{
    auto text = IString::istring{str};
    auto l = L3::Lexer::Init(text);
    auto p = L3::Parser::Init(text);

    while (!p.Terminal())
    {
        l = L3::Lexer::Advance(std::move(l));
        p = L3::Parser::Consume(std::move(p), l.Token());
    }

    auto r = L3::AstRewrite::Rewrite(p.Ast());

    auto functions = L3::Compiler::FindFunctions(r);
    auto atoms = L3::Compiler::BuildAtomTable(r);
    auto strings = L3::Compiler::BuildStringTable(r);

    auto gEnv = L3::Environment::Global();
    gEnv.DeclareGlobal(istring{"g"});

    L3::CompilerContext ctx{gEnv, functions, std::move(atoms), std::move(strings)};

    L3::Compiler::CompileRoot(functions.front(), ctx);
    for (size_t i = 1, endI = functions.size(); i < endI; ++i)
    {
        L3::Compiler::CompileFunction(functions[i], ctx);
    }

    t.RecordOutcome(str, expectSuccess, std::move(ctx), functions);
}

void TestFindFunctions(Test& t)
{
    t.BeginSection("Find functions");

    auto helper = [](Test& test, char const* name, auto& str, size_t count)
    {
        auto text = IString::istring{str};
        auto l = L3::Lexer::Init(text);
        auto p = L3::Parser::Init(text);

        while (!p.Terminal())
        {
            l = L3::Lexer::Advance(std::move(l));
            p = L3::Parser::Consume(std::move(p), l.Token());
        }

        auto r = L3::AstRewrite::Rewrite(p.Ast());
        std::printf("\n");
        PrintNode(r);

        auto fs = L3::Compiler::FindFunctions(r);

        test.RecordOutcome(name, count, fs.size());
    };

    helper(t, "Simple", "var a = fn{};", 2);
    helper(t, "Multiple", "var a = fn{}; var b = 5; a = fn{};", 3);
    helper(t, "Nested", "var a = fn{ var b = fn{}; };", 3);
}

void TestBuildAtomTable(Test& t)
{
    t.BeginSection("Build atom table");

    auto helper = [](Test& test, char const* name, auto& str, L3::StringTable const& e)
    {
        auto text = IString::istring{str};
        auto l = L3::Lexer::Init(text);
        auto p = L3::Parser::Init(text);

        while (!p.Terminal())
        {
            l = L3::Lexer::Advance(std::move(l));
            p = L3::Parser::Consume(std::move(p), l.Token());
        }

        auto r = L3::AstRewrite::Rewrite(p.Ast());
        std::printf("\n");
        PrintNode(r);

        auto builder = L3::Compiler::BuildAtomTable(r);
        auto a = L3::AtomTableBuilder::Make(std::move(builder));

        test.RecordOutcome(name, e, a);
    };

    helper(t, "Simple 1", "var a = @foo;", L3::AtomTable{"foo"});
    helper(t, "Multiple 1", "var a = @foo; var b = @bar; var c = @foo; var d = @bar;",
        L3::AtomTable{"foo", "bar"});
}

void TestBuildStringTable(Test& t)
{
    t.BeginSection("Build string table");

    auto helper = [](Test& test, char const* name, auto& str, L3::StringTable const& e)
    {
        auto text = IString::istring{str};
        auto l = L3::Lexer::Init(text);
        auto p = L3::Parser::Init(text);

        while (!p.Terminal())
        {
            l = L3::Lexer::Advance(std::move(l));
            p = L3::Parser::Consume(std::move(p), l.Token());
        }

        auto r = L3::AstRewrite::Rewrite(p.Ast());
        std::printf("\n");
        PrintNode(r);

        auto builder = L3::Compiler::BuildStringTable(r);
        auto a = L3::StringTableBuilder::Make(std::move(builder));

        test.RecordOutcome(name, e, a);
    };

    helper(t, "Simple 1", "var a = '';", L3::StringTable{""});
    helper(t, "Simple 2", "var a = 'foo';", L3::StringTable{"", "foo"});
    helper(t, "Multiple 1", "var a = ''; var b = 'foo'; var c = ''; var d = 'bar';",
        L3::StringTable{"", "foo", "bar"});
    helper(t, "Chunks", "var a = 'foo' 'bar';", L3::StringTable{"", "foobar"});
    helper(t, "Dupes", "var a, var b = 'foo', 'foo';", L3::StringTable{"", "foo"});
}

void TestSimple(Test& t)
{
    t.BeginSection("Simple");

    Compile(t, "", true);
    Compile(t, "var a;", true);
    Compile(t, "var a = 0;", true);
    Compile(t, "var a = 0; var b = 0;", true);
    Compile(t, "var a = 0; var b = a;", true);
    Compile(t, "var a = 1 + 2 * 3;", true);
    Compile(t, "var a; var b = ++a;", true);
    Compile(t, "return 1;", true);
    Compile(t, "return 1, 2;", true);
    Compile(t, "var a = 'a string!';", true);
    Compile(t, "var a = '';", true);
    Compile(t, "var a = @foo;", true);
}

void TestScope(Test& t)
{
    t.BeginSection("Scope");

    Compile(t, "var a; { var b; }", true);
    Compile(t, "var a = 0; { var b = 0; }", true);
    Compile(t, "var a = 0; { var b = a; }", true);
    Compile(t, "var a = 0; { var a = 0; }", true);
    Compile(t, "var a = 0; { var a = a; }", true);
    Compile(t, "var a; { var a; } var b = a;", true);
}

void TestFunction(Test& t)
{
    t.BeginSection("Function");

    Compile(t, "var f = fn {};", true);
    Compile(t, "var f = fn x, y { var z = x + y; return z; };", true);
    Compile(t, "var f = fn x { var z = fn { return 1; }; return x + z(); }; f(2);", true);
    Compile(t, "var f = fn i { recurse(i); };", true);
}

void TestIf(Test& t)
{
    t.BeginSection("If");

    Compile(t, "var a; if true { a = 1; } return a;", true);
    Compile(t, "var a; if true { a = 1; } else { a = 2; } return a;", true);
    Compile(t, "var a; if true { a = 1; } else if true { a = 2; } return a;", true);
    Compile(t, "var a; if true { a = 1; } else if true { a = 2; } else { a = 3;} return a;", true);
}

void TestLoop(Test& t)
{
    t.BeginSection("Loops");

    Compile(t, "var a = true; while a { a = false; } return a;", true);
    Compile(t, "var a; do { a = !a; } while a; return a;", true);
}

void TestObject(Test& t)
{
    t.BeginSection("Objects");

    Compile(t, "var t = tbl{};", true);
    Compile(t, "var t = tbl{}; t[0] = 1;", true);
    Compile(t, "var t = tbl{}; var a = t[0];", true);
    Compile(t, "var a = lst[];", true);
    Compile(t, "var a = lst[]; a[0] = 1;", true);
    Compile(t, "var a = lst[]; var b = a[0];", true);

    Compile(t, "var t = tbl{}; var a = t.foo;", true);
    Compile(t, "var t = tbl{}; t.foo = 5;", true);
    Compile(t, "var t = tbl{}; t:foo();", true);
    Compile(t, "var t = tbl{}; t:foo(5, 'a');", true);
}

int main()
{
    Test t{"compiler"};

    TestFindFunctions(t);
    TestBuildAtomTable(t);
    TestBuildStringTable(t);
    TestSimple(t);
    TestScope(t);
    TestFunction(t);
    TestIf(t);
    TestLoop(t);
    TestObject(t);

    t.PrintTotal();

    return 0;
}
